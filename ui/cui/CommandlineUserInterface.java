package ui.cui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import domain.Eshop;
import valueObjects.BulkProduct;
import valueObjects.Customer;
import valueObjects.Employer;
import valueObjects.Person;
import valueObjects.Product;
import domain.exceptions.*;

public class CommandlineUserInterface // CUI Klasse wird erstellt und ein Eshop + Einlese Funktion wird implementiert
{
    private Eshop lidl;
    private BufferedReader input;
    private Person user;

    public CommandlineUserInterface() {
        lidl = new Eshop();
        input = new BufferedReader(new InputStreamReader(System.in));
    }

    /**
     * Methode des Startmenues , die den Login für Mitarbeiter und Kunde durchführt
     * 
     * @param username Username der Person
     * @param password Passwort der Person
     */
    private void startMenue() throws IOException {
        String username = null;
        String password = null;

        System.out.print("Bitte Username eingeben: ");
        username = readInput(); // Einlesen von Username

        System.out.print("Bitte Passwort eingeben: ");
        password = readInput(); // Einlesen von Password

        try {
            // Einloggen der Person im EShop
            user = lidl.login(username, password);
        } catch (LoginFailException e) {
            System.out.println(e.getMessage());
            startMenue();
        }
        // Nutzer ist ein Kunde
        if (user instanceof Customer) {
            System.out.println("Erfolgreich als Kunde eingeloggt.");
            runCustomer();
        }

        // Nutzer ist ein Mitarbeiter
        else if (user instanceof Employer) {
            System.out.println("Erfolgreich als Mitarbeiter eingeloggt.");
            runEmployer(); // Ruft nach dem erfolgreichen Mitarbeiter login die Managments auf
        } else {
            System.out.println("Probleme beim Einloggen! Bitte Daten überprüfen!");
        }
    }

    /**
     * Methode nach dem Mitarbeiterlogin , die alle vorhandenen Verwaltungen zeigt
     * und die dazugehörigen Befehle.
     */
    private void managmentSelectionPrint() {
        System.out.println("In welchen Managmentbereich wollen sie?");
        System.out.println("---------------------------------------");
        System.out.println("Kundenverwaltung = 'c'");
        System.out.println("---------------------------------------");
        System.out.println("Mitarbeiterverwaltung = 'e'");
        System.out.println("---------------------------------------");
        System.out.println("Produktverwaltung = 'p'");
        System.out.println("---------------------------------------");
        System.out.println("Ausloggen = 'o'");
        System.out.println("---------------------------------------");
        System.out.println("Programm beenden = 'q'");
    }

    /**
     * Methode, die die vorhandenen Befehle der Kundenverwaltung ausgibt.
     */
    private void printMenueCustomer() {
        System.out.println("Befehle: \n  Kunden auflisten: 'a'");
        System.out.println("         \n  Kunden hinzufügen: 'b'");
        System.out.println("         \n  Kunden entfernen mit Name und Kundennummer : 'c'");
        System.out.println("         \n  Zurück zum Menü: 'm'");
        System.out.println("         \n  Ausloggen: 'o'");
        System.out.println("         \n  --------------------------");
        System.out.println("            \n  Beenden:            'q'");
    }

    /**
     * Methode, die die vorhandenen Befehle der Mitarbeiterverwaltung ausgibt.
     */
    private void printMenueEmployer() {
        System.out.println("Befehle: \n  Mitarbeiter auflisten: 'a'");
        System.out.println("         \n  Mitarbeiter hinzufügen: 'b'");
        System.out.println("         \n  Mitarbeiter entfernen: 'c'");
        System.out.println("         \n  Zurück zum Menü: 'm'");
        System.out.println("         \n  Ausloggen: 'o'");
        System.out.println("         \n  --------------------------");
        System.out.println("            \n  Beenden:            'q'");
    }

    /**
     * Methode, die die vorhandenen Befehle der Produktverwaltung ausgibt.
     */
    private void printMenueProduct() {
        System.out.println("Befehle: \n  Produktliste anzeigen(sortiert nach Name): 'a'");
        System.out.println("         \n  Produktliste anzeigen(sortiert nach Artikelnr.): 'b'");
        System.out.println("         \n  Produkt hinzufügen: 'c'");
        System.out.println("         \n  Massengut Produkt hinzufügen: 'h'");
        System.out.println("         \n  Produkt entfernen: 'd'");
        System.out.println("         \n  Produkt suchen mit Name: 'e'");
        System.out.println("         \n  Produkt suchen mit Preis: 'f'");
        System.out.println("         \n  Produktbestand ändern:    'g'");
        System.out.println("         \n  Ein- und Auslagerung:  'l'   ");
        System.out.println("         \n  Zurück zum Menü: 'm'");
        System.out.println("         \n  Ausloggen: 'o'");
        System.out.println("         \n  --------------------------");
        System.out.println("            \n  Beenden:            'q'");
    }

    /**
     * Methode, die die vorhandenen Befehle der Produktverwaltung ausgibt.
     */
    private void printCommandsCustomer() {
        System.out.println("Befehle: \n  Produktkatalog anzeigen: 'a'");
        System.out.println("         \n  Warenkorb anzeigen: 'b'");
        System.out.println("         \n  Produkt in den Warenkorb legen: 'c'");
        System.out.println("         \n  Produkt aus dem Warenkorb entfernen: 'd'");
        System.out.println("         \n  Stückzahl eines Produktes im Warenkorb verringern: 'e'");
        System.out.println("         \n  Stückzahl eines Produktes im Warenkorb erhöhen: 'f'");
        System.out.println("         \n  Warenkorb kaufen: 'g'");
        System.out.println("         \n  Ausloggen: 'o'");
        System.out.println("         \n  --------------------------");
        System.out.println("         \n  Beenden:            'q'");
    }

    /**
     * Die Eingabe wird zurueckgegeben
     * 
     * @return user input.
     */
    private String readInput() throws IOException {
        return input.readLine();
    }

    /**
     * Alle Methoden der Kundenverwaltung (auflisten, hinzufügen, löschen mit Name,
     * löschen mit Kundennummer)
     * 
     * @param input Eingabe des users
     */
    private void menueCustomermanagment(String input) throws IOException {
        // defeniert die verwendeten Variablen / Arraylist
        String username;
        String password;
        String firstname;
        String lastname;
        String streetname;
        String town;
        String country;
        int plz;
        int streetnumber;

        /**
         * Switch der Kundenverwaltung, der nach Nutzereingabe verschiedene Befehle
         * durchführt
         */
        switch (input) {

            // Liste alle Kunden auf
            case "a":
                System.out.println(lidl.getCustomerList());
                break;

            // Fügt einen neuen Kunden hinzu
            case "b":
                System.out.println("Geben Sie die Daten des neuen Kunden an :");
                System.out.println("-----------------------------------------------");
                System.out.print("Benutzername: ");
                username = readInput();
                System.out.print("Passwort: ");
                password = readInput();
                System.out.print("Vorname: ");
                firstname = readInput();
                System.out.print("Nachname: ");
                lastname = readInput(); // Abfrage der Daten und setzten der Variablen mit den Eingaben
                System.out.print("Straßenname: ");
                streetname = readInput();
                System.out.print("Stadt: ");
                town = readInput();
                System.out.print("Land: ");
                country = readInput();
                System.out.print("PLZ: ");
                plz = Integer.parseInt(readInput());
                System.out.println("Hausnummer: ");
                streetnumber = Integer.parseInt(readInput());
                Customer newCustomer = new Customer(username, password, firstname, lastname, streetname, town, country,
                        plz, streetnumber); // neuen Kunden erstellen mit den Eingaben
                try {
                    if (lidl.addCustomer(newCustomer)) {    // hinzufügen des Kunden in die Kundenliste
                        System.out.println("Kunde " + firstname + " " + lastname + " wurde erfolgreich eingetragen."); 
                    } else {    //Ausgabe, dass es nicht funktioniert hat.
                        System.out.println("Kunde " + firstname + " " + lastname + " wurde nicht eingetragen!"); 
                    }
                } catch (CustomerAlreadyExists e) {
                    System.out.println(e.getMessage());
                }
            break;
            
         // Löscht einen Kunden aus der Liste mit dem Namen                                           
        case "c":
            System.out.println("Geben Sie die Daten des zu löschenden Kundens an :");
            System.out.print("Benutzername: ");
            username = readInput();                             
    // TsODO Der Employer, der in diesem Moment angemeldet ist, soll sich nicht loeschen koennen.                          
            if(lidl.removeCustomer(username))
            {    
                System.out.println(username + " wurde erfolgreich aus der Kundenliste gelöscht. ");
            } else {
                System.out.println("Kunde " + username + " wurde nicht gelöscht!");
            }
            break;
           
        case "m":
            runEmployer();
            break;
            
        // Logout
        case "o":
            lidl.clean();
            user = null;
            startMenue(); 
            break; 
        }
    }

     
    /**
     * Alle Methoden der Mitarbeiterverwaltung (auflisten, hinzufügen, löschen mit Name, löschen mit Mitarbeiternummer)
     * @param input Eingabe der Daten
     * @throws EmployerAlreadyExists
     */
       private void menueEmployermanagment(String input) throws IOException, EmployerAlreadyExists     
       {                                                                                                             
           // defeniert die verwendeten Variablen / Arraylist   
           String username;
           String password;                                                        
           String firstname;
           String lastname;
           
          
        /**
         * Switch der Mitarbeiterverwaltung, der nach Nutzereingabe verschiedene Befehle durchführt
         */
           switch(input){ 

           // Liste alle Mitarbeiter auf
            case "a":                                   
               System.out.print(lidl.getEmployerList());
               break;
            
           // Fügt einen neuen Mitarbeiter hinzu
            case "b":
               System.out.println("Geben Sie die Daten des neuen Mitarbeiters an :");
               System.out.println("-----------------------------------------------");
               System.out.println("Benutzername:");
               username = readInput();
               System.out.println("Passwort:");
               password = readInput();
               System.out.println("Vorname:");
               firstname = readInput();                                                         // Setzt die Variablen mit den Eingaben des Nutzers
               System.out.println("Nachname:");
               lastname = readInput();
               Employer newEmployer = new Employer(username, password, firstname, lastname);

               if(lidl.addEmployer(newEmployer))
               {
                    System.out.println("Mitarbeiter " + firstname + " " + lastname + " wurde erfolgreich eingetragen.");
               } else {
                    System.out.println("Mitarbeiter" + firstname + " " + lastname + " wurde nicht eingetragen!");
               }
               break;
            
           // Löscht einen Mitarbeiter 
           case "c":
               System.out.println("Geben Sie den Namen des zu löschenden Mitarbeiters ein :");
               System.out.println("Benutzername:");
               username = readInput();
               if(lidl.removeEmployer(username))
               {
                    System.out.println("Mitarbeiter " + username + " wurde erfolgreich gelöscht.");
               } else {
                    System.out.println("Mitarbeiter " + username + " wurde nicht gelöscht.");
               }               
               break;

            case "m":
                runEmployer();
                break;

            // Logout
            case "o":
                lidl.clean();
                user = null;
                startMenue(); 
                break;    
            }
       }
    

    /**
    * Alle Methoden der Produktverwaltung (auflisten sortiert nach Name oder Artikelnummer, hinzufügen, löschen mit Name, 
    * suchen nach Produkt mit Produktnamen , suchen nach Produkt mit Produktpreis, Produktmenge festlegen)
    * 
	* @param input Eingabe der Daten
	*/     
    private void menueProductmanagment(String input) throws IOException     
    {                                                                                                              
        // defeniert die verwendeten Variablen / Arraylist                                                           
        String name;
        int stock;
        double price;
        int packSize;
        ArrayList<Product> productlist = new ArrayList<Product>(lidl.getProductList());

        
       
        /**
         * Switch der Produktverwaltung, der nach Nutzereingabe verschiedene Befehle durchführt.
         */
        switch(input)
        { 
            // Liste alle Produkte auf (sortiert nach Namen)
            case "a":                                  
                productlist = lidl.productlistSortName(); 
                // Schoenere Praesentation der Produktliste:
                for(Product product : productlist)
                {
                    // Check ob Objekt geloescht ist
                    if(product.getDeleted() != true)
                    {
                        // Falls nein, gebe dieses aus
                        System.out.println(product.toString());   
                    }
                }
                break;

            // Liste alle Produkte auf (soritiert nach Artikelnummer)
            case "b":
                productlist = lidl.sortMan();
                for(Product product : productlist)
                {
                    if(product.getDeleted() != true)
                    {
                        System.out.println(product.toString());
                    }
                }
                break;
                                
            // füge ein Produkt zu der Liste hinzu
            case "c":                                    
 
                System.out.println("Geben Sie die Daten des neuen Produktes ein :");
                System.out.println("-----------------------------------------------");
                System.out.print("Name:");
                name = readInput();
                System.out.print("Stückzahl: ");                                         // Setzt die Variablen mit den Eingaben des Nutzers
                stock = Integer.parseInt(readInput());                                  // Konvertierung von String zu Integer                          
                System.out.print("Preis: ");
                price = Double.parseDouble(readInput());
                Product newProduct = new Product(name, price);           // erstellt neues Produkt 
                lidl.setStock(user, newProduct, stock);
               try{
                if(lidl.addProduct(newProduct))                             // fügt das neue Produkt zu der Liste hinzu
                {
                    System.out.println("Produkt wurde erfolgreich "+ newProduct.getName() + " hinzugefügt");
                    newProduct = null; // leere das neu erzeugte Produkt.
                    break;
                } 
                }catch (ProductAlreadyExistsException e) {
                    System.out.println("Produkt "+ newProduct.getName() + " wurde nicht hinzugefügt");
                    System.out.println(e.getMessage());
                    newProduct = null; // leere das neu erzeugte Produkt. 
                    break;
                }    

                
                

            // füge Massengut zu der Liste hinzu
            case "h":                                    
                System.out.println("Geben Sie die Daten des neuen Produktes ein :");
                System.out.println("-----------------------------------------------");
                System.out.print("Name:");
                name = readInput();
                System.out.print("Stückzahl: ");                                         // Setzt die Variablen mit den Eingaben des Nutzers
                stock = Integer.parseInt(readInput());
                System.out.print("Packungsgröße: ");                                         // Setzt die Variablen mit den Eingaben des Nutzers
                packSize = Integer.parseInt(readInput());                                  // Konvertierung von String zu Integer                          
                System.out.print("Preis: ");
                price = Double.parseDouble(readInput());
                
                Product bulkProduct = new BulkProduct(name, price, packSize);           // erstellt neues Produkt 
                if(!lidl.setStock(user, bulkProduct, stock))
                {
                    System.out.println("Bestand konnte nicht geändert werden.");
                    System.out.println("Der Bestand muss ein Vielfaches von der Packungsgröße ("
                    + packSize + ") sein.");
                }
                try{
                if(lidl.addProduct(bulkProduct))                             // fügt das neue Produkt zu der Liste hinzu
                {
                    System.out.println("Produkt wurde erfolgreich "+ bulkProduct.getName() + " hinzugefügt");
                    newProduct = null;
                    break;
                } 
                }catch (ProductAlreadyExistsException e) {
                    System.out.println("Produkt "+ bulkProduct.getName() + " wurde nicht hinzugefügt");
                    newProduct = null;
                    break;  
                }
               
                    
            // lösche ein Produkt aus der Liste
            case "d":
                System.out.println("Geben Sie die Daten des zu löschenden Produktes ein :");
                System.out.println("-----------------------------------------------");
                System.out.print("Name:");
                name = readInput();
                try{
                    if(lidl.deactivateProduct(user, name))
                    {
                        System.out.println("Produkt " + name + "wurde erfolgreich entfernt");
                    } 
                }catch (ProductDoesntExistException e) {
                    System.out.println("Produkt " + name + "wurde nicht entfernt");
                    System.out.println(e.getMessage());
                }
                break;
           
            // Sucht Produkt mit Namen             
            case "e":
                System.out.println("Geben Sie die Daten des gesuchten Produktes ein: ");
                System.out.println("-----------------------------------------------");
                System.out.print("Name: ");
                name = readInput();                                                             
                productlist = lidl.searchProducts(name);
                System.out.println(productlist); 
                break;
            
            // Sucht Produkt mit Preis
            case "f":
                System.out.println("Geben Sie die Daten des gesuchten Produktes ein: ");
                System.out.println("-----------------------------------------------");
                System.out.print("Preis: ");
                price = Double.parseDouble(readInput());
                productlist = lidl.searchProductsPrice(price);
                System.out.println(productlist); 
                break;
            
            // Verändert den Lagerbestand des Produktes
            case "g":
                System.out.print("Geben Sie den Namen des Produktes an: ");
                name = readInput();
                System.out.print("Geben Sie den neuen Lagerbestand an: ");
                stock = Integer.parseInt(readInput());
               try{
                if(lidl.setStock(user, name, stock))
                {
                    System.out.println("Produktbestand wurde erfolgreich angepasst.");
                } 
                
                 } catch (ProductDoesntExistException e) {
                    System.out.println("Produktbestand wurde nicht angepasst.");
                    System.out.println(e.getMessage());
                }
                
                break;

            case "l":
                System.out.print("Geben Sie den Namen des Produktes an: ");
                name = readInput();
                lidl.getLog().showProductLog(name);
                break;

            // Menu wird neugestartet
            case "m":
                runEmployer();
               break;

            // User wird wieder ausgeloggt
            case "o":
                lidl.clean();
                user = null;
                startMenue(); 
                break; 
            }
        } 
    

    //* MENUE FOR CUSTOMER
    private void menueForCustomer(String input) throws IOException
    {
        int quantity;   
        Product product;
        Customer customer = (Customer) user;
        ArrayList<Product> productListCustomer = new ArrayList<Product>(lidl.getProductList());
        
        switch(input)
        {
            // Produktliste anzeigen
            case "a":
                productListCustomer = lidl.productlistSortName(); 
                // Schoenere Praesentation der Produktliste:
                for(Product p : productListCustomer)
                {
                    // Pruefe ob objekt geloescht ist
                    if(p.getDeleted() != true)
                    {
                        // Falls nicht, gebe Produkt aus
                        System.out.println(p.toString());   
                    }
                }
                break;

            // Warenkorb anzeigen
            case "b": 
                System.out.println(lidl.getBasketList(customer));
                break;
            
            // Produkt in den Warenkorb legen    
            case "c": 
                try {
                    System.out.println("Welches Produkt wollen Sie in ihren Warenkorb legen? ");
                    product = lidl.searchProduct(readInput()); 
                    System.out.println("Wie viel wollen Sie von dem Produkt: " + product.getName() + " in den Warenkorb legen? ");
                    quantity = Integer.parseInt(readInput());

                    if(lidl.putBasket(customer, product, quantity))
                    {
                        System.out.println("Produkt " + product.getName() + " erfolgreich in den Warenkorb hinzugefuegt.");
                    }

                }catch (StockNotEnoughException e){
                    System.out.println(e.getMessage());  
                 break;
                }
                catch (WrongAmountBulkException | ProductDoesntExistException e)  
                {
                   System.out.println(e.getMessage()); 
                   break;
                }
                
            // Produkt aus dem Warenkorb entfernen
            case "d":
                System.out.println("Welches Produkt wollen Sie aus ihrem Warenkorb entfernen");
                try{
                    product = lidl.searchProduct(readInput());
                    lidl.removeInBasket(customer, product);
                    break;
                }catch(ProductDoesntExistException e)
                {
                    System.out.println(e.getMessage());  
                    break; 
                } 
             
                    
            // Stückzahl im Warenkorb für Artikel verringern
            case "e":
                System.out.println("Die Stückzahl welches Produktes wollen Sie verringern?: ");
                try{
                product = lidl.searchProduct(readInput());
                System.out.println("Neue Stückzahl : ");
                quantity = Integer.parseInt(readInput());

                if(lidl.reduceBasket(customer, product, quantity))
                {
                    System.out.println("Produktbestand wurde erfolgreich angepasst.");
                } else {
                    System.out.println("Produktbestand wurde nicht angepasst.");
                }
                break;
            }catch(ProductDoesntExistException e)
            {
                System.out.println(e.getMessage());
                break;   
            } 
               
            // Stückzahl im Warenkorb für Artikel erhoehen
            case "f":
                System.out.println("Die Stückzahl welches Produktes wollen Sie erhöhen?: ");
                try{
                    product = lidl.searchProduct(readInput());
                    System.out.println("Um wie viel möchten sie das Produkt erhöhen?: ");
                    quantity = Integer.parseInt(readInput());
                    try{        
                        if(lidl.putBasket(customer, product, quantity))
                        {
                            System.out.println("Produkt " + product.getName() + " erfolgreich Menge erhöht.");
                        } else {
                            System.out.println("Produkt " + product.getName() + " Menge wurde nicht erhöht!");
                            System.out.println("Es kann sein, dass die Packungsgröße nicht passt, "
                            + "nicht genügend Produkte im Lager sind oder die Produkte reserviert sind.");
                        }
                        break;  
                    } catch(StockNotEnoughException e){
                        System.out.println(e.getMessage());
                        break;
                    } catch(WrongAmountBulkException e){
                        System.out.println(e.getMessage());
                        break;
                    } 
                } catch(ProductDoesntExistException e){
                    System.out.println(e.getMessage()); 
                    break;  
                } 
                
            // Warenkorb kaufen
            case "g":
                lidl.buy(customer);
                break;

            // Logge aktuellen Nutzer aus
            case "o":
                lidl.cleanC(customer);
                user = null;
                startMenue(); 
            break; 
        }  
    }   




    /**
     * Methode mit einer Schleife nach dem erfolgreichen einloggen des Mitarbeiters,
     * die nach der Eingabe in den jeweiligen Managment if-Zweig geht und solange
     * Befehle abfragt bis 'q' als Eingabewert gegeben wird.
     * 
     * @throws IOException
     */
    public void runEmployer() throws IOException
    {
        String in = "";    // setzt einen Eingabe String zuerst als leer

        do {
            managmentSelectionPrint();                  // gibt das Menue aus mit der Auswahl der Verwaltungen (Kunden/Mitarbeiter/Produkt)
            try {
                in = readInput();                    // Einlesen von Eingabe 
              
                
                
            } catch (IOException e) {
                System.out.println(e.getMessage());  
            } 
             // Überprüfung ob die Eingabe die Kundenverwaltung ist.
            if (in.equals("c"))        
            {
                                           
                do {
                    try {
                        printMenueCustomer();
                        in = readInput();                   
                        menueCustomermanagment(in);
                
                } catch (IOException e) {
                    System.out.println(e.getMessage());    
                }
                
                } while (!in.equals("q"));  // solange die Antwort nicht q ist wird immmer wieder gefragt welchen Befehl man ausführen möchte
            }    
            
            // Überprüfung ob die Eingabe die Mitarbeiterverwaltung ist.
            if (in.equals("e"))         
            {
                                       
                do {
                    
                    try {
                        printMenueEmployer();
                        in = readInput();                   
                        menueEmployermanagment(in);
                
                    } catch (IOException e){
                        System.out.println(e.getMessage());  
                    } catch (EmployerAlreadyExists e) {
                        System.out.println(e.getMessage());
                    }

                } while (!in.equals("q"));  // solange die Antwort nicht q ist wird immmer wieder gefragt welchen Befehl man ausführen möchte
            }

        // Überprüfung ob die Eingabe die Produktverwaltung ist.
            if (in.equals("p"))         
            {
                                        
                do {
                    try {
                        printMenueProduct();
                        in = readInput();                   
                        menueProductmanagment(in);
                
                } catch (IOException e) {
                    System.out.println(e.getMessage());    
                }
                } while (!in.equals("q"));  // solange die Antwort nicht q ist wird immmer wieder gefragt welchen Befehl man ausführen möchte
            }    
        
        if(in.equals("o"))
        {
            lidl.clean();
            user = null;
            startMenue();    
        }
        
        } while (!in.equals("q"));       
       
        
        if (in.equals("q"))
        {
            lidl.getLog().showTotalLog();
            lidl.clean();
            user = null;
            System.exit(0);   
        }

    }
    

    /**
	 * Methode mit einer Schleife nach dem erfolgreichen einloggen des Kudens, 
     * die nach der Eingabe Interaktion mit den Produkten und dem Warenkorb durchführt  bis 'q' als Eingabewert gegeben wird.
	 */    
    public void runCustomer() throws IOException
    {
        String in = "";    // setzt einen Eingabe String zuerst als leer

        do {
                printCommandsCustomer();               // gibt das Menue aus mit der Auswahl der Befehle des Kunden 
                in = readInput();                    // Einlesen von Eingabe 
             

                if (!in.equals("q"))
                { 
                    menueForCustomer(in);
                } 
                
        } while(!in.equals("q")); 
        
        // Verlasse Programm
       if (in.equals("q"))
       {
            Customer customer = (Customer) user;
            lidl.cleanC(customer);
            System.exit(0); 
       }
    }
    


    public static void main (String [] args) throws IOException
    {
        CommandlineUserInterface cui;               // Erstellt eine Cui
        cui = new CommandlineUserInterface();       // cui ist ein neues Commandline-User-Interface
        cui.startMenue();                                    
    }

}