package ui.gui;

import domain.exceptions.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import domain.Eshop;
import valueObjects.Customer;
import valueObjects.Employer;
import valueObjects.Person;

public class LogIn extends JFrame implements ActionListener
{

    private static final long serialVersionUID = 1L;
    
    private JLabel labelUsername = new JLabel("Username eingeben: ");
    private JLabel labelPassword = new JLabel("Passwort eingeben: ");
    private JTextField fieldUsername = new JTextField(30);
    private JPasswordField fieldPassword = new JPasswordField(30);
    private JButton buttonLogIn = new JButton("Login");
    private JButton buttonRegist = new JButton("Registrieren"); 
    private JTabbedPane tabbedPane;
    private JTabbedPane tabbedPaneEmployer = null;
    
    private Eshop eshop;
    private String username = null; 
    private String password = null;
    private Person user;
    private Customer customer = null;
    private Employer employer = null;
   
    private LogIn loginGUI;
    private GuiCustomer guiCustomer;
    private GuiBuy guibuy_TAB;
    private Log_TAB logTab;
    
    public LogIn(Eshop eshop)
    {
        super();
        this.eshop = eshop;
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        loginGUI = this;
        this.setExtendedState(JFrame.MAXIMIZED_BOTH); 
        this.initialize();
    }

    private void initialize()
    {
    
       
        JPanel mainpanel = new JPanel();
        mainpanel.setLayout(new BorderLayout());
        

        tabbedPane = new JTabbedPane();
   
     
        // erstelle Panel fuer außen herum
        //this.setLayout(new BorderLayout());
        
        // erstelle neues JPanel mit GridBagLayout
        JPanel loginPanel = new JPanel(new GridBagLayout());
        
        // Fuege Panel dem Fenster hinzu
        //this.add(loginPanel, BorderLayout.CENTER);

        // erstelle Objekt wo der Inhalt gespeichert werden soll
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
        
        // gebe Panel Komponenten
        constraints.gridx = 0;
        constraints.gridy = 0;
        loginPanel.add(labelUsername, constraints);

        constraints.gridx = 1;
        loginPanel.add(fieldUsername, constraints);

        constraints.gridx = 0;
        constraints.gridy = 1;
        loginPanel.add(labelPassword, constraints);

        constraints.gridx = 1;
        loginPanel.add(fieldPassword, constraints);

        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 4;
        constraints.anchor = GridBagConstraints.CENTER;
        loginPanel.add(buttonLogIn, constraints);


        constraints.gridx= 2;  
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.anchor = GridBagConstraints.CENTER;
        loginPanel.add(buttonRegist, constraints);

        
        //! Panels zsm bauen

        this.add(mainpanel);
        mainpanel.add(tabbedPane);
        tabbedPane.add("Login", loginPanel);
        

        this.setVisible(true);
        
        // REGISTER Panel
        Registration regst_TAB = new Registration(eshop);
        tabbedPane.add("Registration", regst_TAB);
        

        // erstelle Rand fuer das Fenster
        loginPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Login"));
      
        //-----------LISTENER--------

    //Listener fuer login button
    buttonLogIn.addActionListener(new ActionListener()
    {
        public void actionPerformed(ActionEvent ae)
        {
            username = fieldUsername.getText();
            password = fieldPassword.getText();

           try {
            // Einloggen der Person im EShop
            user = eshop.login(username, password);     
        
            }catch (LoginFailException e)
            {
                JOptionPane.showConfirmDialog(
                        null,
                        e.getMessage(),
                        "Fehler",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.ERROR_MESSAGE
                    );
                System.out.println(e.getMessage());
                   
            }
            // Nutzer ist ein Kunde 
            System.out.println(user instanceof Customer);
            if(user instanceof Customer)            
            {
                customer = (Customer) user;
               // GuiBuy gb = new GuiBuy(eshop, customer);
                System.out.println("Erfolgreich als Kunde eingeloggt.");
                tabbedPane.remove(loginPanel);
                tabbedPane.remove(regst_TAB);
               
                mainpanel.remove(tabbedPane);
                GuiBuy guiBuy = new GuiBuy(eshop,customer,loginGUI);
                mainpanel.add(guiBuy);
                
                
                
            }
            // Nutzer ist ein Mitarbeiter 
            else if(user instanceof Employer)
            {
                employer = (Employer) user;
               // System.out.println("Erfolgreich als Mitarbeiter eingeloggt.");

               
               mainpanel.remove(tabbedPane);

               tabbedPaneEmployer = new JTabbedPane();

               mainpanel.add(tabbedPaneEmployer);

               Log_TAB logTab = new Log_TAB(eshop, employer,loginGUI);
                // Fuege logTab der Tab-Leiste hinzu
                tabbedPaneEmployer.add("Ereignisübersicht", logTab);

                // Lege CustomerManagement_TAB an
                CustomerManagement_TAB cusManTab = new CustomerManagement_TAB(eshop, employer, loginGUI);
                // Fuege cusManTab der Tab-Leiste hinzu
                tabbedPaneEmployer.add("Kundenverwaltung", cusManTab);
                tabbedPaneEmployer.setSelectedIndex(0);

                // Lege ProductManagement an
                ProductManagement_TAB GuiProdTab = new ProductManagement_TAB(eshop, employer,loginGUI);    
                // Fuege ProductManagement der Tab-Leiste hinzu
                tabbedPaneEmployer.add("Produktverwaltung",GuiProdTab);
                tabbedPaneEmployer.setSelectedIndex(0);

                 // Lege EmployerManagement an
                EmployerManagement_TAB GuiEmplTab = new EmployerManagement_TAB(eshop, employer,loginGUI);
                // Fuege EmployerManagement der Tab-Leiste hinzu
                tabbedPaneEmployer.add("Mitarbeiterverwaltung",GuiEmplTab);
                tabbedPaneEmployer.setSelectedIndex(0);


       
                
            } else 
                {
                    JOptionPane.showConfirmDialog(
                        null,
                        "Probleme beim Einloggen! Bitte Daten überprüfen!",
                        "Fehler",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.ERROR_MESSAGE
                    );
                    System.out.println("Probleme beim Einloggen! Bitte Daten überprüfen!");
                }
        }
    });

     //Listener fuer Register button
    buttonRegist.addActionListener(new ActionListener()
    {
        public void actionPerformed(ActionEvent ae)
        {

        tabbedPane.setSelectedIndex(1);    
           
        }

    });

   
   

   //FIXME aktualisiert nicht die Liste
  // ChangeListener fuer das TabbedPane
  tabbedPaneEmployer.addChangeListener( new ChangeListener()
    {
        public void stateChanged(ChangeEvent ce)
        {
            System.out.println("Derzeit ausgewähltes Tab: " + tabbedPaneEmployer.getSelectedIndex());  
            logTab.refreshTable();
        }
    });
    
    
        //TODO LogTab soll aktualisiert werden beim
        this.addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) 
            {
                eshop.clean();
                System.out.println("Programm wurde beendet");
            }
        });
    }
    

    @Override
    public void actionPerformed(ActionEvent e) 
    {
        if (e.getActionCommand().equals("KAUFEN"))
        {
           System.out.println("TEST KAUFEN BUTTOn");
            tabbedPane.remove(1);
            GuiInvoice guiInvoice = new GuiInvoice(eshop, customer, guibuy_TAB);
            tabbedPane.add("Rechnung", guiInvoice);
            tabbedPane.setEnabledAt(0, false);
            tabbedPane.setEnabledAt(1, true);
            tabbedPane.setSelectedIndex(1);
        }
        if (e.getActionCommand().equals("logout"))
        {
            System.out.println("Kunde erfolgreich ausgeloggt"); 
           // FIXME Führt zu einem Error , da Basket nicht bekannt vermutlich
           System.out.println(customer);
           eshop.cleanC(customer);
            dispose();
            LogIn li = new LogIn(eshop);
        }
        if (e.getActionCommand().equals("again"))
        {
            eshop.cleanC(customer);
            
            guiCustomer.back();
            tabbedPane.setEnabledAt(0, true);
            tabbedPane.setEnabledAt(1, false);
            tabbedPane.setSelectedIndex(0);
            System.out.println("nochmal einkaufen");
        }
        if (e.getActionCommand().equals("logoutEmployer"))
        {
            System.out.println("Mitarbeiter erfolgreich ausgeloggt"); 
            eshop.clean();
            dispose();
            LogIn li = new LogIn(eshop);
            
        }
    }
 


    //Hier beginnt das Programm
    public static void main(String[] args) 
    {
        Eshop eshop = new Eshop();
        LogIn li = new LogIn(eshop);
    }

       

}


