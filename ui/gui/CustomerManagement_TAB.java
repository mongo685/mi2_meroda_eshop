package ui.gui;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import domain.Eshop;
import domain.exceptions.CustomerAlreadyExists;
import valueObjects.Customer;
import valueObjects.Employer;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.*;
import java.awt.event.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.Dimension;

public class CustomerManagement_TAB extends JPanel {
    private static final long serialVersionUID = 1L;

    private JButton logOut;

    private JTable personOverView;
    private JTextField inputUsername;
    private JTextField inputPassword;
    private JTextField inputFirstname;
    private JTextField inputLastname;
    private JTextField inputStreetname;
    private JTextField inputCountry;
    private JTextField inputTown;
    private JTextField inputPlz;
    private JTextField inputStreetnumber;
    private JButton addButton;

    private JTextField searchTextField;
    private JButton searchButton;
    private JButton removeButton;

    private Eshop eshop;
    private Employer loggedEmployer;
    private LogIn logIn;

    public CustomerManagement_TAB(Eshop eshop,  Employer loggedEmployer, LogIn logIn) {
        super();
        this.eshop = eshop;
        this.loggedEmployer = loggedEmployer;
        this.logIn = logIn;
        this.initialize();
    }

    private void initialize() {
        // Farbe der Buttons
        Color cb = new Color(255, 244, 200);

        // Schriftarten
        Font f1 = new Font("Impact", Font.BOLD, 30);

        // Aueßeres Layout
        this.setLayout(new BorderLayout());

        // --- Gestallte oberen Teil

        // Grundlegende Einstellungen
        JPanel upperPanel = new JPanel();
        upperPanel.setLayout(new BoxLayout(upperPanel, BoxLayout.LINE_AXIS));
        upperPanel.setBackground(Color.BLACK);
        upperPanel.setPreferredSize(new Dimension(200, 40));
        this.add(upperPanel, BorderLayout.NORTH);

        // Ueberschrift Mitarbeiterverwaltung
        JLabel headTitle = new JLabel();
        headTitle.setText("Kundenverwaltung");
        headTitle.setFont(f1);
        headTitle.setForeground(Color.RED);
        upperPanel.add(headTitle);

        // Luecke
        Dimension upMin = new Dimension(20, 5);
        Dimension upPre = new Dimension(Short.MAX_VALUE, 5);
        Dimension upMax = new Dimension(Short.MAX_VALUE, 5);
        upperPanel.add(new Box.Filler(upMin, upPre, upMax));

        // Setzte Log-Out Button
        logOut = new JButton("Log-Out");
        logOut.setBackground(cb);
        logOut.setPreferredSize(new Dimension(200, 30));
        upperPanel.add(logOut);

        // --- Gestalltung der linken Seite ---

        // Erzeuge linkes uebesichts Panel
        JPanel middelPanel = new JPanel();
        middelPanel.setLayout(new BorderLayout());
        this.add(middelPanel, BorderLayout.CENTER);

        // Daten die aus dem System in die Tabelle uebergeben werden sollen
        ArrayList<Customer> customerList = eshop.getCustomerList();
        TableModel customerViewModel = makeTableModel(customerList, null);

        // Erstelle Tabelle mit spezifischen Einstellungen
        personOverView = new JTable(customerViewModel) {
            // Veraenderung der deaktivieren
            @Override
            public boolean isCellEditable(int x, int y) {
                return false;
            }
        };

        // Spalten sollen nicht umgeordnet werden koennen
        personOverView.getTableHeader().setReorderingAllowed(false);

        // Gebe Tabelle Faheigkeit scrollbar zu sein
        JScrollPane scrollPane = new JScrollPane(personOverView);
        personOverView.setFillsViewportHeight(true);
        middelPanel.add(scrollPane, BorderLayout.CENTER);

        // --- Untere Suchleiste der Kundenuebersicht ---

        JPanel searchPanel = new JPanel();
        searchPanel.setLayout(new GridLayout());
        this.add(searchPanel, BorderLayout.SOUTH);

        searchTextField = new JTextField();
        searchPanel.add(searchTextField);

        searchButton = new JButton("Suchen");
        searchButton.setBackground(cb);
        searchPanel.add(searchButton);

        removeButton = new JButton("Löschen");
        removeButton.setBackground(cb);
        searchPanel.add(removeButton);

        // --- Gestalltung der rechten Seite ---

        JPanel rightpanel = new JPanel();
        rightpanel.setLayout(new BoxLayout(rightpanel, BoxLayout.PAGE_AXIS));
        rightpanel.setBackground(Color.BLACK);

        this.add(rightpanel, BorderLayout.EAST);

        // Abstandhalter zwischen Rand und ersten Element
        Dimension borderMinSize = new Dimension(5, 5);
        Dimension borderPrefSize = new Dimension(5, 5);
        Dimension borderMaxSize = new Dimension(5, 5);
        rightpanel.add(new Box.Filler(borderMinSize, borderPrefSize, borderMaxSize));

        // Titel ueber eingabe Feld fuer Kundehinzufuegen
        JLabel customerAddTitle = new JLabel("Neuen Kunden hinzufuegen");
        rightpanel.add(customerAddTitle);

        // Textfelder um Kundendaten einzutragen
        // ~ Benutzername
        inputUsername = new JTextField();
        inputUsername.setText("Benutzername eingeben");
        inputUsername.setPreferredSize(new Dimension(200, 30));
        rightpanel.add(inputUsername);
        rightpanel.add(new Box.Filler(borderMinSize, borderPrefSize, borderMaxSize));

        // ~ Passwort
        inputPassword = new JPasswordField();
        inputPassword.setText("Passwort eingeben");
        inputPassword.setPreferredSize(new Dimension(200, 30));
        rightpanel.add(inputPassword);
        rightpanel.add(new Box.Filler(borderMinSize, borderPrefSize, borderMaxSize));

        // ~ Vorname
        inputFirstname = new JTextField();
        inputFirstname.setText("Vorname eingeben");
        inputFirstname.setPreferredSize(new Dimension(200, 30));
        rightpanel.add(inputFirstname);
        rightpanel.add(new Box.Filler(borderMinSize, borderPrefSize, borderMaxSize));

        // ~ Nachname
        inputLastname = new JTextField();
        inputLastname.setText("Nachname eingeben");
        inputLastname.setPreferredSize(new Dimension(200, 30));
        rightpanel.add(inputLastname);
        rightpanel.add(new Box.Filler(borderMinSize, borderPrefSize, borderMaxSize));

        // ~ Straßenname
        inputStreetname = new JTextField();
        inputStreetname.setText("Straßenname eingeben");
        inputStreetname.setPreferredSize(new Dimension(200, 30));
        rightpanel.add(inputStreetname);
        rightpanel.add(new Box.Filler(borderMinSize, borderPrefSize, borderMaxSize));

        // ~ Land
        inputCountry = new JTextField();
        inputCountry.setText("Land eingeben");
        inputCountry.setPreferredSize(new Dimension(200, 30));
        rightpanel.add(inputCountry);
        rightpanel.add(new Box.Filler(borderMinSize, borderPrefSize, borderMaxSize));

        // ~ Stadt
        inputTown = new JTextField();
        inputTown.setText("Stadt eingeben");
        inputTown.setPreferredSize(new Dimension(200, 30));
        rightpanel.add(inputTown);
        rightpanel.add(new Box.Filler(borderMinSize, borderPrefSize, borderMaxSize));

        // ~ PLZ
        inputPlz = new JTextField();
        inputPlz.setText("PLZ eingeben");
        inputPlz.setPreferredSize(new Dimension(200, 30));
        rightpanel.add(inputPlz);
        rightpanel.add(new Box.Filler(borderMinSize, borderPrefSize, borderMaxSize));

        // ~ Straßennummer
        inputStreetnumber = new JTextField();
        inputStreetnumber.setText("Straßennumer eingeben");
        inputStreetnumber.setPreferredSize(new Dimension(200, 30));
        rightpanel.add(inputStreetnumber);
        rightpanel.add(new Box.Filler(borderMinSize, borderPrefSize, borderMaxSize));

        // Button zum hinzufuegen
        addButton = new JButton("Hinzufügen");
        addButton.setBackground(cb);
        addButton.setPreferredSize(new Dimension(200, 30));
        rightpanel.add(addButton);

        // Groeßere Luecke
        Dimension bMinS = new Dimension(5, 20);
        Dimension bPS = new Dimension(5, Short.MAX_VALUE);
        Dimension bMaxS = new Dimension(5, Short.MAX_VALUE);
        rightpanel.add(new Box.Filler(bMinS, bPS, bMaxS));

        // Sortierleiste der Kunden
        JLabel sort = new JLabel("Sortieren nach: ");
        rightpanel.add(sort);
        // Sortiermoeglichkeiten
        String[] options = { "Nummerisch", "Alphabetisch" };
        JComboBox<String[]> optionList = new JComboBox(options);
        // Defaultwert der Liste
        optionList.setSelectedIndex(0);
        rightpanel.add(optionList);

        rightpanel.add(new Box.Filler(bMinS, bPS, bMaxS));

        // --- LISTENER ---

        //Listener fuer logout Button
        logOut.addActionListener(logIn);
        logOut.setActionCommand("logoutEmployer");
        optionList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                // Die Elemente in der OptionList sind bei 0 beginnend,
                // somit muss i geprueft werden und auf i+1 gehandelt werden

                int col = 0;
                // optionList beginnt bei 0
                int i = optionList.getSelectedIndex();

                if (i == 0) {
                    col = 0;
                    // Sortiere nach MUN
                } else if (i == 1) {
                    col = 1;
                    // Sortieren nach Benutzernamen
                }
                sortTable(personOverView, col);
            }
        });

        // Fuege neuen Kunden in die Liste hinzu
        addButton.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent ae) 
            {
                // ? TODO: Soll hier ueberprueft werden ob der neue Customer nicht schon drin
                // ist?
                Customer newCustomer = new Customer(inputUsername.getText(), inputPassword.getText(),
                        inputFirstname.getText(), inputLastname.getText(), inputStreetname.getText(),
                        inputCountry.getText(), inputTown.getText(), Integer.parseInt(inputPlz.getText()),
                        Integer.parseInt(inputStreetnumber.getText()));
                // Wenn Customer erfolgreich hinzugefuegt wuerde, dann soll die Liste
                // aktualisiert werden.
                try {
                    if (eshop.addCustomer(newCustomer)) 
                    {
                        DefaultTableModel model = (DefaultTableModel) personOverView.getModel();
                        model.addRow(new Object[] { newCustomer.getMUN().getNumber(), newCustomer.getUsername(),
                                newCustomer.getPassword(), newCustomer.getFirstname(), newCustomer.getLastname(),
                                newCustomer.getAddress().getStreetname(), newCustomer.getAddress().getCountry(),
                                newCustomer.getAddress().getTown(), newCustomer.getAddress().getPlz(),
                                newCustomer.getAddress().getStreetnumber(), newCustomer.getDeleted() });
                    }
                } catch (CustomerAlreadyExists e) {
                    e.getMessage();
                }
            }
        });

        // Listener fuer loeschButton
        removeButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                // try {
                    // Es ist immer die Zeile mit der Mun ausgewaehlt
                int column = 0;
                // Angeklickte Zeile
                int row = personOverView.getSelectedRow();
                // Nur wenn eine Zeile angeklickt ist, dann kann entfernt weerden
                if(row >= 0)
                {
                    // Mun des Customers wird ausgelesen
                    int number = Integer.parseInt(personOverView.getModel().getValueAt(row, column).toString());
                    // Mit der Mun wird der Customer geloescht
                    // Loesche Customer aus der CustomerTabelle
                    
                    Customer toDealet = eshop.searchCustomerByNumber(number);
                    if(toDealet != null)
                    {
                        //Loesche den Customer im System
                        if (eshop.removeCustomer(toDealet.getUsername()))
                        {
                            DefaultTableModel model = (DefaultTableModel)personOverView.getModel();
                            model.setValueAt(toDealet.getDeleted(), row, 10);   
                        }
                    }
                }
                
            }
        });

        // Listener fuer logOut Button
       // logOut.addActionListener(employerGui);
       // logOut.setActionCommand("logout");


   
    //Listener such Button
    searchButton.addActionListener(new ActionListener()
    {
        public void actionPerformed(ActionEvent ae)
        {
            ArrayList<Customer> customerlist = null; 
            String searchTerm = (String)searchTextField.getText();
            //Wenn nichts im Textfeld steht, wird die gesamte Liste angezeigt
            if(searchTerm.equals(""))
            {
                System.out.println("Mitarbeiterliste");
                customerlist = eshop.getCustomerList();
            }
            //sonst wird nach dem Username gesucht
            else
            {
            customerlist = eshop.searchCustomerByUsername(searchTerm);   
            
                System.out.println("Username");
                System.out.println(searchTerm);
            }
            DefaultTableModel model = (DefaultTableModel)personOverView.getModel();
            
            //die Tabelle wird geleert
            model.setRowCount(0);
            //die Tabelle wird neu befuellt
            makeTableModel(customerlist, model);
            
        }
    });
    }

// --- Methode ---

    public TableModel makeTableModel(ArrayList<Customer> list, DefaultTableModel model)
	{
		if(model == null)
		{	
			// Definiere Kopfzeile
			String[] header =
			{
                "MUN",
				"Benutzername",
                "Passwort",
                "Vorname",
                "Nachname",
                "Straßenname",
                "Hausnummer",
                "Stadt",
                "Land",
                "PLZ",
                "Deleted"

			};
			model = new DefaultTableModel(header, 0);
		}

		// Fuelle fuer jeden LogEintrag die Attribute in die logListe
		for(Customer customer : list)
		{
			Object[] row =
			{	
				customer.getMUN().getNumber(),
				customer.getUsername(),
				customer.getPassword(),
                customer.getFirstname(),
                customer.getLastname(),
                customer.getAddress().getStreetname(),
                customer.getAddress().getStreetnumber(),
                customer.getAddress().getCountry(),
                customer.getAddress().getTown(),
                customer.getAddress().getPlz(),
                customer.getDeleted(),
			};
			model.addRow(row);
		}
		return model;
    }

    private void sortTable(JTable table, int col)
    {
        TableRowSorter<TableModel> sorter = new TableRowSorter<>(table.getModel());
        table.setRowSorter(sorter);
        ArrayList<RowSorter.SortKey> sortKeys = new ArrayList<>();
        sortKeys.add(new RowSorter.SortKey(col, SortOrder.ASCENDING));
        sorter.setSortKeys(sortKeys);
        sorter.sort();
    }

}