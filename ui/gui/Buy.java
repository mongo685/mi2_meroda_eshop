package ui.gui;

import java.awt.*;
import javax.swing.*;


public class Buy extends JFrame
{

    private static final long serialVersionUID = 1L;
    Container c;
    JLabel lab;

    public Buy()
    {

        super("KAUF");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.initialize();
        


        
    }
    private void initialize()
    {
        c = getContentPane();
        c.setLayout(new FlowLayout());
        c.setBackground(Color.RED);
        Icon bild = new ImageIcon("ui/gui/majbGUI/einkaufswagen.jpg");
        lab = new JLabel("Vielen Dank für Ihren Einkauf", bild, JLabel.CENTER);
        Font  f1  = new Font("Impact",  Font.BOLD, 30);
        lab.setFont(f1);
        lab.setHorizontalTextPosition(JLabel.CENTER);
        lab.setVerticalTextPosition(JLabel.BOTTOM);
        c.add(lab);

        this.setSize(500, 500);
        this.setVisible(true);
    }
}