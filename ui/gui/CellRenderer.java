package ui.gui;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

import java.awt.*;


public class CellRenderer extends DefaultTableCellRenderer 
{

    private static final long serialVersionUID = 1L;
    

    //Die Methode sorgt dafuer, dass in der Tabelle der Rechnung die Letzte Zeile 
    //mit dem Gesamtbetrag in einer anderen Farbe erscheint
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row,
            int column) 
            {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    
        this.setForeground(Color.BLACK);
        this.setBackground(Color.WHITE);
        if (table.getValueAt(row, 0) instanceof String){
            String w = (String)table.getValueAt(row, 0);
            if (w.equals("Gesamtbetrag")) {
              //this.setValue(table.getValueAt(row, column));
              this.setFont(this.getFont().deriveFont(Font.BOLD));
              this.setForeground(Color.RED);
              this.setBackground(new Color(200, 200, 200));
            }
        }
        return this;
    }
    }