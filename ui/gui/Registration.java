package ui.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import domain.Eshop;
import domain.exceptions.CustomerAlreadyExists;
import valueObjects.Customer;

public class Registration extends JPanel {
    private Eshop eshop;

    private JTextField usernameTextField = new JTextField("Username");
    private JTextField passwordTextField = new JTextField("Passwort");
    private JTextField firstnameTextField = new JTextField("Vorname");
    private JTextField lastnameTextField = new JTextField("Nachname");
    private JTextField streetnameTextField = new JTextField("Straßenname");
    private JTextField townTextField = new JTextField("Stadt");
    private JTextField countryTextField = new JTextField("Land");
    private JTextField plzTextField = new JTextField("PLZ");
    private JTextField streetnumberTextField = new JTextField("Hausnummer");
    private JButton confirmRegisterButton = new JButton("Registrieren");

    public Registration(Eshop eshop) {

        super();
        this.eshop = eshop;

        this.initialize();

    }

    private void initialize() {
        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new BorderLayout());

        JPanel registerPanel = new JPanel();

        GridBagLayout registerGridBagLayout = new GridBagLayout();
        registerPanel.setLayout(registerGridBagLayout);

        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 1;

        registerPanel.add(usernameTextField, c);
        c.gridx = 1;
        c.gridy = 1;

        registerPanel.add(passwordTextField, c);
        c.gridx = 0;
        c.gridy = 2;

        registerPanel.add(firstnameTextField, c);
        c.gridx = 1;
        c.gridy = 2;

        registerPanel.add(lastnameTextField, c);
        c.gridx = 0;
        c.gridy = 3;

        registerPanel.add(streetnameTextField, c);
        c.gridx = 1;
        c.gridy = 3;

        registerPanel.add(streetnumberTextField, c);
        c.gridx = 0;
        c.gridy = 4;

        registerPanel.add(townTextField, c);
        c.gridx = 1;
        c.gridy = 4;

        registerPanel.add(plzTextField, c);
        c.gridx = 0;
        c.gridy = 5;

        registerPanel.add(countryTextField, c);
        c.gridx = 0;
        c.gridy = 6;

        registerPanel.add(confirmRegisterButton, c);

        setVisible(true);

        centerPanel.add(registerPanel, BorderLayout.CENTER);
        this.add(centerPanel);

        // LISTENER Neuer Kunde wird registriert
        confirmRegisterButton.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent ae) 
            {
                Customer newCustomer = new Customer(usernameTextField.getText(), passwordTextField.getText(),
                        firstnameTextField.getText(), lastnameTextField.getText(), streetnameTextField.getText(),
                        countryTextField.getText(), townTextField.getText(), Integer.parseInt(plzTextField.getText()),
                        Integer.parseInt(streetnumberTextField.getText()));
                // Wenn Customer erfolgreich hinzugefuegt wuerde, dann soll die Liste
                // aktualisiert werden.
                try {
                    if (eshop.addCustomer(newCustomer)) {
                        JOptionPane.showMessageDialog(null, "Nutzer erfolgreich registriert");
                    }
                } catch (HeadlessException e) {
                    e.printStackTrace();
                } catch (CustomerAlreadyExists e) {
                    e.printStackTrace();
                }
             }
            });
    }




public static void main (String [] args){
    Eshop lidl = new Eshop();
  

}
}