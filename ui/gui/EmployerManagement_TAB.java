
package ui.gui;

import valueObjects.*;
import domain.Eshop;
import domain.exceptions.EmployerAlreadyExists;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class EmployerManagement_TAB extends JPanel {

  private Eshop eshop;

  private JButton addEmployerButton;
  private JTextField usernameTextField;
  private JTextField passwordTextField;
  private JTextField firstnameTextField;
  private JTextField lastnameTextField;
  private JButton logOut;

  private Employer loggedEmployer;
  private LogIn logIn ;

  public EmployerManagement_TAB(Eshop eshop, Employer loggedEmployer, LogIn logIn) 
  {
    super();
    this.eshop = eshop;
    this.loggedEmployer = loggedEmployer;
    this.logIn = logIn;
    this.initialize();
    this.setVisible(true);
  }

  private void initialize() 
  {
    Color cb = new Color(255, 244, 200);
    Font f1 = new Font("Impact", Font.BOLD, 30);

    this.setLayout(new BorderLayout());

    // Grundlegende Einstellungen
    JPanel upperPanel = new JPanel();
    upperPanel.setLayout(new BoxLayout(upperPanel, BoxLayout.LINE_AXIS));
    upperPanel.setBackground(Color.BLACK);
    upperPanel.setPreferredSize(new Dimension(200, 40));
    this.add(upperPanel, BorderLayout.NORTH);

    // Ueberschrift Mitarbeiterverwaltung
    JLabel headTitle = new JLabel();
    headTitle.setText("Mitarbeiterverwaltung");
    headTitle.setFont(f1);
    headTitle.setForeground(Color.RED);
    upperPanel.add(headTitle);

    // Luecke
    Dimension upMin = new Dimension(20, 5);
    Dimension upPre = new Dimension(Short.MAX_VALUE, 5);
    Dimension upMax = new Dimension(Short.MAX_VALUE, 5);
    upperPanel.add(new Box.Filler(upMin, upPre, upMax));

    // Setzte Log-Out Button
    logOut = new JButton("Log-Out");
    logOut.setBackground(cb);
    logOut.setPreferredSize(new Dimension(200, 30));
    upperPanel.add(logOut);

    // CENTER Employer LIST mit der Tabelle + Suche
    JPanel centerPanel = new JPanel();
    centerPanel.setLayout(new BorderLayout());

    // !Tabelle der Mitarbeiterliste
    ArrayList<Employer> employerlist = eshop.getEmployerList();
    TableModel employerModel = makeTableModelEmployer(employerlist, null);
    JTable employerTable = new JTable(employerModel) {
      // der Nutzer kann nicht in die Tabelle schreiben
      public boolean isCellEditable(int x, int y) {
        return false;
      }
    };
    // die Spalten lassen sich vom Nutzer nicht vertauschen
    employerTable.getTableHeader().setReorderingAllowed(false);
    // Tabelle ist scrollbar
    JScrollPane scrollPane = new JScrollPane(employerTable);
    employerTable.setFillsViewportHeight(true);

    // EAST_Panel für Mitarbeiter hinzufügen
    JPanel eastPanel = new JPanel();
    eastPanel.setLayout(new BorderLayout());
    eastPanel.setPreferredSize(new Dimension(500, 500));

    // ! Employer hinzufügen PANEL
    JPanel addEmployerPanel = new JPanel();
    GridBagLayout gridBagLayout = new GridBagLayout();
    addEmployerPanel.setLayout(gridBagLayout);
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridy = 0; // Zeile 0
    c.gridy = 1; // Zeile 1

    JLabel employerLabel = new JLabel("Neuen Mitarbeiter hinzufügen");
    c.gridx = 0; // Spalte 0
    c.weightx = 1.0; // 100% der gesamten Breite
    gridBagLayout.setConstraints(employerLabel, c);
    addEmployerPanel.add(employerLabel);

    c.gridy = 2; // Zeile 2

    usernameTextField = new JTextField("Username");
    c.weightx = 1.0; // 100% der gesamten Breite
    gridBagLayout.setConstraints(usernameTextField, c);
    addEmployerPanel.add(usernameTextField);

    passwordTextField = new JTextField("Passwort");
    c.gridy = 3; // Zeile 3
    c.weightx = 1.0; // 100% der gesamten Breite
    gridBagLayout.setConstraints(passwordTextField, c);
    addEmployerPanel.add(passwordTextField);

    c.gridy = 4; // Zeile 4

    firstnameTextField = new JTextField("Vorname");
    c.weightx = 1.0; // 100% der gesamten Breite
    gridBagLayout.setConstraints(firstnameTextField, c);
    addEmployerPanel.add(firstnameTextField);

    c.gridy = 5; // Zeile 5

    lastnameTextField = new JTextField("Nachname");
    c.weightx = 1.0; // 100% der gesamten Breite
    gridBagLayout.setConstraints(lastnameTextField, c);
    addEmployerPanel.add(lastnameTextField);

    c.gridy = 6; // Zeile 6

    addEmployerButton = new JButton("Hinzufügen");
    c.weightx = 1.0; // 100% der gesamten Breite
    gridBagLayout.setConstraints(addEmployerButton, c);
    addEmployerPanel.add(addEmployerButton);

    eastPanel.add(addEmployerPanel, BorderLayout.NORTH);

    // SOUTH SEARCH +- remove
    JPanel searchPanel = new JPanel();
    GridBagLayout searchGridBagLayout = new GridBagLayout();
    searchPanel.setLayout(searchGridBagLayout);
    GridBagConstraints d = new GridBagConstraints();
    d.fill = GridBagConstraints.HORIZONTAL;
    d.gridy = 0; // Zeile 0

    JTextField searchTextField = new JTextField();
    d.gridx = 0; // Spalte 0
    d.weightx = 3.0;

    // gridBagLayout.setConstraints(searchTextField, d);
    searchPanel.add(searchTextField, d);

    JButton searchButton = new JButton("Suchen");
    d.gridx = 1;
    d.weightx = 0.3;
    // gridBagLayout.setConstraints(searchButton, d);
    searchPanel.add(searchButton, d);

    JButton removeButton = new JButton("Entfernen");
    d.gridx = 4;
    d.weightx = 0.3;
    // gridBagLayout.setConstraints(removeButton, d);
    searchPanel.add(removeButton, d);

    // ! CenterPanel zsm setzten aus scrollPane und searchPanel
    centerPanel.add(scrollPane, BorderLayout.CENTER);
    centerPanel.add(searchPanel, BorderLayout.SOUTH);

    // ! alles zsmfügen alle LAYOUTS
    this.add(centerPanel, BorderLayout.CENTER);
    this.add(eastPanel, BorderLayout.EAST);

    // ----- Listener ------
    // Listener fuer logout Button
    
    //Listener fuer logout Button
    logOut.addActionListener(logIn);
    logOut.setActionCommand("logoutEmployer");

    // Listener fuer den Hinzufuegen Button
    // ein Mitarbeiter wird der Mitarbeiterliste hinzugefügt
    addEmployerButton.addActionListener(new ActionListener() 
    {
      public void actionPerformed(ActionEvent ae) 
      {

        System.out.println("TEST");
        String username = usernameTextField.getText();
        String password = passwordTextField.getText();
        String firstname = firstnameTextField.getText();
        String lastname = lastnameTextField.getText();

        Employer newEmployer = new Employer(username, password, firstname, lastname);
        
        try {

          if (eshop.addEmployer(newEmployer)) 
          {
              System.out.println("der neue Mitarbeiter mit den Daten :" + username + password + firstname + lastname
              + "wurde erfolgreich hinzugefügt!");

              DefaultTableModel model = (DefaultTableModel) employerTable.getModel();
              model.addRow(new Object[] { newEmployer.getMUN().getNumber(), username, password, firstname, lastname,
              newEmployer.getDeleted() });
              newEmployer = null;
          } else {
              System.out.println("Beim hinzufügen ist ein Fehler aufgetreten");
          }

        } catch (EmployerAlreadyExists e) {
            e.getMessage();
        }

      }
    }); 
    

//! TODO: Alle Fehlermeldungen in den GUI's sollen als Dialog dargestellt werden.


      //Listener fuer Loeschen Button
        //beim Betaetigen des Loeschen Buttons
        //wird das ausgewaehlte Produkt aus dem Warenkorb und der Tabelle entfernt
        removeButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                    int column = 0;
                    //row ist die angeklickte Zeile
                    int row = employerTable.getSelectedRow();
                    //nur wenn eine Zeile angeklickt ist wird das folgende ausgefuehrt
                    if(row>=0)
                    {
                        //die MUN  mder Vorname und Nachname des Mitarbeiters werden ausgelesen
                        int mun = Integer.parseInt(employerTable.getModel().getValueAt(row,0).toString());
                        String username = employerTable.getModel().getValueAt(row,1).toString(); 
                        Employer delEmployer = eshop.searchEmployerByNumber(mun);
                        //der Mitarbeiter wird auf der Liste als gelöscht gekennzeichnet
                       
                        if (eshop.removeEmployer(username))
                        {
                          DefaultTableModel model = (DefaultTableModel)employerTable.getModel();
                          model.setValueAt(delEmployer.getDeleted(), row, 5);
                           System.out.println("Der Mitarbeiter mit dem Username " + delEmployer.getUsername() + " wurde gelöscht" );
                       }
                     }
              } 
        });
         
      

      //Listener such Button
     searchButton.addActionListener(new ActionListener()
     {
         public void actionPerformed(ActionEvent ae)
         {
             ArrayList<Employer> employerlist = null; 
             String searchTerm = (String)searchTextField.getText();
             //Wenn nichts im Textfeld steht, wird die gesamte Liste angezeigt
             if(searchTerm.equals(""))
             {
                 System.out.println("Mitarbeiterliste");
                 employerlist = eshop.getEmployerList();
             }
             //sonst wird nach dem Username gesucht
             else
             {
              employerlist = eshop.searchEmployerByUsername(searchTerm);    
              
                System.out.println("Username");
                 System.out.println(searchTerm);
             }
             DefaultTableModel model = (DefaultTableModel)employerTable.getModel();
            
             //die Tabelle wird geleert
             model.setRowCount(0);
             //die Tabelle wird neu befuellt
             makeTableModelEmployer(employerlist, model);
             
         }
     });
    }
 
       //--- Methoden --- 

       
      // Erstellt Mitarbeiterliste
      public TableModel makeTableModelEmployer( ArrayList<Employer> list, DefaultTableModel model)
      {
        if (model == null)
        {
           
          //Kopfzeile der Tabelle
            String[] header = 
            {
                "MUN",  
                "Username",
                "Passwort",
                "Vorname",
                "Nachname",
                "Deleted",
                
            };
                model = new DefaultTableModel(header, 0);
        }
        
  
        for (Employer e : list)
        {   
              Object[] row = 
              {
                  e.getMUN().getNumber(),
                  e.getUsername(),
                  e.getPassword(),
                  e.getFirstname(),
                  e.getLastname(),
                  e.getDeleted(),
              };
              model.addRow(row);
          }
      
      return model;
    }


      

        public static void main (String [] args){
        Eshop eshop = new Eshop(); 
        }
    }
    


