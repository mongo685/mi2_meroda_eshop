package ui.gui;

import domain.Eshop;
import domain.exceptions.*;
import valueObjects.BulkProduct;
import valueObjects.Customer;
import valueObjects.Product;
import valueObjects.ProductQuantity;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;


public class GuiCustomer extends JPanel implements ListSelectionListener
{

    private JTable basketTable;
    private JTable productTable;
    private JButton plus;
    private JButton minus;
    private JButton logOut;
    private JButton addButton;
    private JButton searchButton;
    private JButton remove;
    private JButton buyButton;
    private JTextField nameTextField;
    private JTextField quantityTextField;

    private Eshop eshop;
    private Customer customer;
    private GuiBuy guiBuy;
    private LogIn logIn;
    
    public GuiCustomer(Eshop eshop, Customer customer, GuiBuy guiBuy, LogIn logIn) 
    {
        super();
        this.eshop = eshop;
        this.customer = customer;
        this.guiBuy = guiBuy;
        this.logIn = logIn;
        this.initialize();
    }

 
    
   

    
    private void initialize()
    {   
        //Farbe der Buttons
        Color cb = new Color(255, 244, 200);
        //Hintergrundfarbe
        Color ch = new Color(255, 200, 0);

        //Schriftarten
        Font  f1  = new Font("Impact",  Font.BOLD, 30);
        Font  f2  = new Font("Impact",  Font.BOLD, 20);

        Font  fa  = new Font(Font.DIALOG,  Font.BOLD, 30);
        Font  fb  = new Font(Font.SERIF, Font.PLAIN,  30);
        Font  fc  = new Font("TimesRoman", Font.BOLD | Font.ITALIC, 30);

        //Luecken zwischen den Buttons
        Dimension buMin = new Dimension(1, 1);
        Dimension buP = new Dimension(1, 1);
        Dimension buMax = new Dimension(1, 1);
        
        
        //Layout ganz außen drum
        this.setLayout(new BorderLayout());
        

        //--------Layout  Oben--------
        JPanel upPanel = new JPanel();
        upPanel.setLayout(new BoxLayout(upPanel, BoxLayout.LINE_AXIS));
        this.add(upPanel, BorderLayout.NORTH);
        upPanel.setBackground(Color.BLACK);

        //Ueberschrift Onlineshop
        JLabel heading = new JLabel();
        heading.setText("Onlineshop");
        heading.setFont(f1);
        //Schriftfarbe
        heading.setForeground(Color.RED);
        upPanel.setPreferredSize(new Dimension(200, 40));
        upPanel.add(heading);
        //Luecke
        Dimension upMin = new Dimension(20,5);
        Dimension upPre = new Dimension(Short.MAX_VALUE,5);
        Dimension upMax = new Dimension(Short.MAX_VALUE,5);
        upPanel.add(new Box.Filler(upMin, upPre, upMax));
        //log out Button
        logOut = new JButton("logout");
        logOut.setBackground(cb);
        logOut.setPreferredSize(new Dimension(200, 30));
        upPanel.add(logOut);
    

     
        


        //----------Layout auf der Linken Seite---------
        JPanel leftPanel = new JPanel();
        leftPanel.setLayout(new BorderLayout());
        //leftPanel.setOpaque(true);
        //Hintergrundfarbe
        leftPanel.setBackground(ch);
        this.add(leftPanel, BorderLayout.CENTER);

        JLabel prodList = new JLabel("Artikelliste");
        prodList.setFont(f2);
        leftPanel.add(prodList, BorderLayout.NORTH);

        
        JPanel centerPanel = new JPanel();
        JPanel centerPanel1 = new JPanel();
        JPanel centerPanel1a = new JPanel();
        JPanel centerPanel1b = new JPanel();
        JPanel centerPanel2 = new JPanel();
        centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
        centerPanel1.setLayout(new BorderLayout());
        centerPanel1a.setLayout(new GridLayout(2,2));
        centerPanel1b.setLayout(new BoxLayout(centerPanel1b, BoxLayout.Y_AXIS));
        centerPanel2.setLayout(new BoxLayout(centerPanel2, BoxLayout.Y_AXIS));
        centerPanel.add(centerPanel1);
        centerPanel.add(centerPanel2);
        centerPanel1.add(centerPanel1a, BorderLayout.CENTER);
        centerPanel1.add(centerPanel1b, BorderLayout.EAST);

        //centerPanel.setOpaque(true);
        //Hintergrundfarbe
        centerPanel.setBackground(ch);
        leftPanel.add(centerPanel, BorderLayout.CENTER);


        // Abstandhalter ("Filler") zwischen Rand und erstem Element
        Dimension borderMinSize = new Dimension(5, 10);
        Dimension borderPrefSize = new Dimension(5, 10);
        Dimension borderMaxSize = new Dimension(5, 10);
        


        JLabel name = new JLabel("Artikelname: ");
        centerPanel1a.add(name);
        //Textfeld um Produktnamen einzutragen
        nameTextField = new JTextField();
        nameTextField.setPreferredSize(new Dimension(200, 20));
        centerPanel1a.add(nameTextField);
       


        JLabel quantity = new JLabel("Anzahl: ");
        centerPanel1a.add(quantity);
        //Textfeld, um Anzahl des Produktes einzutragen
        quantityTextField = new JTextField();
        quantityTextField.setPreferredSize(new Dimension(200, 20));
        centerPanel1a.add(quantityTextField);
        

        //Button, um Produkt dem Warenkorb hinzuzufuegen
        addButton = new JButton("Hinzufügen");
        addButton.setBackground(cb);
        addButton.setPreferredSize(new Dimension(100, 100));
        centerPanel1b.add(addButton);

        //Sortiermoeglichkeiten
        String[] options = { "Artikelname", "Preis", "Artikelnummer" };
        JComboBox<String[]> optionList = new JComboBox(options);
        optionList.setSelectedIndex(0);
        centerPanel2.add(optionList);
       
        


        //Tabelle fuer die Artikelliste wird erstellt
        ArrayList<Product> productlist = eshop.productlistSortName(); 
        TableModel productModel = makeTableModel(productlist, null);
        productTable = new JTable(productModel) {
            //der Nutzer kann nicht in die Tabelle schreiben
            public boolean isCellEditable(int x, int y){
                return false;
            }
        };
        productTable.getSelectionModel().addListSelectionListener(this);
        //die Spalten lassen sich vom Nutzer nicht vertauschen
        productTable.getTableHeader().setReorderingAllowed(false);
        //Tabelle ist scrollbar
        JScrollPane scrollPane = new JScrollPane(productTable);
        productTable.setFillsViewportHeight(true);
        centerPanel2.add(scrollPane);



        //Suchen
        JPanel searchPanel = new JPanel();
        searchPanel.setLayout(new GridLayout());
        leftPanel.add(searchPanel, BorderLayout.SOUTH);

        JTextField searchTextField = new JTextField();
        searchPanel.add(searchTextField);

        searchButton = new JButton("Suchen");
        searchButton.setBackground(cb);
        searchPanel.add(searchButton);


        



        //--------Layout auf der rechten Seite-----------
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BorderLayout());
        rightPanel.setBackground(ch);
        this.add(rightPanel, BorderLayout.EAST);

        //Bereich fuer Einstellungen im Warenkorb
        JPanel basketControl = new JPanel();
        basketControl.setLayout(new GridLayout());
        rightPanel.add(basketControl, BorderLayout.SOUTH);

        //Anzahl eines Produktes im Warenkorb erhöhen
        plus = new JButton(" + ");
        plus.setBackground(cb);
        basketControl.add(plus);

        //Anzahl eines Produktes im Warenkorb verringern
        minus = new JButton(" - ");
        minus.setBackground(cb);
        basketControl.add(minus);

        //Produkt aus dem Warenkorb entfernen
        remove = new JButton(" LÖSCHEN ");
        remove.setBackground(cb);
        basketControl.add(remove);

        //Warenkorb kaufen
        buyButton = new JButton(" KAUFEN ");
        buyButton.setBackground(cb);
        basketControl.add(buyButton);

        //Tabelle des Warenkorbs erstellen
        ArrayList<ProductQuantity> basketlist = eshop.getBasket(customer);
        TableModel basketModel = basketTableModel(basketlist);
        basketTable = new JTable(basketModel) {
            //der Nutzer kann nicht in die Tabelle schreiben
            public boolean isCellEditable(int x, int y){
                return false;
            }
        };
        //Spalten der Tabelle koennen vom Nutzer nicht vertauscht werden
        basketTable.getTableHeader().setReorderingAllowed(false);
        //Tabelle ist scrollbar
        JScrollPane basketPane = new JScrollPane(basketTable);
        basketTable.setFillsViewportHeight(true);
        ListSelectionModel selectionModel = basketTable.getSelectionModel();
        checkButton();
        //prueft ob eine Zeile im Warenkorb angeklickt ist 
        //nur dann die + - und loeschen Buttons aktivieren
        selectionModel.addListSelectionListener(new ListSelectionListener() 
        {
            public void valueChanged(ListSelectionEvent e) 
            {
                checkButton();
            }
        });
        rightPanel.add(basketPane, BorderLayout.CENTER);


        //Ueberschrift Warenkorb
        JLabel basket = new JLabel("Warenkorb");
        basket.setFont(f2);
        rightPanel.add(basket, BorderLayout.NORTH);
        
        //-----------LISTENER--------


        //Listener fuer Sortieroptionen der Artikelliste
        //die Artikelliste kann nach Name Nummer und Preis sortiert werden
        optionList.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                int col = 1;
                int i = optionList.getSelectedIndex();
                //im folgenden wird immer von 0 an gezaehlt
                //wenn in den Sortieroptionen Artikelname ausgewaehlt ist,
                //also Zeile 0, wird nach Spalte 1 in der Tabelle sortiert
                //was ebenfalls Artikelname ist
                if(i==0)
                {
                    col = 1;
                //nach Preis sortieren
                } else if (i==1)
                {
                    col = 3;
                //nach Artikelnummer sortieren
                } else if (i==2)
                {
                    col =0;
                }
                sortTable(productTable, col);
            }

        });

        //Listener fuer den Hinzufuegen Button
        //ein Produkt wird dem Warenkorb hinzugefuegt
        addButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                try{
                    String name = nameTextField.getText();
                    //wenn das Produkt noch nicht dem Warenkorb hinzugefuegt wurde
                    //wird es ihm hinzugefuegt
                   
                    
                    Product product = eshop.searchProduct(name); 
                    int quantity = Integer.parseInt(quantityTextField.getText());
                    if(eshop.putBasket(customer, product, quantity))
                    {
                        int index = searchB(name);
                        DefaultTableModel model = (DefaultTableModel)basketTable.getModel();
                        if(index==-1)
                        {
                            System.out.println("Produkt " + product.getName() + " erfolgreich in den Warenkorb hinzugefuegt.");
                            model.addRow(new Object[]{product.getName(), quantity, Eshop.convertPrice(product.getPrice()) });
                            //Wenn man es sortiert funktioniert + - und löschen nicht mehr richtig
                            //sortBasketTable();
                        }
                        else 
                        {
                            System.out.println("Produkt " + product.getName() + " im Warenkorb erhöht.");
                            int q = (int)model.getValueAt(index, 1);
                            q = q + quantity;
                            model.setValueAt(q, index, 1);
                        }
                    }
                    
                    

                }catch (StockNotEnoughException e){
                    System.out.println(e.getMessage());
                    //Zeigt Fehlermeldung in einem Fenster
                    JOptionPane.showConfirmDialog(
                        guiBuy,
                        e.getMessage(),
                        "Fehler",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.ERROR_MESSAGE
                    );
 
                }
                catch (WrongAmountBulkException | ProductDoesntExistException e)  
                {
                    System.out.println(e.getMessage()); 
                    //zeigt Fehlermeldung in einem Fenster
                    JOptionPane.showConfirmDialog(
                        guiBuy,
                        e.getMessage(),
                        "Fehler",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.ERROR_MESSAGE
                    );
                }
            }
        });

        

        //Listener fuer Kaufen Button
        buyButton.addActionListener(guiBuy);
        buyButton.setActionCommand("KAUFEN");
        //Listener fuer logout Button
        logOut.addActionListener(logIn);
        logOut.setActionCommand("logout");





        //Listener fuer + Button
        //Anzahl eines Produktes wird im Warenkorb um eins oder die Packungsgroeße erhoeht
        plus.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
            

                try 
                {
                    int column = 0;
                    int row = basketTable.getSelectedRow();
                    //wenn eine Zeile in der Tabelle des Warenkorbs angeklicht ist
                    //wird nachfolgendes ausgefuehrt
                    if(row>=0)
                    {

                    
                        String value = basketTable.getModel().getValueAt(row, column).toString();
                        Product product = eshop.searchProduct(value); 
                        //bei normalen Produkten wird eins hinzugefuegt
                        int quantity = 1;
                        //bei Bulkproduct wird eine Packung hinzugefuegt
                        if(product instanceof BulkProduct)
                        {
                            BulkProduct bp = (BulkProduct) product;
                            quantity = bp.getPackSize();
                        }

                        if(eshop.putBasket(customer, product, quantity))
                        {
                            DefaultTableModel model = (DefaultTableModel)basketTable.getModel();
                            model.setValueAt(eshop.getQuantity(customer, product), row, 1);
                            System.out.println("Produkt " + product.getName() + " erfolgreich Menge erhöht.");
                        }
                    }
                }
                catch (StockNotEnoughException e)
                {
                    System.out.println(e.getMessage());  
                    JOptionPane.showConfirmDialog(
                        guiBuy,
                        e.getMessage(),
                        "Fehler",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.ERROR_MESSAGE
                    );
                 
                }
                catch (WrongAmountBulkException | ProductDoesntExistException e)  
                {
                   System.out.println(e.getMessage()); 
                   JOptionPane.showConfirmDialog(
                        guiBuy,
                        e.getMessage(),
                        "Fehler",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.ERROR_MESSAGE
                    );
                  
                }
            }

        });

        //Listener fuer - Button
        //Anzahl eines Produktes wird im Warenkorb um eins oder die Packungsgroeße verringert
        minus.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
            

                try 
                {
                    int column = 0;
                    int row = basketTable.getSelectedRow();
                    //wenn eine Zeile in der Tabelle des Warenkorbs angeklicht ist
                    //wird nachfolgendes ausgefuehrt
                    if(row>=0)
                    {

                    
                        String value = basketTable.getModel().getValueAt(row, column).toString();
                        Product product = eshop.searchProduct(value); 
                        //bei normalen Produkten wird eins entfernt
                        int quantity = 1;
                        //bei Bulkproduct wird eine Packung entfernt
                        if(product instanceof BulkProduct)
                        {
                            BulkProduct bp = (BulkProduct) product;
                            quantity = bp.getPackSize();
                        }

                        if(eshop.reduceBasket(customer, product, quantity))
                        {
                            DefaultTableModel model = (DefaultTableModel)basketTable.getModel();
                            model.setValueAt(eshop.getQuantity(customer, product), row, 1);
                            System.out.println("Produkt " + product.getName() + " erfolgreich Menge verringert.");
                        }
                    }
                }
            
                catch(ProductDoesntExistException e)
                {
                    System.out.println(e.getMessage());
                    JOptionPane.showConfirmDialog(
                        guiBuy,
                        e.getMessage(),
                        "Fehler",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.ERROR_MESSAGE
                    );
                    
                } 
            }    

        });

        //Listener fuer Loeschen Button
        //beim Betaetigen des Loeschen Buttons
        //wird das ausgewaehlte Produkt aus dem Warenkorb und der Tabelle entfernt
        remove.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                try
                {
                    int column = 0;
                    //row ist die angeklickte Zeile
                    int row = basketTable.getSelectedRow();
                    //nur wenn eine Zeile angeklickt ist wird das folgende ausgefuehrt
                    if(row>=0)
                    {
                        //der Name des Produktes wird ausgelesen 
                        //und das Produkt aus dem Basket entfernt
                        String value = basketTable.getModel().getValueAt(row, column).toString();
                        //das Produkt wird aus der Basket Tabelle entfernt
                        Product product = eshop.searchProduct(value); 
                        if (product != null){
                            eshop.removeInBasket(customer, product);
                            DefaultTableModel model = (DefaultTableModel)basketTable.getModel();
                            model.removeRow(row);
                            System.out.println("Produkt wurde entfernt " + product.getName() + " " + row);
                        }
                    }

                }
                catch(ProductDoesntExistException e)
                {
                    System.out.println(e.getMessage()); 
                    JOptionPane.showConfirmDialog(
                        guiBuy,
                        e.getMessage(),
                        "Fehler",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.ERROR_MESSAGE
                    ); 
                    
                } 

            }
        });

        //Listener such Button
        searchButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                ArrayList<Product> productlist = null; 
                String searchTerm = (String)searchTextField.getText();
                //wenn ein Punkt oder Komma im Textfeld ist, wird nach einem Preis gesucht
                if (searchTerm.indexOf(",") > -1)
                {
                    searchTerm = searchTerm.replace(",", ".");
                }
                if(searchTerm.indexOf(".") > -1)
                {
                    Double price = Double.parseDouble(searchTerm);
                    productlist = eshop.searchProductsPrice(price);
                    System.out.println("Preis");
                }
                //Wenn nichts im Textfeld steht, wird die gesamte Liste angezeigt
                else if(searchTerm.equals(""))
                {
                    System.out.println("Artikelliste");
                    productlist = eshop.getProductList();
                }
                //sonst wird nach dem Produktnamen gesucht
                else
                {
                    productlist = eshop.searchProducts(searchTerm);
                    System.out.println("Name");
                    System.out.println(searchTerm);

                    if(productlist.size()==0)
                    {
                        JOptionPane.showConfirmDialog(
                            guiBuy,
                            "Es wurde kein Produkt mit diesem Namen gefunden.",
                            "Fehler",
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.ERROR_MESSAGE
                        );
                    }
                }
                DefaultTableModel model = (DefaultTableModel)productTable.getModel();
               
                //die Tabelle wird geleert
                model.setRowCount(0);
                //die Tabelle wird neu befuellt
                makeTableModel(productlist, model);
                
            }
        });
        
    }

    //----------METHODEN-------

    //Tabelle der Artikelliste erstellen
    public TableModel makeTableModel( ArrayList<Product> list, DefaultTableModel model)
    {
        if (model == null){
            //Kopfzeile der Tabelle
            String[] header = 
            {
                "Artikelnummer",
                "Artikelname",
                "Packungsgröße",
                "Preis",
            };
                model = new DefaultTableModel(header, 0);
        }
        //fuer jedes Produkt in der Produktliste wird 
        //Nummer, Name, Packungsgroesse und Preis in die Tabelle eingetragen
        for (Product p : list)
        {   
            if(!p.getDeleted()) 
            {
                //wenn das Produkt kein BulkProduct ist, wird als Packungsgroesse eins eingetragen
                String packSize = "1";
                if(p instanceof BulkProduct)
                {
                    BulkProduct bp = (BulkProduct) p;
                    packSize = "" + bp.getPackSize();
                }
                String price = Eshop.convertPrice(p.getPrice());
                Object[] row = 
                {
                    "" + p.getMUN().getNumber(),
                    p.getName(),
                    packSize,
                    price
                };
                model.addRow(row);
            }
        }
        return model;
    }

     //Tabelle fuer Warenkorb erstellen
     public TableModel basketTableModel( ArrayList<ProductQuantity> list)
     {
         //Kopfzeile der Tabelle
         String[] header = 
         {
             "Artikelname",
             "Stückzahl",
             "Preis pro Stück",
         };
        DefaultTableModel model = new DefaultTableModel(header, 0);
        //fuer jedes Produkt im Warenkorb wird 
        //Name, Anzahl und Preis(pro Stueck) in die Tabelle eingetragen
        for (ProductQuantity pq : list)
        {   if(!pq.getProduct().getDeleted())
             {
                String quantity = "" + pq.getQuantity();
                String price = Eshop.convertPrice(pq.getProduct().getPrice());
                Object[] row = 
                {
                    pq.getProduct().getName(),
                    quantity,
                    price
                };
                model.addRow(row);
             }
         }
         return model;
     }

    //sortiert die Artikelliste nach ausgewaehltem Wert
    private void sortTable(JTable table, int col)
    {
        TableRowSorter<TableModel> sorter = new TableRowSorter<>(table.getModel());
        table.setRowSorter(sorter);
        System.out.println(col);
        ArrayList<RowSorter.SortKey> sortKeys = new ArrayList<>();
        sortKeys.add(new RowSorter.SortKey(col, SortOrder.ASCENDING));
        sorter.setSortKeys(sortKeys);
        //fuer Artikelnummer und Preis wird eine Zahlenvergleichmethode angewendet
        if(col==0 || col==3)
        {
            DecimalSorter comp = new DecimalSorter();
            sorter.setComparator(col, comp);

        }
        sorter.sort();
    }
    //die Tabelle des Warenkorbs wird sortiert nach Namen
    private void sortBasketTable()
    {
        TableRowSorter<TableModel> sorter = new TableRowSorter<>(basketTable.getModel());
        basketTable.setRowSorter(sorter);
        System.out.println(0);
        ArrayList<RowSorter.SortKey> sortKeys = new ArrayList<>();
        sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
        sorter.setSortKeys(sortKeys);
        
        sorter.sort();
    }

    //prueft, ob eine Zeile im Warenkorb angeklickt ist,
    //wenn nicht sind die Buttons + - und loeschen deachtiviert
    private void checkButton()
    {
        if(basketTable.getSelectedRow()>=0)
        {
            //Buttons werden aktiviert
            plus.setEnabled(true);
            minus.setEnabled(true);
            remove.setEnabled(true);
        }
        else
        {
            //Buttons werden deaktiviert
            plus.setEnabled(false);
            minus.setEnabled(false);
            remove.setEnabled(false);
        }
    }

    //untersucht, ob das Produkt schon in der angezeigten Warenkorb liste ist
    //wenn es dort ist, wird die Zeile zurueckgegeben
    //wenn es dort nicht ist wird -1 zurueckgegeben
    private int searchB(String name)
    {
        DefaultTableModel model = (DefaultTableModel)basketTable.getModel();
        for (int i = 0; i < model.getRowCount(); i++){
            String n = (String)model.getValueAt(i, 0);
            if (n.equals(name)){
                return i;
            }
        }
        return -1;
    }

    



    public static void main(String[] args)
    {
        //Eshop lidl = new Eshop();
        //GuiCustomer gc = new GuiCustomer(lidl);
        //Buy b = new Buy();
        
    }

    /**
     * Methode schreibt Namen des in der Produktliste angeklickten Procukts
     * in das Textfeld
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        int column = 1;
        int row = productTable.getSelectedRow();

        if(row>=0)
        {
            String name = productTable.getModel().getValueAt(row, column).toString();
            nameTextField.setText(name);
        }

    }

    /**
     * Methode leert die Warenkorbliste und die Textfelder
     */
    public void back()
    {
        DefaultTableModel model = (DefaultTableModel)basketTable.getModel();
        //die Tabelle wird geleert
        model.setRowCount(0);
        //Texfelder leeren
        nameTextField.setText("");
        quantityTextField.setText("");
    }

}