package ui.gui;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.WindowConstants;

import domain.Eshop;

import valueObjects.*;

public class OneFrameGUI extends JFrame 
{
private JTabbedPane tabbedPane;
private Eshop eshop;
private Employer loggedEmployer;
private Customer loggedCustomer;    

private static final long serialVersionUID = 1L;

public OneFrameGUI(Eshop eshop)
{
    super("Onlineshop Mitarbeiteransicht");
    this.eshop = eshop;
    this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    this.initialize();
}

private void initialize() {

// Erzeuge Tab-Leiste für das Fenster
tabbedPane = new JTabbedPane();
this.add(tabbedPane);

 // Lege Instanz von Login an
// LogIn login_Tab = new LogIn(eshop,this);
 // Fuege Login der Tab-Leiste hinzu
 //tabbedPane.add("Login", login_Tab);

 // Lege Instanz von Register an
 //Registration register_Tab = new Registration(eshop,this);
 // Fuege Login der Tab-Leiste hinzu
 //tabbedPane.add("Register", register_Tab);
 tabbedPane.setSelectedIndex(0);





// Generelle Konfigurationen für das Fenster
        // Setze Fenster auf fullscreen
        this.setExtendedState(Frame.MAXIMIZED_BOTH);
        this.pack();
        this.setVisible(true);
}



public static void main(String[] args)
{
   Eshop eshop = new Eshop();
   OneFrameGUI ofg = new OneFrameGUI(eshop);
}



}