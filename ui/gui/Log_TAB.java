package ui.gui;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import domain.Eshop;
import domain.logging.LogEntry;
import valueObjects.Employer;
import java.awt.BorderLayout;
import java.util.ArrayList;

import java.awt.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.Dimension;

import java.awt.event.*;

public class Log_TAB extends JPanel
{  
	private static final long serialVersionUID = 1L;

	private JButton logOut;

	private JTable logTable;

	private JTextField searchTextField;
	private JButton searchButton;

	private Eshop eshop;
	private Employer loggedEmployer;
	private LogIn logIn;

	// Klassen eigener Konstruktor
	public Log_TAB(Eshop eshop, Employer loggedEmployer, LogIn logIn)
	{
		super();
		this.loggedEmployer = loggedEmployer;
		this.eshop = eshop;
		this.logIn = logIn;
		this.initialize();
	}

	// Definiert welche Komponenten erzeugt werden sollen & mit welchen Einstellungen
	private void initialize()
	{	
		// Farbe der Buttons
        Color cb = new Color(255, 244, 200);

        //Schriftarten
        Font f1 = new Font("Impact",  Font.BOLD, 30);

        // Aueßeres Layout
        this.setLayout(new BorderLayout());
		
	// --- Obere Leiste ---

		// Grundlegende Einstellungen
		JPanel upperPanel = new JPanel();
		upperPanel.setLayout(new BoxLayout(upperPanel, BoxLayout.LINE_AXIS));
		upperPanel.setBackground(Color.BLACK);
		upperPanel.setPreferredSize(new Dimension(200, 40));
		this.add(upperPanel, BorderLayout.NORTH);

		// Ueberschrift Ereignisuebersicht
		JLabel headTitle = new JLabel();
		headTitle.setText("Ereignisübersicht");
		headTitle.setFont(f1);
		headTitle.setForeground(Color.RED);
		upperPanel.add(headTitle);

		// Luecke
		Dimension upMin = new Dimension(20,5);
        Dimension upPre = new Dimension(Short.MAX_VALUE,5);
        Dimension upMax = new Dimension(Short.MAX_VALUE,5);
		upperPanel.add(new Box.Filler(upMin, upPre, upMax));
		
		// Setzte Log-Out Button
        logOut = new JButton("Log-Out");
        logOut.setBackground(cb);
        logOut.setPreferredSize(new Dimension(200, 30));
        upperPanel.add(logOut);

	// --- Mittlere Uebersicht ---

		// Erzeuge Panel, wo alle Elemente drauf sollen
		JPanel middelPanel = new JPanel();
		middelPanel.setLayout(new BorderLayout());
		this.add(middelPanel, BorderLayout.CENTER);

		// Daten die aus dem System in die Tabelle Ueberfuehrt werden
		ArrayList<LogEntry> logEntryList = eshop.getLog().getLogList();
		TableModel logModel = makeTableModel(logEntryList, null);

		// Erstelle Tabelle mit vordefinierten Einstellungen
		logTable = new JTable(logModel)
		{
			// Nutzer soll Tabelle nicht bearbeiten koennen
			@Override
			public boolean isCellEditable(int x, int y)
			{
				return false;
			}
		};

		// Spalten sollen vom Nutzer nich geaendert werden koennen
		logTable.getTableHeader().setReorderingAllowed(false);

		// Gebe Tabelle Faheigkeit scrollbar zu sein
		JScrollPane scrollPane = new JScrollPane(logTable);
		logTable.setFillsViewportHeight(true);
		middelPanel.add(scrollPane, BorderLayout.CENTER);
	
	// --- Untere Leiste ---

		JPanel downerPanel = new JPanel();
		downerPanel.setLayout(new BoxLayout(downerPanel, BoxLayout.PAGE_AXIS));
		this.add(downerPanel, BorderLayout.SOUTH);
		
		//- der untere Block mit der sortierleiste
		JPanel sortPanel = new JPanel();
		sortPanel.setLayout(new GridLayout());
		downerPanel.add(sortPanel);
		
		//- abstand zwischen den Elementen
		Dimension bMinS = new Dimension(5, 5);
		Dimension bPS = new Dimension(5, 5);
		Dimension bMaxS = new Dimension(5, 5);
		downerPanel.add(new Box.Filler(bMinS, bPS, bMaxS));

		//- der obere Block mit der Suchleiste
		JPanel searchPanel = new JPanel();
		searchPanel.setLayout(new GridLayout());
		downerPanel.add(searchPanel);
		
		searchTextField = new JTextField();
		searchPanel.add(searchTextField);
		
		searchButton = new JButton("Suchen");
		searchButton.setBackground(cb);
		searchPanel.add(searchButton);
		
		// Sortierleiste der Logs
		JLabel sort = new JLabel("Sortieren nach:");
		sortPanel.add(sort);
		// Sortiermoeglickeiten
		String[] optins = 
		{
			"Nummerisch nach Anzahl",
			"Alphabetisch"
		};
		JComboBox<String[]> optionList = new JComboBox(optins);
		// Defaultwerte der Liste
		optionList.setSelectedItem(0);

		sortPanel.add(optionList);
	

// --- Listener ---

	//Listener fuer logout Button
    logOut.addActionListener(logIn);
    logOut.setActionCommand("logoutEmployer");


	// Listener fuer Sortieroptionen der LogListe
	// Die LogListe kann nach Alphabetisch und Nummerisch sortiert werden
	optionList.addActionListener(new ActionListener()
	{
		public void actionPerformed(ActionEvent e)
		{
			int col = 1;
			int i = optionList.getSelectedIndex();
			// Es wird immer die erste Option, somit die Nummerische Sortierung
			// gestartet, sonst aber die Alphabetische
			if(i == 0)
			{
				col = 3;
				// Nummerische Suche
			} else {
				col = 2;
				// Alphabetische Suche
			}
			sortTable(logTable, col);
		}
	});

	// Listener fuer den Suchbutton mit dem dazugehoerigen TextFeld
	searchButton.addActionListener(new ActionListener()
	{
		public void actionPerformed(ActionEvent ae)
		{
			ArrayList<LogEntry> logEntryList = null;
			String searchTerm = (String) searchTextField.getText();
			System.out.println(searchTerm);
			// Wenn im SearchTerm nichts drin ist und dennoch auf Sucher gedrueckt wird
			if(searchTerm.equals(""))
			{
				System.out.println("Ereignisliste");
				logEntryList = eshop.getLog().getLogList();
			} else {
				logEntryList = eshop.getAllSpecificEntries(searchTerm);
				System.out.println("Name");
				System.out.println(searchTerm);
				System.out.println(logEntryList);
				if(logEntryList.size()==0)
				{
					JOptionPane.showConfirmDialog(
                        	logIn,
                        "Es wurden keine Logeinträge zu diesem Produkt gefunden.",
                        "Fehler",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.ERROR_MESSAGE
                    );
				}
			}
			DefaultTableModel model = (DefaultTableModel) logTable.getModel();

			// Tabelle wird geleehrt
			model.setRowCount(0);
			// Tabelle wird neu ausgerichtet
			makeTableModel(logEntryList, model);
		}
	});

	}

// --- Methoden ---

	public TableModel makeTableModel(ArrayList<LogEntry> list, DefaultTableModel model)
	{
		if(model == null)
		{	
			// Definiere Kopfzeile
			String[] header =
			{
				"Stück",
				"Produkt",
				"Datum & Uhrzeit",
				"Aktion",  // Satz der aus move entsteht
				"Rolle",
				"Benutzername"
			};
			model = new DefaultTableModel(header, 0);
		}

		// Fuelle fuer jeden LogEintrag die Attribute in die logListe
		for(LogEntry logentry : list)
		{
			// Stueck bestimmen
			int produktStueck = logentry.getQuantity();

			// Produkt bestimmen
			String productName = logentry.getProduct().getName();

			// Datum Bestimmen
			String dateAndTime = logentry.createDateAndTime();

			// Aktion bestimmen
			String action = logentry.getAction();

			// Rolle bestimmen
			String personRol = "unbekannt"; // Default wert
			if (logentry.getPerson() != null)
			{
				personRol = (String) logentry.getPerson().getClass().getName();
				if(personRol.equals("valueObjects.Customer"))
				{
					personRol = "Kunde";
				} else if (personRol.equals("valueObjects.Employer")) {
					personRol = "Mitarbeiter";
				} 
			}

			// Benutzername bestimmer
			String username = (String) logentry.getPerson().getUsername();
			
			Object[] row =
			{	
				produktStueck,
				productName,
				dateAndTime,
				action,
				personRol,
				username
			};
			model.addRow(row);
		}
		return model;
	}

	public void refreshTable()
	{
		this.removeAll();
		this.initialize();
	}

	public void sortTable(JTable table, int col)
	{
		TableRowSorter<TableModel> sorter = new TableRowSorter<>(table.getModel());
		table.setRowSorter(sorter);
		ArrayList<RowSorter.SortKey> sortKeys = new ArrayList<>();
		// Sortierer Nummerisch
		sortKeys.add(new RowSorter.SortKey(col, SortOrder.ASCENDING));
		sorter.setSortKeys(sortKeys);
		
		sorter.sort();
	}

	
}
