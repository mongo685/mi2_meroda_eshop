package ui.gui;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.WindowConstants;
import java.awt.BorderLayout;
import domain.Eshop;
import ui.gui.LogIn;
import valueObjects.Customer;


public class GuiBuy extends JPanel implements ActionListener
{
    //zum Testen
   // Customer customer = new Customer("username", "password", "firstname", "lastname", "streetname", "town", "country", 22222, 2 );
    
    
    private JTabbedPane tabbedPane;
    private Customer customer;

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Eshop eshop;
    private GuiCustomer guiCustomer;

    private LogIn logIn;
    public GuiBuy(Eshop eshop, Customer customer , LogIn logIn) 
    {
        super();
        this.eshop = eshop;
        this.customer = customer;
        this.logIn = logIn;
        //    this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.initialize();
    }

    
    private void initialize()
    {
      
       this.setLayout(new BorderLayout());
    
        tabbedPane = new JTabbedPane();
        JPanel login = new JPanel();
        tabbedPane.add("Login", login);
        guiCustomer = new GuiCustomer(eshop, customer, this,logIn);
        tabbedPane.add("Onlineshop", guiCustomer);
        //GuiInvoice guiInvoice = new GuiInvoice(eshop, customer, this);
        //tabbedPane.add("Rechnung", guiInvoice);
        JPanel invoice = new JPanel();
        tabbedPane.add("Rechnung", invoice);
        this.add(tabbedPane);
        tabbedPane.setEnabledAt(0, false);
        tabbedPane.setEnabledAt(2, false);
        tabbedPane.setSelectedIndex(1);


      /*  //Listener Schließen
        //beim Schließen wird der Warenkorb geleert und Produktliste und log 
        //persistent gespeichert
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                eshop.cleanC(customer);
                System.out.println("Programm wurde beendet");
            }
        });

*/
        this.setVisible(true);
       // this.pack();
        //this.setExtendedState(Frame.MAXIMIZED_BOTH);

    }

    @Override
    public void actionPerformed(ActionEvent e) 
    {
        if (e.getActionCommand().equals("KAUFEN"))
        {
            System.out.println("KAUFEN WURDE GEDRÜCKT");
            tabbedPane.remove(2);
            GuiInvoice guiInvoice = new GuiInvoice(eshop, customer, this);
            tabbedPane.add("Rechnung", guiInvoice);
            tabbedPane.setEnabledAt(0, false);
            tabbedPane.setEnabledAt(1, false);
            tabbedPane.setEnabledAt(2, true);
            tabbedPane.setSelectedIndex(2);
        }
        /*if (e.getActionCommand().equals("logout"))
        {
            System.out.println("LOGOUT WURDE GEDRÜCKT");
           eshop.cleanC(customer);
           // dispose();
        }*/
        if (e.getActionCommand().equals("again"))
        {
            System.out.println("NOCHMAL EINKAUFEN WURDE GEDRÜCKT");
            eshop.cleanC(customer);
            guiCustomer.back();
            tabbedPane.setEnabledAt(0, false);
            tabbedPane.setEnabledAt(1, true);
            tabbedPane.setEnabledAt(2, false);
            tabbedPane.setSelectedIndex(1);
            System.out.println("nochmal einkaufen");
            
        }
 
    }


    public static void main(String[] args)
    {
        // Eshop lidl = new Eshop();
        // GuiBuy gb = new GuiBuy(lidl);
    }


}