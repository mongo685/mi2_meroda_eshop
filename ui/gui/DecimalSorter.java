package ui.gui;

import java.util.Comparator;

public class DecimalSorter implements Comparator<String>
{
    @Override
    public int compare(String o1, String o2) 
    {
        
        Double d1 = convertToDouble(o1);
        Double d2 = convertToDouble(o2);
        if (d1 == null && d2 == null) 
        {
            return 0;
        } else if (d1 == null) 
        {
            return 1;
        } else if (d2 == null) 
        {
            return -1;
        }
        //wenn die erste Zahl kleiner als die Zweite ist wird -1 zurueckgegeben
        //wenn die zweite Zahl kleiner als die Erste ist wird 1 zurueckgegeben
        //wenn beide gleichgross sind wird 0 zurueckggegeben
        return d1 < d2 ? -1 : d2 < d1 ? 1 : 0;
    }

    private Double convertToDouble(String s) 
    {
        //Komma wird durch Punkt ersetzt
        if (s.indexOf(",") > -1)
            s = s.replace(",", ".");
        //das Wort Euro wird entfernt
        if (s.indexOf("Euro") > -1)
            s = s.replace("Euro","");
        s = s.trim();
        //-?\\d+([.]{1}\\d+)? sind regular Expressions
        //und geben an, wie eine double/Dezimalzahl augebaut ist
        //also es kann eine Zahl ohne Punkt sein,
        //wenn es einen Punkt gibt, darf nur EINEN Punkt geben
        //und vor und nach dem Punkt muessen Ziffern stehen 
        if (s.matches("-?\\d+([.]{1}\\d+)?")) 
        {
            try 
            {
                return new Double(s);
            } catch (NumberFormatException e) 
            {
            }
        }
        return null;
    }
}