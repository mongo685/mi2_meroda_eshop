package ui.gui;

import domain.Eshop;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import valueObjects.BulkProduct;
import valueObjects.Customer;
import valueObjects.ProductQuantity;

import java.awt.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class GuiInvoice extends JPanel 
{
    private final DateTimeFormatter dayMonthYearAtTime = DateTimeFormatter.ofPattern("dd.MM.yyyy' um 'HH:mm:ss");
    private JButton logOut;
    private JButton again;
    private Eshop eshop;
    private Customer customer;
    private GuiBuy guiBuy;

    public GuiInvoice(Eshop eshop, Customer customer, GuiBuy guiBuy) 
    {
        super();
        this.eshop = eshop;
        this.customer = customer;
        this.guiBuy = guiBuy;

        this.initialize();
    }

    private void initialize()
    { 
        

        //Farbe der Buttons
        Color cb = new Color(255, 244, 200);
        //Hintergrundfarbe
        Color ch = new Color(255, 200, 0);

        //Schriftarten
        Font  f1  = new Font("Impact",  Font.BOLD, 30);
        Font  f2  = new Font("Impact",  Font.BOLD, 20);
        Font  f3  = new Font("Source Sans Pro Light",  Font.BOLD, 15);


        //Layout ganz außen drum
        this.setLayout(new BorderLayout());


        //--------Layout  Oben--------
        JPanel upPanel = new JPanel();
        upPanel.setLayout(new BoxLayout(upPanel, BoxLayout.LINE_AXIS));
        this.add(upPanel, BorderLayout.NORTH);
        upPanel.setBackground(Color.BLACK);

        //Ueberschrift Onlineshop
        JLabel heading = new JLabel();
        heading.setText("Rechnung");
        heading.setFont(f1);
        //Schriftfarbe
        heading.setForeground(Color.RED);
        upPanel.setPreferredSize(new Dimension(200, 40));
        upPanel.add(heading);
        //Luecke
        Dimension upMin = new Dimension(20,5);
        Dimension upPre = new Dimension(Short.MAX_VALUE,5);
        Dimension upMax = new Dimension(Short.MAX_VALUE,5);
        upPanel.add(new Box.Filler(upMin, upPre, upMax));
        //Button um nochmal einkaufen zu können
        again = new JButton("nochmal einkaufen");
        again.setBackground(cb);
        again.setPreferredSize(new Dimension(200, 30));
        upPanel.add(again);
        //log out Button
        logOut = new JButton("log out");
        logOut.setBackground(cb);
        logOut.setPreferredSize(new Dimension(200, 30));
        upPanel.add(logOut);


        //--------Layout Links-------
        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.PAGE_AXIS));
        //Hintergrundfarbe
        centerPanel.setBackground(ch);
        this.add(centerPanel, BorderLayout.CENTER);
        Dimension borderMinSize = new Dimension(10, 10);
        Dimension borderPrefSize = new Dimension(20, 20);
        Dimension borderMaxSize = new Dimension(30, 30);
        centerPanel.add(new Box.Filler(borderMinSize, borderPrefSize, borderMaxSize));
        JLabel name = new JLabel("Name: "+customer.getFirstname()+" "+customer.getLastname());
        name.setFont(f3);
        centerPanel.add(name);
        JLabel street = new JLabel("Straße: "+customer.getAddress().getStreetname()+" "
                +customer.getAddress().getStreetnumber());
        street.setFont(f3);
        centerPanel.add(street);
        JLabel town = new JLabel("Stadt: "+customer.getAddress().getPlz()+" "
                +customer.getAddress().getTown());
        town.setFont(f3);
        centerPanel.add(town);
        JLabel country = new JLabel("Land: "+customer.getAddress().getCountry());
        country.setFont(f3);
        centerPanel.add(country);
        centerPanel.add(new Box.Filler(borderMinSize, borderPrefSize, borderMaxSize));


        //--------Layout Rechts-------
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.PAGE_AXIS));
        rightPanel.setBackground(ch);
        this.add(rightPanel, BorderLayout.EAST);
        rightPanel.add(new Box.Filler(borderMinSize, borderPrefSize, borderMaxSize));
        JLabel date = new JLabel("Datum: "+LocalDateTime.now().format(dayMonthYearAtTime));
        date.setFont(f3);
        rightPanel.add(date);
        JLabel number = new JLabel("Kundennummer: "+customer.getMUN().getNumber());
        number.setFont(f3);
        rightPanel.add(number);
        rightPanel.add(new Box.Filler(borderMinSize, borderPrefSize, borderMaxSize));



        logOut.addActionListener(guiBuy);
        logOut.setActionCommand("logout");
        again.addActionListener(guiBuy);
        again.setActionCommand("again");


        
        //--------Layout Rechts-------
        JPanel downPanel = new JPanel();
        downPanel.setLayout(new GridLayout());
        //Tabelle des Warenkorbs erstellen
        ArrayList<ProductQuantity> basketlist = eshop.getBasket(customer);
        TableModel invoiceModel = invoiceTableModel(basketlist);
        JTable invoiceTable = new JTable(invoiceModel) {
            //der Nutzer kann nicht in die Tabelle schreiben
            public boolean isCellEditable(int x, int y){
                return false;
            }
        };
        for(int i=0; i<5; i++)
        {
            invoiceTable.getColumnModel().getColumn(i).setCellRenderer(new CellRenderer());
        }
        //invoiceTable.getColumnModel().getColumn(4).setCellRenderer(new CellRenderer());
        //Spalten der Tabelle koennen vom Nutzer nicht vertauscht werden
        invoiceTable.getTableHeader().setReorderingAllowed(false);
        //Tabelle ist scrollbar
        JScrollPane invoicePane = new JScrollPane(invoiceTable);
        invoiceTable.setFillsViewportHeight(true);
        //this.add(invoicePane, BorderLayout.SOUTH);
        downPanel.add(invoicePane);
        this.add(downPanel, BorderLayout.SOUTH);

        eshop.buy(customer);

        //-----------LISTENER--------

        //Listener fuer logout Button
        logOut.addActionListener(guiBuy);
        logOut.setActionCommand("logout");




    }

    //Tabelle fuer Warenkorb in Rechnung
    public TableModel invoiceTableModel( ArrayList<ProductQuantity> list)
    {
        //Kopfzeile der Tabelle
        String[] header = 
        {
            "Artikelnummer",
            "Produkt",
            "Anzahl",
            "Preis pro Stück",
            "Gesamtpreis"
        };
       DefaultTableModel model = new DefaultTableModel(header, 0);
       //fuer jedes Produkt in der Rechnung wird 
       //Nummer, Name, Anzahl, Preis(pro Stueck) und Gesamtpreis in die Tabelle eingetragen
       for (ProductQuantity pq : list)
       {   if(!pq.getProduct().getDeleted())
            {
               String quantity = "" + pq.getQuantity();
               String price = Eshop.convertPrice(pq.getProduct().getPrice());
               double pricefort; 
                if(pq.getProduct() instanceof BulkProduct)
                {
                    BulkProduct bulkproduct = (BulkProduct) pq.getProduct();     
                    pricefort = pq.getProduct().getPrice() * (pq.getQuantity() / bulkproduct.getPackSize());
                }else
                {
                    pricefort = pq.getProduct().getPrice() * pq.getQuantity();    
                }  
               String priceT =Eshop.convertPrice( pricefort);
               Object[] row = 
               {
                   pq.getProduct().getMUN().getNumber(),
                   pq.getProduct().getName(),
                   quantity,
                   price,
                   priceT

               };
               model.addRow(row);
            }
        }
        String total = "Gesamtbetrag";
        String n ="";
        String totalPrice = Eshop.convertPrice(this.customer.getBasket().getTotalprice());
        Object[] row = 
        {
            total,
            n,
            n,
            n,
            totalPrice

        };
        model.addRow(row);
    return model;
    }

 





    public static void main(String[] args)
    {
        Eshop lidl = new Eshop();
        //GuiInvoice gi = new GuiInvoice(lidl);
        
        
        
    }

}