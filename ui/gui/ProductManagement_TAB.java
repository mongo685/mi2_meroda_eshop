package ui.gui;

import valueObjects.*;
import domain.Eshop;

import javax.swing.*;
import javax.swing.border.Border;

import domain.exceptions.ProductAlreadyExistsException;
import domain.exceptions.ProductDoesntExistException;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;



public class ProductManagement_TAB extends JPanel {

    private Eshop eshop;

    private Employer employer; 
    private JButton logOut;
    private LogIn logIn;

    boolean productchoose = true ;

    public ProductManagement_TAB(Eshop eshop,Employer employer, LogIn logIn) {
        super();
        this.eshop = eshop;
        this.employer = employer;
        this.logIn = logIn;
        this.initialize();
       
    }

    public TableModel makeTableModelProduct(ArrayList<Product> list, DefaultTableModel model) {
        if (model == null) {

            // Kopfzeile der Tabelle
            String[] header = { "Artikelnummer", "Artikelname", "Packungsgröße", "Preis", "Lagerbestand", "Deleted", };
            model = new DefaultTableModel(header, 0);
        }

        // fuer jedes Produkt in der Produktliste wird
        // Nummer, Name, Packungsgroesse, Preis und der Lagerbestand wird in die Tabelle
        // eingetragen
        for (Product p : list) {
            // wenn das Produkt kein BulkProduct ist, wird als Packungsgroesse eins
            // eingetragen
            String packSize = "1";

            if (p instanceof BulkProduct) {
                BulkProduct bp = (BulkProduct) p;
                packSize = "" + bp.getPackSize();
            }
            String price = Eshop.convertPrice(p.getPrice());
            Object[] row = { "" + p.getMUN().getNumber(), p.getName(), packSize, price, p.getStock(), p.getDeleted(), };
            model.addRow(row);

        }
        return model;
    }

    private void initialize() {
        Color cb = new Color(255, 244, 200);
        Font f1 = new Font("Impact",  Font.BOLD, 30);

        this.setLayout(new BorderLayout());

          // Grundlegende Einstellungen
          JPanel upperPanel = new JPanel();
          upperPanel.setLayout(new BoxLayout(upperPanel, BoxLayout.LINE_AXIS));
          upperPanel.setBackground(Color.BLACK);
          upperPanel.setPreferredSize(new Dimension(200, 40));
          this.add(upperPanel, BorderLayout.NORTH);
        
          // Ueberschrift Mitarbeiterverwaltung
          JLabel headTitle = new JLabel();
          headTitle.setText("Produktverwaltung");
          headTitle.setFont(f1);
          headTitle.setForeground(Color.RED);
          upperPanel.add(headTitle);

          // Luecke
          Dimension upMin = new Dimension(20,5);
          Dimension upPre = new Dimension(Short.MAX_VALUE,5);
          Dimension upMax = new Dimension(Short.MAX_VALUE,5);
          upperPanel.add(new Box.Filler(upMin, upPre, upMax));

          // Setzte Log-Out Button
          logOut = new JButton("Log-Out");
          logOut.setBackground(cb);
          logOut.setPreferredSize(new Dimension(200, 30));
          upperPanel.add(logOut);
          
        
        
        
        // CENTER Employer LIST mit der Tabelle + Suche
        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new BorderLayout());

        // !Tabelle der Artikelliste
        ArrayList<Product> productlist = eshop.productlistSortName();
        TableModel productModel = makeTableModelProduct(productlist, null);
        JTable productTable = new JTable(productModel) {
            // der Nutzer kann nicht in die Tabelle schreiben
            public boolean isCellEditable(int x, int y) {
                return false;
            }
        };
        // die Spalten lassen sich vom Nutzer nicht vertauschen
        productTable.getTableHeader().setReorderingAllowed(false);
        // Tabelle ist scrollbar
        JScrollPane scrollPane = new JScrollPane(productTable);
        productTable.setFillsViewportHeight(true);

        // EAST HINZUFÜGEN /SORT
        JPanel eastPanel = new JPanel();
        eastPanel.setLayout(new BorderLayout());
        eastPanel.setPreferredSize(new Dimension(300, 500));

        // ! Produkt hinzufügen PANEL
        JPanel addproductPanel = new JPanel();
        GridBagLayout gridBagLayout = new GridBagLayout();
        addproductPanel.setLayout(gridBagLayout);
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridy = 0; // Zeile 0


        c.gridy = 1; // Zeile 1
        JLabel productLabel = new JLabel("Neues Produkt hinzufügen");
        c.gridx = 0; // Spalte 0
        c.weightx = 1.0; // 100% der gesamten Breite
        gridBagLayout.setConstraints(productLabel, c);
        // productLabel.setBorder(blackBorder);
        addproductPanel.add(productLabel);

       

        c.gridy = 2; // Zeile 2

        JTextField nameTextField = new JTextField("Name");
        c.gridx = 0; // Spalte 0
        c.weightx = 1.0; // 100% der gesamten Breite
        gridBagLayout.setConstraints(nameTextField, c);
        addproductPanel.add(nameTextField);

        JTextField quantityTextField = new JTextField("Anzahl");
        c.gridx = 1; // Spalte 0
        c.weightx = 1.0; // 100% der gesamten Breite
        gridBagLayout.setConstraints(quantityTextField, c);
        addproductPanel.add(quantityTextField);

        c.gridy = 3; // Zeile 3

        JTextField priceTextField = new JTextField("Preis");
        c.gridx = 0; // Spalte 0
        c.weightx = 1.0; // 100% der gesamten Breite
        gridBagLayout.setConstraints(priceTextField, c);
        addproductPanel.add(priceTextField);

        JTextField packsizeTextField = new JTextField("Packsize");
        c.gridx = 1; // Spalte 0
        c.weightx = 1.0; // 100% der gesamten Breite
        packsizeTextField.setEditable(false);
        packsizeTextField.setBackground(Color.LIGHT_GRAY);
        gridBagLayout.setConstraints(packsizeTextField, c);
        addproductPanel.add(packsizeTextField);

        c.gridy = 4; // Zeile 4

        JButton addproductButton = new JButton("Hinzufügen");
        c.gridx = 1; // Spalte 1
        c.weightx = 0.5; // 100% der gesamten Breite
        gridBagLayout.setConstraints(addproductButton, c);
        addproductPanel.add(addproductButton);

        JCheckBox bulkCheckBox = new JCheckBox("Massengutartikel");
        c.gridy = 5;
        c.gridx = 1; // Spalte 2
        c.weightx = 0.5;
        gridBagLayout.setConstraints(bulkCheckBox, c);
        addproductPanel.add(bulkCheckBox);

        
        // ! Product sort PANEL
        JPanel sortproductPanel = new JPanel();
        JLabel sortLabel = new JLabel("Sortieren nach : ");
        // sortproductPanel.setBorder(new EmptyBorder(50, 50, 0, 0) );
        sortproductPanel.add(sortLabel);

        String comboBoxList[] = { "Name", "Preis", "Artikelnummer" };

        JComboBox sortChoose = new JComboBox(comboBoxList);
        sortproductPanel.add(sortChoose);

        eastPanel.add(addproductPanel, BorderLayout.NORTH);
        eastPanel.add(sortproductPanel, BorderLayout.SOUTH);

        // SOUTH SEARCH +- remove
        JPanel searchPanel = new JPanel();
        GridBagLayout searchGridBagLayout = new GridBagLayout();
        searchPanel.setLayout(searchGridBagLayout);
        GridBagConstraints d = new GridBagConstraints();
        d.fill = GridBagConstraints.HORIZONTAL;
        d.gridy = 0; // Zeile 0

        JTextField searchTextField = new JTextField();
        d.gridx = 0; // Spalte 0
        d.weightx = 3.0;

        // gridBagLayout.setConstraints(searchTextField, d);
        searchPanel.add(searchTextField, d);

        JButton searchButton = new JButton("Suchen");
        d.gridx = 1;
        d.weightx = 0.3;
        // gridBagLayout.setConstraints(searchButton, d);
        searchPanel.add(searchButton, d);

        JButton plusButton = new JButton("+");
        d.gridx = 2;
        d.weightx = 0.2;
        // gridBagLayout.setConstraints(plusButton, d);
        searchPanel.add(plusButton, d);

        JButton minusButton = new JButton("-");
        d.gridx = 3;
        d.weightx = 0.2;
        // gridBagLayout.setConstraints(minusButton, d);
        searchPanel.add(minusButton, d);

        JButton removeButton = new JButton("Entfernen");
        d.gridx = 4;
        d.weightx = 0.3;
        // gridBagLayout.setConstraints(removeButton, d);
        searchPanel.add(removeButton, d);

        // ! CenterPanel zsm setzten aus scrollPane und searchPanel
        centerPanel.add(scrollPane, BorderLayout.CENTER);
        centerPanel.add(searchPanel, BorderLayout.SOUTH);

        // ! alles zsmfügen alle LAYOUTS
        this.add(centerPanel, BorderLayout.CENTER);
        this.add(eastPanel, BorderLayout.EAST);
        setVisible(true);
       

        // ------Listener---------
        //Listener fuer logout Button
            logOut.addActionListener(logIn);
            logOut.setActionCommand("logoutEmployer");

        // Listener fuer Sortieroptionen der Artikelliste
        // die Artikelliste kann nach Name Nummer und Preis sortiert werden
        sortChoose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int col = 1;
                int i = sortChoose.getSelectedIndex();
                // im folgenden wird immer von 0 an gezaehlt
                // wenn in den Sortieroptionen Artikelname ausgewaehlt ist,
                // also Zeile 0, wird nach Spalte 1 in der Tabelle sortiert
                // was ebenfalls Artikelname ist
                if (i == 0) {
                    col = 1;
                    // nach Preis sortieren
                } else if (i == 1) {
                    col = 3;
                    // nach Artikelnummer sortieren
                } else if (i == 2) {
                    col = 0;
                }
                sortTable(productTable, col);
            }

        });

        
        Border redborder = BorderFactory.createLineBorder(Color.red);
        // Listener für die Auswahl von Massenartikel und normalen Produkt mit Packsize 1
       
    
  bulkCheckBox.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                
            if(bulkCheckBox.isSelected())
            {
                productchoose = false; 
                packsizeTextField.setEditable(true);
                packsizeTextField.setBackground(nameTextField.getBackground());

            } 
            
            else
            {
                productchoose = true;
                packsizeTextField.setEditable(false);
                packsizeTextField.setBackground(Color.LIGHT_GRAY);
            }
                   

            
                
                
            }
        }); 

    // Listener fuer den Hinzufuegen Button eines neuen Produktes
  addproductButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                try{
                    // Auslesen der Produktdetails fürs hinzufügen
                    
                    String name = nameTextField.getText();
                    int stock = Integer.parseInt(quantityTextField.getText());
                    int packSize = 1;
                    String readprice = priceTextField.getText();
                    
                    if (readprice.indexOf(",") > -1)
                     {
                        readprice = readprice.replace(",", ".");
                    }
                   double price =  Double.parseDouble(readprice);
                   
                    Product newProduct = new Product(name,price);
                    
                   
                   

                    if(productchoose == true && !eshop.compareProduct(newProduct)){
                    eshop.addProduct(newProduct); 
                    //System.out.println(newProduct);
                    eshop.setStock(employer, newProduct , stock);
                    //System.out.println(newProduct);
                    DefaultTableModel model = (DefaultTableModel)productTable.getModel();
                    model.addRow(new Object[]{newProduct.getMUN().getNumber(),name,packSize, Eshop.convertPrice(price), stock, newProduct.getDeleted(),  });
                       
                    }
                    
                    // FIXME: Den Client so klein wie moeglich halten, der Server soll alle implementierten Regeln anwenden und senden
                    if(productchoose == false && !eshop.compareProduct(newProduct))
                    {
                       packSize = Integer.parseInt(packsizeTextField.getText());
                       Product bulkproduct = new BulkProduct(name,price,packSize);
                     
                       if(stock % packSize == 0)
                       {
                       eshop.addProduct(bulkproduct);
                       eshop.setStock(employer, bulkproduct, stock);
                       DefaultTableModel model = (DefaultTableModel)productTable.getModel();
                        model.addRow(new Object[]{newProduct.getMUN().getNumber(),name,packSize, Eshop.convertPrice(price), stock, newProduct.getDeleted(),  });
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(null, "Das Produkt konnte nicht hinzugefügt werden, da der Bestand " + stock + " kein Vielfaches von " + packSize + " ist");    
                        }
                        
                   
                    }

                
                }
                catch (ProductAlreadyExistsException e)  
                {
                    System.out.println(e.getMessage()); 
                    //zeigt Fehlermeldung in einem Fenster
                    JOptionPane.showConfirmDialog(null,
                     e.getMessage(),
	                "Fehler",
                    JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.ERROR_MESSAGE
                    );
                }
            }
        }); 

    
    
 
 
         //Listener fuer + Button
        //Anzahl eines Produktes wird im Warenkorb um eins oder die Packungsgroeße erhoeht
        plusButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
            

                try 
                {
                    int column = 1;
                    int row = productTable.getSelectedRow();
                    //wenn eine Zeile in der Tabelle des Warenkorbs angeklicht ist
                    //wird nachfolgendes ausgefuehrt
                    if(row>=0)
                    {

                    
                        String value = productTable.getModel().getValueAt(row, column).toString();
                        Product product = eshop.searchProduct(value); 
                        //bei normalen Produkten wird eins hinzugefuegt
                        int quantity = 1;
                        //bei Bulkproduct wird eine Packung hinzugefuegt
                        if(product instanceof BulkProduct)
                        {
                            BulkProduct bp = (BulkProduct) product;
                            quantity = bp.getPackSize();
                        }
                        
                        if(eshop.setStock(employer, product, (product.getStock() + quantity)))
                        {
                            DefaultTableModel model = (DefaultTableModel)productTable.getModel();
                            model.setValueAt(product.getStock(), row, 4);
                            System.out.println("Produkt " + product.getName() + " erfolgreich Menge erhöht.");
                        }
                    }
                }
                catch (ProductDoesntExistException e)
                {
                    System.out.println(e.getMessage());  
                 
                }
            
            }
        
        });
    


    //Listener fuer Loeschen Button
        //beim Betaetigen des Loeschen Buttons
        //wird das ausgewaehlte Produkt auf Delted gesetzt
        removeButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                try
                {
                    int column = 1;
                    //row ist die angeklickte Zeile
                   int row = productTable.getSelectedRow();
                    //nur wenn eine Zeile angeklickt ist wird das folgende ausgefuehrt
                    if(row>=0)
                    {
                        //der Name des Produktes wird ausgelesen 
                        //und das Produkt aus der Produktliste entfernt
                        String name = productTable.getModel().getValueAt(row, 1).toString();
                        //das Produkt wird aus der Basket Tabelle entfernt
                        Product delProduct = eshop.searchProduct(name); 
                        if(eshop.deactivateProduct(employer,name))
                      {
                        DefaultTableModel model = (DefaultTableModel)productTable.getModel();
                        model.setValueAt(delProduct.getDeleted(), row, 5);
                        System.out.println("Produkt wurde entfernt " + delProduct.getName() + " " + row);

                      }
                
                    }


                }
                     
                

                
                catch(ProductDoesntExistException e)
                {
                    System.out.println(e.getMessage());  
                    
                } 

            }
        });
 
 
 //? TODO: Soll noch eine weitere Moeglichkeit geschaffen werden die Packungsgroese anzupassen?
    
    //Listener fuer - Button
        //Anzahl eines Produktes wird in der Produktliste um eins oder die Packungsgroeße verringert
        minusButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                try 
                {
                    int column = 1;
                    int row = productTable.getSelectedRow();
                    //wenn eine Zeile in der Tabelle des Warenkorbs angeklicht ist
                    //wird nachfolgendes ausgefuehrt
                    if(row>=0)
                    {
                        String value = productTable.getModel().getValueAt(row, column).toString();
                        Product product = eshop.searchProduct(value); 
                        //bei normalen Produkten wird eins hinzugefuegt
                        int quantity = 1;
                        //bei Bulkproduct wird eine Packung hinzugefuegt
                        if(product instanceof BulkProduct)
                        {
                            BulkProduct bp = (BulkProduct) product;
                            quantity = bp.getPackSize();
                        }
                        
                        if(eshop.setStock(employer, product, (product.getStock() - quantity)))
                        {
                            DefaultTableModel model = (DefaultTableModel)productTable.getModel();
                            model.setValueAt(product.getStock(), row, 4);
                            System.out.println("Produkt " + product.getName() + " erfolgreich Menge verringert.");
                        }
                    }
                }
                catch (ProductDoesntExistException e)
                {
                    System.out.println(e.getMessage());  
                }
            
            }
        
        });
    
 
     //Listener such Button
     searchButton.addActionListener(new ActionListener()
     {
         public void actionPerformed(ActionEvent ae)
         {
             ArrayList<Product> productlist = null; 
             String searchTerm = (String)searchTextField.getText();
             //wenn ein Punkt oder Komma im Textfeld ist, wird nach einem Preis gesucht
             if (searchTerm.indexOf(",") > -1)
             {
                 searchTerm = searchTerm.replace(",", ".");
             }
             if(searchTerm.indexOf(".") > -1)
             {
                 Double price = Double.parseDouble(searchTerm);
                 productlist = eshop.searchProductsPrice(price);
                 System.out.println("Preis");
             }
             //Wenn nichts im Textfeld steht, wird die gesamte Liste angezeigt
             else if(searchTerm.equals(""))
             {
                 System.out.println("Artikelliste");
                 productlist = eshop.getProductList();
             }
             //sonst wird nach dem Produktnamen gesucht
             else
             {
                 productlist = eshop.searchProducts(searchTerm);
                 System.out.println("Name");
                 System.out.println(searchTerm);
             }
             DefaultTableModel model = (DefaultTableModel)productTable.getModel();
            
             //die Tabelle wird geleert
             model.setRowCount(0);
             //die Tabelle wird neu befuellt
             makeTableModelProduct(productlist, model);
             
         }
     });
    }
 
 
 //------- Methoden-------
 private void sortTable(JTable table, int col)
 {
     TableRowSorter<TableModel> sorter = new TableRowSorter<>(table.getModel());
     table.setRowSorter(sorter);
     System.out.println(col);
     ArrayList<RowSorter.SortKey> sortKeys = new ArrayList<>();
     sortKeys.add(new RowSorter.SortKey(col, SortOrder.ASCENDING));
     sorter.setSortKeys(sortKeys);
     //fuer Artikelnummer und Preis wird eine Zahlenvergleichmethode angewendet
     if(col==0 || col==3)
     {
         DecimalSorter comp = new DecimalSorter();
         sorter.setComparator(col, comp);

     }
     sorter.sort();
 }
    
  
   
   



    public static void main (String [] args){
    Eshop lidl = new Eshop();
    //GuiProduct gui = new GuiProduct(lidl);    
    }
}
