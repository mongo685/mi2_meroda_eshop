package domain;

import java.util.ArrayList;
import domain.logging.LogEntry;
import domain.management.CustomerManagement;
import domain.management.EmployerManagement;
import domain.management.LogManagement;
import domain.management.ProductManagement;
import domain.management.ShoppingManagement;
import valueObjects.Product;
import valueObjects.ProductQuantity;
import valueObjects.Customer;
import valueObjects.Employer;
import valueObjects.Person;
import domain.exceptions.*;

//- TODO Pluspunkte fuer:
// -> Updates an alle Clients in der Anwendung

public class Eshop
{
    // Verwaltung der Mitarbeiter
    private final EmployerManagement employerManagement;
    // Verwaltung der Kunden
    private CustomerManagement customerManagement;
    // Verwaltung der Produkte
    private final ProductManagement productManagement;
    // Verwaltung des Warenkorbs
    private ShoppingManagement shoppingManagement;
    // Verwaltung des Log
    private LogManagement logManagement;
    
    


    
    public Eshop()
    {
        customerManagement = new CustomerManagement();
        employerManagement = new EmployerManagement();
        productManagement = new ProductManagement();
        shoppingManagement = new ShoppingManagement();
        logManagement = new LogManagement(customerManagement, employerManagement, productManagement);   
    }


/** Sucht im CustomerManagement nach einem passenden Customer.
 * 
 *  return Customer, sonnst null
 */
public Person login (String userUsername, String userPassword) throws LoginFailException
{   
    Person foundPerson = null;

    // Versuche Login immer erst bei CustomerManagement
    if(foundPerson == null)
    {
        foundPerson = customerManagement.login(userUsername, userPassword);
    }

    
    // Bei keinem Erfolg wird in Employermanagment versucht
    if(foundPerson == null)
    {
        foundPerson = employerManagement.login(userUsername, userPassword);
    }


    // Pruefe ob Objekt vorhanden
    if(foundPerson != null)
    {
        if(foundPerson instanceof Customer)
        {
            // bei erfolgreichem login gebe Customer zurueck
            return (Customer) foundPerson;
        } else { 
            // bei erfolgreichem login gebe Employer zurueck
            return (Employer) foundPerson;
        }
    } else {
        {
            throw new LoginFailException(userUsername);
            // System.out.println("Es wurde keine Person gefunden");
        }
    }


}

 


//* ----------------------  CustomerManagement ----------------------

    public Customer searchCustomerByNumber(long number) 
    {
        return this.customerManagement.searchCustomerByNumber( number);
    }

    
    public ArrayList<Customer> searchCustomerByUsername(String searchUsername)
    {
        return customerManagement.searchCustomerByUsername(searchUsername);
    }

    /**
     * Methode zum Hinzufuegen von Kunden in die Kundenliste
     * 
     * @param newCustomer Kunde, der hinzugefuegt werden soll
     * @return ob erfolgreich hinzugefuegt
     * @throws CustomerAlreadyExists
     */
    public boolean addCustomer(Customer newCustomer) throws CustomerAlreadyExists
    {
       return this.customerManagement.addCustomer(newCustomer);
    }


    /**
     * Methode um Kunden aus der Kundenliste zu entfernen
     * 
     * @param firstname Vorname des zu entfernenden Kunden
     * @param lastname Nachname des z entfernenden Kunden
     * @return ob erfolgreich entfernt
     */
    public boolean removeCustomer(String username)
    {
        return this.customerManagement.removeCustomer(username);
    }


    /**
     * @return Kundenliste als String ausgeben
     */
    public String cmToString()
    {
        return this.customerManagement.toString();
    }


//* ----------- Getter & Setter : CustomerManagemnt -----------


    /**
     * 
     * @return Kundenliste als Arraylist
     */
    public ArrayList <Customer> getCustomerList()               
    {
        return this.customerManagement.getCustomerList();
    }


    /**
     * 
     * @return Kundenliste als CustomerManagement
     */
    public CustomerManagement getCustomerManagement()
    {
        return this.customerManagement;
    }


    /**
     * 
     * @param newCustomer neues Customermanagement
     */
    public void setCustomerManagement( CustomerManagement newCustomer)
    {
        this.customerManagement = newCustomer;
    }


//* -------------------  ProductManagement ------------------- 

    public Product searchProductByNumber(long number) 
    {
        return this.productManagement.searchProductByNumber( number);
    }

    
    /**
     * Methode um ein Produkt der Produktliste hinzuzufuegen
     * 
     * @param product hinzuzufuegendes Produkt
     * @return ob Produkt erfolgreich hinzugefuet
     */
    //Produkt hinzufügen
    public boolean addProduct(final Product product) throws ProductAlreadyExistsException 
    {
        return productManagement.addProduct(product);
    }


    /**
     * Methode um ein Produkt aus der Produktliste zu entfernen
     * 
     * @param name Name des Produkts, das entfernt werden soll
     * @return ob Produkt erfolgreich entfernt
     * @throws ProductIsNullException
     */
    // Produkt entfernen
    public boolean deactivateProduct(Person person, String name) throws ProductDoesntExistException
	{
		// Produkt wird aus der Liste gelöscht
		return productManagement.removeProduct(person, name, logManagement);
	}
    

    /**
     * Methode um Produkte in der Produktliste zu suchen
     * 
     * @param name Name der zu suchende Produkte
     * @return Liste der Produkte, die diesen Namen haben
     */
    //Produkte nach Namen suchen
    public ArrayList<Product> searchProducts(String name) 
	{
		return productManagement.searchProducts(name);
    }
    
    
    /**
	 * Methode, die die Produktliste nach dem gesuchten Product sucht und einen
	 * boolean zurückgibt , wenn dieses Produkt bereits vorhanden ist
	 * 
	 * @param compareProduct Produkt das verglichen wird mit der Liste 
	 * @return true bei gefunden , false bei nicht gefunden
	 */
    public boolean compareProduct(Product compareProduct)
    {
        return productManagement.compareProduct(compareProduct) ;    
    }


    /**
     * Methode um Produkte mit einem bestimmten Preis zu suchen
     * 
     * @param price Preis der zu suchenden Produkte
     * @return Liste der Produkte mit diesem Preis
     */
    //Produkte nach Preis suchen
    public ArrayList<Product> searchProductsPrice( double price) 
	{
		return productManagement.searchProductsPrice(price);
    }
    

    /**
     * Methode um ein Produkt in der Produktliste zu suchen
     * 
     * @param searchProduct Name des zu suchenden Produkts
     * @return Produkt
     */
    public Product searchProduct(String searchProduct) throws ProductDoesntExistException
    {
        return productManagement.searchProduct(searchProduct);
    }


    /**
     * 
     * @return Produktliste
     */
    public ArrayList<Product> getProductList() 
	{
		return productManagement.getProductList();
    }


    /**
     * 
     * @return Produktmanagement
     */
    public ProductManagement getProductManagement()
    {
        return productManagement;
    }
    

    /**
     * Methode zum sortieren der Produktliste nach der Produktnummer
     * 
     * @return Liste, die nach der Produktnummer sortiert ist
     */
    //Produktliste nach Produktnummer sortieren
    public ArrayList<Product> sortMan() 
	{
		return productManagement.sortMan();
    }
    

    /**
     * Methode zum sortieren der Produktliste nach den Namen (alphabetisch)
     * 
     * @return Liste, die nach dem Namen sortiert ist (alphabetisch)
     */
    //Produktliste nach Namen sortieren
    public ArrayList<Product> productlistSortName() 
	{
		return productManagement.productlistSortName();
    }
    

    /**
     * Methode, die eine Arraylist in einen String umwandelt
     * 
     * @param v Liste, die umgewandelt werden soll
     * @return Liste als String
     */
    //Arrayliste in einen String umwandeln
    public String arrayToString( ArrayList<Product> v) 
	{
		return productManagement.arrayToString(v);
    }
    

    /**
     * 
     * @return Produktmanagement als String
     */
    public String pmToString()
    {
        return productManagement.toString();
    }


    /**
     * Methode zum aendern des Lagerbestands
     * 
     * @param name     Name des Produkt, dessen Lagerbestand geändert werden soll
     * @param newStock neuer Lagerbestand des Produktes
     * @return ob erfolgreich der Lagerbestand geaendert wurde
     * @throws ProductDoesntExistException
     */
    // Product Stock ändern
    public boolean setStock(Person person, String name, int newStock) throws ProductDoesntExistException
    {
        return productManagement.setStock(person, name, newStock, logManagement); 
    }

    public boolean setStock(Person person, Product product,  int newStock)
    {   
        System.out.println("Person "+person);
        System.out.println("Produkt "+product);
        System.out.println("Anzahl "+newStock);
        return productManagement.setStock(person, product, newStock, logManagement);
    }

//* ------------------------ Log ------------------------


    public LogManagement getLog()
    {
        return this.logManagement;
    }

    public ArrayList<LogEntry> getAllSpecificEntries(String name)
	{
        ArrayList<LogEntry> list = logManagement.getAllSpecificEntries(name);
        return list;
    }

    public void removeLogEntryByIndex(int index) throws IndexOutOfBoundsException
    {
        this.logManagement.removeLogEntryByIndex(index);        
    }

    public LogEntry getLogEntryByIndex(int index) throws CantFindLogException
    {
        
        return this.logManagement.getLogEntryByIndex(index);
    }

    
//* ------------------- EmployerManagement -------------   

    public Employer searchEmployerByNumber(long number) 
    {
        return this.employerManagement.searchEmployerByNumber( number);
    }


    /**
     * Methode zum Hinzufuegen von Mitarbeitern in die Mitarbeiterliste
     * 
     * @param employer Mitarbeiter, der in die Liste hinzugefuegt werden soll
     * @return ob Mitarbeiter erfolgreich hinzugefuegt
     * @throws EmployerAlreadyExists
     */
    // Methode um Mitarbeiter in die Liste einzufügen
    public boolean addEmployer(Employer employer) throws EmployerAlreadyExists 
    {
        return this.employerManagement.addEmployer(employer);
    }


    /**
     * Methode zum Entfernen von Mitarbeitern aus der Mitarbeiterliste
     * 
     * @param username Vorname des zu entfernenden Mitarbeiters
     * @return ob Mitarbeiter erfolgreich entfernt
     */
    // Methode um Mitarbeiter aus der Liste zu entfernen
    public boolean removeEmployer(String username)    
    {
        return this.employerManagement.removeEmployer(username);    
    }

   /**
     * Methode zum Suchen von Mitarbeitern aus der Mitarbeiterliste mit Hilfe des Usernames
     * 
     * @param username Username des Mitarbeiters
     * 
     * @return Employer , wenn er gefunden wurde sonst null
     */

    public ArrayList<Employer> searchEmployerByUsername(String searchUsername){
        return employerManagement.searchEmployerByUsername(searchUsername);
    }
    
//* ------------ Getter & Setter ------------


    /**
     * Methode um die Mitarbeiterliste als ArrayList zu bekommen
     * 
     * @return Mitarbeiterliste
     */
    // gibt die aktuelle Mitarbeiterliste zurück
    public ArrayList <Employer> getEmployerList()               
    {
        return this.employerManagement.getEmployerList();
    }


    /**
     * 
     * @return EmployerManagement
     */
    public EmployerManagement getEmployerManagement()
    {
        return this.employerManagement;
    }


    /**
     * Methode, um das Employermanagement als String auszugeben
     * 
     * @return EmployerManagement als String
     */
    public String emToString()
    {                               
        return this.employerManagement.toString();
    }


//* --------------Basket---------------


    /**
     * Methode, um Produkte in den Warenkorb zu legen oder die Menge zu erhöhen
     * 
     * @param customer betreffender Kunde
     * @param product betreffendes Produkt
     * @param quantity Menge des Produktes
     * @return ob erfolgreich in den Warenkorb gelegt
     */
    // etwas wird in den Warenkorb gelegt und im Lager reserviert
    // synchronized: es können nicht zwei gleichzeitig die Methode aufrufen,
    // so wird verhindert, dass mehr reserviert wird als im Lager vorhanden ist
    public synchronized boolean putBasket(Customer customer, Product product, int quantity) throws WrongAmountBulkException, StockNotEnoughException
    {
        boolean ok = shoppingManagement.putBasket(customer, product, quantity);
        productManagement.save();
        return ok;
    }


    /**
     * Methode, um Produktmenge im Warenkorb zu reduzieren 
     * 
     * @param customer betreffender Kunde
     * @param product zu reduzierendes Produkt
     * @param quantity Menge des Produkts
     * @return ob erfolgreich reduziert
     */
    // etwas aus dem Warenkorb reduzieren und Reservierung im Lager reduzieren
    public boolean reduceBasket(Customer customer, Product product, int quantity)
    {
        boolean ok = shoppingManagement.reduceBasket(customer, product, quantity);
        productManagement.save();
        return ok;
    }


    /**
     * Methode, um ein Produkt aus dem Warenkorb zu entfernen
     * 
     * @param customer betreffender Kunde
     * @param product zu entfernendes Produkt
     */
    // Produkt aus dem Warenkorb entfernen
    public void removeInBasket(Customer customer, Product product)
    {
        shoppingManagement.removeInBasket(customer, product);
        productManagement.save();
    }


    /**
     * Methode, um die Menge eines Produktes im Warenkorb herauszufinden
     * 
     * @param customer betreffender Kunde
     * @param product Produkt, von dem man die Menge wissen möchte
     * @return Menge des Produktes
     */
    // Menge für ein Produkt ausgeben
    public int getQuantity( Customer customer,  Product product)
    {
        return shoppingManagement.getQuantity(customer, product);
    }


    /**
     * Methode um Warenkorb als String zu bekommen
     * 
     * @param customer betreffender Kunde
     * @return Liste der Produkte im Warenkorb als String
     */
    // Liste der Produkte im Warenkorb als String
    public String getBasketList( Customer customer)
    {
        return shoppingManagement.getBasketList(customer);
    }


    /**
     * Methode, um Warenkorb als ArrayList zu bekommen
     * 
     * @param customer betreffender Kunde
     * @return Liste der Produkte im Warenkorb als ArrayList
     */
    // Liste der Produkte im Warenkorb als ArrayList
    public ArrayList<ProductQuantity> getBasket( Customer customer)
    {
        return shoppingManagement.getBasket(customer);
    }


    /**
     * Methode, um den Inhalt des Warenkorbs zu kaufen:
     *  Es wird eine Rechnung erstellt, Produktmengen im Lager reduziert
     *  und der Warenkorb geleert
     * 
     * @param customer betreffender Kunde
     */
    // Kaufen:  -eine Rechnung erstellen und anzeigen
    //          -Lager reduzieren und Reservierung reduzieren
    //          -Warenkorb leeren
    public void buy(Customer customer)
    {
        shoppingManagement.buy(customer, logManagement, productManagement);
        productManagement.save();
    }


    /**
     * Warenkorb wird geleert und Produktliste persistent gespeichert
     * Log wird persistent gespeichert
     */
    public void cleanC(Customer customer)
    {
        shoppingManagement.cleanC( customer);
        productManagement.save();
        logManagement.save();
    }
    /**
     * Produktliste wird persistent gespeichert
     * Log wird persistent gespeichert
     */
    public void clean()
    {
        productManagement.save();
        logManagement.save();
    }


    /** für die Preise:   rundet kaufmännisch auf zwei Nachkommastellen
     *                    fügt Euro zum Preis hinzu
     *                    ersetzt . durch , (für deutsche Schreibweise)
     */ 
    public static String convertPrice(double d){
        return ShoppingManagement.convertPrice(d);
    }


}
