package domain.management;

import java.io.IOException;
import java.util.*;

import domain.exceptions.EmployerAlreadyExists;
import domain.lists.EmployerList;
import persistance.EmployerPersistence;
import valueObjects.Employer;

//test test
/**
 * Klasse zur Verwaltung der Mitarbeiter
 * 
 * 
 */
public class EmployerManagement {
    private EmployerList employerList;
    private EmployerPersistence ep;

    public EmployerManagement() {
        this.employerList = new EmployerList();
        this.ep = new EmployerPersistence();

        try {
            employerList = ep.loadAllEmployer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean save() {
        boolean success = true;

        try {
            ep.saveAllEmployer(employerList);
        } catch (IOException e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }

    /**
     * Methode um sich im Programm als Mitarbeiter anmelden zu koennen.
     * 
     * @param username
     * @param password
     * @return korrektes Employer-Objekt, sonst null
     */
    public Employer login(String username, String password) {
        Employer toLogIn = null;

        for (Employer employer : this.employerList.getEmployerList()) {
            if (employer.getUsername().equals(username) && employer.getPassword().equals(password)) {
                toLogIn = employer; // Rueckgabe des passenden Mitarbeiters
            }
        }
        return toLogIn;
    }

    public Employer getEmployer(String firstName, String lastName) {
        Employer foundEmployer = null;
        for (Employer employer : this.employerList.getEmployerList()) {
            if (employer.getFirstname().equals(firstName) && employer.getLastname().equals(lastName)) {
                foundEmployer = employer;
            }
        }
        return foundEmployer;
    }

    /**
     * Methode zum Hinzufügen von Mitarbeitern in die Liste
     * 
     * 
     * @param employer , der hinzugefügt werden soll
     * @throws EmployerAlreadyExists
     * 
     */
    public boolean addEmployer(Employer employer) throws EmployerAlreadyExists 
    {
        boolean success = false;
        success = employerList.addEmployer(employer);
        if(success)
        {
            success = save();
        }
        return success;
    }


    /**
	 * Methode macht Objekt für Nutzer unsichtbar, durch product.getDeleted = true. 
	 * Objekt ist im System noch vorhanden. 
	 * 
	 * @param firstname, lastname des Mitarbeiters.
	 */
    public boolean removeEmployer(String username)
    {
        boolean success = this.employerList.removeEmployer(username);
        if(success)
        {
            success = save();
        }
        return success;
    }
    /**
	 * Methode um mit einem Username nach einem Mitarbeiter zu suchen.
     * 
	 * @return Employer , wenn er  in der Liste gefunden wird sonst null
	 */

    public ArrayList<Employer> searchEmployerByUsername(String searchUsername){
    
        return this.employerList.searchEmployerByUsername(searchUsername);
    }
    /**
	 * Methode zum Abfragen der Mitarbeiterliste.
     * 
	 * @return Liste der Mitarbeiter.
	 */
    public ArrayList <Employer> getEmployerList()               
    {
        return this.employerList.getEmployerList();
    }
    

    public Employer searchEmployerByNumber(long number) 
	{
        return this.employerList.searchEmployerByNumber( number);
    }


    /**
	 * Methode zum Setzen der Mitarbeiterliste.
     * 
	 * @return Neue Liste der Mitarbeiter.
	 */
    public void setEmployerList(EmployerList employerList)
    {
        this.employerList = employerList;
    }



    /**
    * toString Methode für die Ausgabe der Mitarbeiterliste
    *
    * @return string
    */
    public String toString()
    {                              
        return "\n" +
            "Mitarbeiterliste:" + "\n" + this.employerList.toString();             
    }

}