package domain.management;

import java.io.IOException;
import java.util.ArrayList;

import domain.lists.ProductList;
import domain.logging.LogEntry;
import persistance.ProductPersistence;
import valueObjects.Person;
import valueObjects.Product;
import domain.exceptions.*;

/**
 * Klasse zur Verwaltung von Produkten.
 * 
 * @author Majbrit
 * @version 2
 */
public class ProductManagement {

	private ProductList productlist;
	ProductPersistence pp;

	public ProductManagement() 
	{
		this.productlist = new ProductList();
		pp = new ProductPersistence();
		try {
			productlist = pp.loadAllProducts();
		} catch (IOException | ProductAlreadyExistsException e) {
			e.printStackTrace();	
		} 

	}


	/**
	 * Methode zum Einlesen von Produktdaten aus einer Datei.
	 * 
	 * @param datei Datei, die einzulesenden Produktbestand enthÃ¤lt
	 * @throws IOException
	 */
	public void readData(String datei) // throws IOException
	{

	}

	/**
	 * Methode zum Schreiben der Produktdaten in eine Datei.
	 * 
	 * @param datei Datei, in die der Produktbestand geschrieben werden soll
	 * @throws IOException
	 */
	public void writeData(String datei)// throws IOException
	{

	}

	public Product getProduct(String productName) 
	{
		Product foundProduct = null;
		for (Product product : this.productlist.getProductList()) 
		{
			if (product.getName().equals(productName)) 
			{
				foundProduct = product;
			}
		}
		return foundProduct;
	}

	/**
	 * Methode, die ein Produkt an das Ende der Produktliste einfügt.
	 * 
	 * @param product das einzufügende Produkt
	 * @throws productAlreadyExistsException wenn das Produkt bereits existiert
	 */
	public boolean addProduct(Product product) throws ProductAlreadyExistsException
	{ 
		boolean ok = false;
		ok = productlist.addProduct(product);
		if (ok) {
			ok = save();
		}
		return ok;
	}

	public boolean save() 
	{
		boolean ok = true;
		try {
			pp.saveAllProducts(productlist);
		} catch (IOException e) {
			e.printStackTrace();
			ok = false;
		}
		return ok;
	}

	/**
	 * Methode macht Objekt für Nutzer unsichtbar, durch product.getDeleted = true. 
	 * Objekt ist im System noch vorhanden. 
	 * @throws ProductDoesntExistException 
	 * 
	 * @param product das zu löschende Produkt @throws
	 */
	public boolean removeProduct(Person person, String name, LogManagement log) throws ProductDoesntExistException{
		boolean ok = false;
		Product product = searchProduct(name);
		int quantity = product.getStock();

		
		ok = productlist.deactivateProduct(product); // Produkt wird aus der Liste gelöscht

		if (ok)
		{
			ok = save();
			log.addLogEntry(new LogEntry(product, person, product.getStock(), LogEntry.MOVE_OUTSTORAGE));
		}
		return ok;
	}

	/**
	 * Methode, die die Produktliste nach dem gesuchten Product sucht und einen
	 * boolean zurückgibt , wenn dieses Produkt bereits vorhanden ist
	 * 
	 * @param compareProduct Produkt das verglichen wird mit der Liste 
	 * @return true bei gefunden , false bei nicht gefunden
	 */
	public boolean compareProduct(Product compareProduct){
		return productlist.compareProduct(compareProduct);	
	}





	/**
	 * Methode, die anhand eines Produktnamens nach Produkten sucht. Es wird eine Liste von Produkten
	 * zurückgegeben, die alle Produkte mit exakt Übereinstimmendem Produktnamen enthält.
	 * 
	 * @param name Name des gesuchten Produkts
	 * @return Liste der Produkte mit gesuchtem Namen (evtl. leer)
	 */
	public ArrayList<Product> searchProducts(String name) 
	{
		return productlist.searchProducts(name);
	}


	/**
	 * Methode, die anhand eines Preises nach Produkten sucht. Es wird eine Liste von Produkten
	 * zurückgegeben, die alle Produkte mit exakt Übereinstimmendem Preis enthält.
	 * 
	 * @param price Preis der gesuchten Produkte
	 * @return Liste der Produkte mit gesuchtem Preis (evtl. leer)
	 */
	public ArrayList<Product> searchProductsPrice(double price) 
	{
		return productlist.searchProductsPrice(price);
	}

	
	/**
	 * Methode, die eine KOPIE des Produktbestands zurückgibt.
	 * (Eine Kopie ist eine gute Idee, wenn ich dem Empfänger 
	 * der Daten nicht ermöglichen möchte, die Original-Daten 
	 * zu manipulieren.)
	 * ACHTUNG: die in der kopierten Produktliste referenzierten
	 * 			sind nicht kopiert worden, d.h. ursprüngliche
	 * 			Produktliste und ihre Kopie verweisen auf dieselben
	 * 			Produkt-Objekte.
	 * 
	 * @return Liste aller Produkte im Produktbestand (Kopie)
	 */
	public ArrayList<Product> getProductList() 
	{
		return productlist.getProductList();
	}
	

	/**
	 * Methode sortiert Produkte nach Artikelnummer
	 * 
	 * @return vector mit Produktliste, die nach der Artikelnummer(man) sortiert ist
	 */
	public ArrayList<Product> sortMan() 
	{
		return productlist.sortMan();
	}

	
	/**
	 * Methode sortiert Produkte nach Namen
	 * 
	 * @return vector mit Produktliste, deren Namen nach Alphabet sortiert sind
	 */
	public ArrayList<Product> productlistSortName() 
	{
		return productlist.productlistSortName();
	}

	
	public boolean setStock(Person person, String name, int newStock, LogManagement log) throws ProductDoesntExistException
	{
		Product product = productlist.searchProduct(name);
		return setStock(person, product, newStock, log);
	}


	public boolean setStock(Person person, Product product, int newStock, LogManagement log)
	{
        int stockChange = newStock - product.getStock();
        String move;
        if(stockChange >= 0)
        {
            move = LogEntry.MOVE_STORAGE;
        }
        else
        {
            stockChange = -stockChange;
            move = LogEntry.MOVE_OUTSTORAGE;
        }
		boolean ok = product.setStock(newStock);
		/*System.out.println(product);
		   System.out.println(person);
		   System.out.println(stockChange);
		   System.out.println(move);*/
		if (ok){
		   log.addLogEntry(new LogEntry(product, person, stockChange, move));
		   /*System.out.println(product);
		   System.out.println(person);
		   System.out.println(stockChange);*/
		   System.out.println(move);
			ok = save();
		}
		return ok;
	}


	public boolean reduceStock(Person person, Product product, int newStock, LogManagement log)
	{
        
        String move = LogEntry.MOVE_SOLD;
		product.reduceStock(newStock);
        log.addLogEntry(new LogEntry(product, person, newStock, move));
		boolean ok = save();
		return ok;
	}


	public Product searchProduct(String searchProduct) throws ProductDoesntExistException
	{
		return productlist.searchProduct(searchProduct);
	}


	public Product searchProductByNumber(long number) 
	{
        return this.productlist.searchProductByNumber( number);
	}
	
	
	//ursprüngliche ArrayList als String ausgeben
	public String toString() 
	{
		return productlist.toString();
	}
	

	//neu erstellte ArrayList, der die sortierten Daten bekommt als String ausgeben
	public String arrayToString(ArrayList<Product> v) 
	{
		return productlist.arrayToString(v);
	}


}