package domain.management;

import java.io.IOException;
import java.util.ArrayList;

import domain.Eshop;
import domain.exceptions.CantFindLogException;
import domain.lists.LogList;
import domain.logging.LogEntry;
import persistance.LogPersistence;

public class LogManagement 
{
	LogPersistence lp;
	LogList ll;
	LogList logentrys;
    


	public LogManagement(CustomerManagement cm, EmployerManagement em, ProductManagement pm)
	{
		lp = new LogPersistence();
		// Liste Kunden Produkt Mitarbeiter übergeben
        this.logentrys = new LogList();
        lp = new LogPersistence();
		try {
			logentrys = lp.loadAllLogEntrys(cm, em, pm);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    /**
     * Die Methode speichert die Logeinträge persistent
     * 
     * @return Gibt zurück, ob die Logeinträge erfolgreich gespeichert werden konnten
     */
	public boolean save()
	{
		boolean ok = true;
		try {
			lp.saveAllLogEntrys(this);
		} catch (IOException e) {
			e.printStackTrace();
			ok = false;
		}
		return ok;
	}


    /** Fuegt LogEntry in die Log-Liste hinzu.
    *
    * @param newLogEntry, neuer Eintrag.
    */
    public void addLogEntry(LogEntry newLogEntry)
    {
        this.logentrys.addLogEntry(newLogEntry);
    }


    /**
     * Leert die vorhandene Log-Liste.
     */
    public void emptyLog()
    {
        this.logentrys.emptyLogList();
    }


    /** Entfernt LogEntry an bestimmter Stelle, aus der Log-List.
     * 
     * @param int index
     */
    public void removeLogEntryByIndex(int index)
    {
        this.logentrys.removeLogEntryByIndex(index);        
    }


    /** Erhalte bestimmten LogEntry von Stelle index.
     * 
     * @param index
     * @return LogEntry
     */
    public LogEntry getLogEntryByIndex(int index) throws CantFindLogException
    {
        
        return this.logentrys.getLogEntryByIndex(index);
    }


    /** Zeige alle Eintraege des Logs, im Terminal
     */
    public void showTotalLog()
    {
        this.logentrys.showTotalLog();
    }

    /**
     * 
     * @param name
     */
    public void showProductLog(String name)
    {
        this.logentrys.showProductLog(name);
    }


    /** Bekomme LogList-Objekt
     * 
     * @return ArrayList-Objekt
     */
    public ArrayList<LogEntry> getLogList()
    {
        return this.logentrys.getLogList();
    }


    /** Setze LogList neu
     * 
     * @param newLogList
     */
    public void setLog(LogList newLogList)
    {
        this.logentrys = newLogList;
    }



    public ArrayList<LogEntry> getAllSpecificEntries(String name)
	{
		ArrayList<LogEntry> list = this.logentrys.getAllSpecificEntries(name);
		return list;
	}

    


    
}