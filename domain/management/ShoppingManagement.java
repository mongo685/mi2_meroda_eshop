package domain.management;

import java.math.BigDecimal;
import java.util.ArrayList;

import domain.logging.LogEntry;
import valueObjects.Bill;
import valueObjects.BulkProduct;
import valueObjects.Customer;
import valueObjects.Product;
import valueObjects.ProductQuantity;
import domain.exceptions.*;

public class ShoppingManagement 
{
   //private ProductManagement productManagement = new ProductManagement();

   /**
     * Methode, um Produkte in den Warenkorb zu legen oder die Menge zu erhöhen
     * 
     * @param customer betreffender Kunde
     * @param product betreffendes Produkt
     * @param quantity Menge des Produktes
     * @return ob erfolgreich in den Warenkorb gelegt
     * @throws WrongAmountBulkException bei falscher Mengenangabe , StockNotEnoughException bei überschreitung des Lagerbestandes
     */
    // etwas wird in den Warenkorb gelegt und im Lager reserviert
    // synchronized: es können nicht zwei gleichzeitig die Methode aufrufen,
    // so wird verhindert, dass mehr reserviert wird als im Lager vorhanden ist
    public synchronized boolean putBasket(Customer customer, Product product, int quantity) throws WrongAmountBulkException, StockNotEnoughException
    {
        // fuktioniert nur, wenn noch genug im Lager ist
        boolean ok = true;
        if (product instanceof BulkProduct){
            BulkProduct bp = (BulkProduct)product;
            ok = bp.checkQuantity(quantity);
        }
        if (ok){
            if(product.addReserved(quantity))
            {  
                customer.getBasket().put(product, quantity);
                return ok;
            } else {
                throw new StockNotEnoughException(product);
                
            }
        }
        return ok;
    }


    /**
     * Methode, um Produktmenge im Warenkorb zu reduzieren 
     * 
     * @param customer betreffender Kunde
     * @param product zu reduzierendes Produkt
     * @param quantity Menge des Produkts
     * @return ob erfolgreich reduziert
     */
    // etwas aus dem Warenkorb reduzieren und Reservierung im Lager reduzieren
    public boolean reduceBasket(Customer customer, Product product, int quantity) 
    {
        // funtioniert nur, wenn quantity nicht mehr als, das was im Warenkorb ist, ist
        try{
        if(customer.getBasket().reduce(product, quantity))
        {
            product.reduceReserved(quantity);
            // Produkt entfert wenn auf null
            return true;
        }
    }catch (WrongAmountBulkException e){
        e.printStackTrace();   
       }
        return false;  
    }
        

    /**
     * Methode, um ein Produkt aus dem Warenkorb zu entfernen
     * 
     * @param customer betreffender Kunde
     * @param product zu entfernendes Produkt
     */
    // Produkt aus dem Warenkorb entfernen
    public void removeInBasket(Customer customer, Product product)
    {
        customer.getBasket().remove(product);
    }


    /**
     * Methode, um die Menge eines Produktes im Warenkorb herauszufinden
     * 
     * @param customer betreffender Kunde
     * @param product Produkt, von dem man die Menge wissen möchte
     * @return Menge des Produktes
     */
    // Menge für ein Produkt ausgeben
    public int getQuantity( Customer customer,  Product product)
    {
        return customer.getBasket().getProductStock(product);
    }


    /**
     * Methode um Warenkorb als String zu bekommen
     * 
     * @param customer betreffender Kunde
     * @return Liste der Produkte im Warenkorb als String
     */
    // Liste der Produkte im Warenkorb als String
    public String getBasketList( Customer customer)
    {
        return customer.getBasket().toString();
    }


    /**
     * Methode, um Warenkorb als ArrayList zu bekommen
     * 
     * @param customer betreffender Kunde
     * @return Liste der Produkte im Warenkorb als ArrayList
     */
    // Liste der Produkte im Warenkorb als ArrayList
    public ArrayList<ProductQuantity> getBasket( Customer customer)
    {
        return customer.getBasket().getProdList();
    }


    /**
     * Methode, um den Inhalt des Warenkorbs zu kaufen:
     *  Es wird eine Rechnung erstellt, Produktmengen im Lager reduziert
     *  und der Warenkorb geleert
     * 
     * @param customer betreffender Kunde
     */
    // Kaufen:  -eine Rechnung erstellen und anzeigen
    //          -Lager reduzieren und Reservierung reduzieren
    //          -Warenkorb leeren
    public void buy(Customer customer, LogManagement log, ProductManagement pm)
    {
        Bill bill = new Bill(customer);
        bill.printCuiBill();
        ArrayList<ProductQuantity> list = customer.getBasket().getProdList();
        for(ProductQuantity product : list)
        {
           //log.addLogEntry(new LogEntry(product.getProduct(), customer, product.getQuantity(), LogEntry.MOVE_SOLD));
            pm.reduceStock(customer, product.getProduct(), product.getQuantity(), log);
            //product.getProduct().reduceStock(product.getQuantity());
            product.getProduct().reduceReserved(product.getQuantity());
        }
        customer.getBasket().clear();
    }



    /**
     * Warenkork wird geleert
     */
    public void cleanC(Customer customer)
    {
        ArrayList<ProductQuantity> list = customer.getBasket().getProdList();
        for(ProductQuantity product : list)
        {
            product.getProduct().reduceReserved(product.getQuantity());
        }
        customer.getBasket().clear();
    }

    /** für die Preise:   rundet kaufmännisch auf zwei Nachkommastellen
     *                    fügt Euro zum Preis hinzu
     *                    ersetzt . durch , (für deutsche Schreibweise)
     */ 
    public static String convertPrice(double d){
        BigDecimal b = new BigDecimal(d);
        b = b.setScale(2, BigDecimal.ROUND_HALF_UP); // nur für Java 8, in Java 9 herabgestuft
        String s = b.toString() + " Euro";
        s = s.replace(".", ",");
        return s;
    }
}