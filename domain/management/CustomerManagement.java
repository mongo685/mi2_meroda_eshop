package domain.management;

import java.io.IOException;
import java.util.ArrayList;

import domain.exceptions.CustomerAlreadyExists;
import domain.lists.CustomerList;
import persistance.CustomerPersistence;
import valueObjects.Customer;

public class CustomerManagement {
    private CustomerList customerList;
    private CustomerPersistence cp;


    public CustomerManagement() 
    {
        this.customerList = new CustomerList();
        cp = new CustomerPersistence();

        try {
            customerList = cp.loadAllCustomer();
        } catch (IOException | CustomerAlreadyExists e) {
            e.getMessage();
        }
    }


    /**
     * Speichere alle Eintraege die im Moment im System gespeichert sind.
     * 
     * @return true bei Erfolg, sonst false
     */
    public boolean save() 
    {
        boolean success = true;

        try {
            cp.saveAllCustomer(customerList);
        } catch (IOException e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }


    /**
     * Methode um sich im Programm als Kunde anmelden zu koennen.
     * 
     * @param username
     * @param password
     * @return korrektes Customer-Objekt, sonst null
     */
    public Customer login(String username, String password) {
        Customer toLogIn = null;
        for (Customer customer : this.customerList.getCustomerList()) {
            if (customer.getUsername().equals(username) && customer.getPassword().equals(password)) {
                toLogIn = customer; // Rueckgabe des passenden Kunden
            }
        }
        return toLogIn;
    }


    /**
     * Erhalte Customer durch Username
     * 
     * @param searchUsername
     * @return Customer
     */
    public ArrayList<Customer> searchCustomerByUsername(String searchUsername) 
    {

        return this.customerList.searchCustomerByUsername(searchUsername);
    }


    /**
     * Erhalte Customer durch MUN 
     * 
     * @param number
     * @return Customer
     */
    public Customer searchCustomerByNumber(long number) 
    {
        return this.customerList.searchCustomerByNumber(number);
    }


    /**
     * Erhalte Customer durch Vor- und Nachnamen.
     * 
     * @param firstName
     * @param lastName
     * @return Customer
     */
    public Customer getCustomer(String firstName, String lastName) 
    {
        Customer foundCustomer = null;
        for (Customer customer : this.customerList.getCustomerList()) 
        {
            if (customer.getFirstname().equals(firstName) && customer.getLastname().equals(lastName)) 
            {
                foundCustomer = customer;
            }
        }
        return foundCustomer;
    }


    /**
     * Methode zum Hinzufuegen von Kunden in die Liste.
     * 
     * @param newCustomer
     * @return true bei Erfolg, sonst false
     * @throws CustomerAlreadyExists
     */
    public boolean addCustomer(Customer customer) throws CustomerAlreadyExists
    {
        boolean success = false;
        success = customerList.addCustomer(customer);
        if(success) 
        {
            success = save();
        }
        return success;
    }

    /**
     * Methode macht Objekt für Nutzer unsichtbar, durch product.getDeleted = true. 
	 * Objekt ist im System noch vorhanden. 
     */
    public boolean removeCustomer(String username)
    {
        boolean success = this.customerList.removeCustomer(username);
        if(success)
        {
            //? try save() -> SoveNotSuccessfullException?
            success = save();
            //? Muss hier ein LogEntry rein?
        }
        return success;
    }


    /**
     * Leehrt die CustomerListe 
     */
    public void emptyCustomerList()
    {
        this.customerList.emptyCustomerList();
    }


    /**
     * Gibt die CustomerList als kopierte ArrayListe zurueck
     * 
     * @return ArrayList<Customer> die CustomerListe
     */
    public ArrayList<Customer> getCustomerList()
    {
        return this.customerList.getCustomerList();
    }


    /**
     * Setzt neue CustomerListe
     * 
     * @param customerList
     */
    public void setCustomerList(CustomerList customerList)
    {
        this.customerList = customerList;
    }


    public String toString()
    {
        return "{" +
            "customerManagement='" + this.customerList.toString() + "'" +
            "}";
    }

}