package domain.logging;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import valueObjects.Customer;
import valueObjects.Person;
import valueObjects.Product;

public class LogEntry
{
    private String message;
    private int dateymd; // Datum in der Reihenfolge Jahr Monat Tag // Date nehmen
    private int time;
    private int quantity;
    private Product product;
    private Person person;
    private String personType;
    private String move;
    public final static String MOVE_STORAGE = "E"; // E fuer Einlagerung
    public final static String MOVE_OUTSTORAGE = "A"; // A fuer Auslagerung
    public final static String MOVE_SOLD = "V"; // V fuer verkauft
    private DateTimeFormatter dayMonthYearAtTime = DateTimeFormatter.ofPattern("dd-MM-yyyy' um 'HH:mm:ss");
    private long personNumber;
    private long productNumber;


    public LogEntry(Product product, Person person, int quantity, String move)
    {
        this.person = person;
        this.product = product;
        this.personNumber = person.getMUN().getNumber();
        this.productNumber = product.getMUN().getNumber();
        this.quantity = quantity;
        this.move = move;
        LocalDateTime ldt = LocalDateTime.now();
        // z.B. das Datum 09.05.2020 wird gespeichert als 20200509
        this.dateymd = ldt.getYear()*10000 + ldt.getMonthValue()*100 + ldt.getDayOfMonth(); 
        this.time = ldt.getHour()*10000 + ldt.getMinute()*100 +ldt.getSecond();
        //Logeintrag als Nachricht
        createMessage();
    }

    public LogEntry(String message)
    {
        this.message = message;
    }

    // fuer Persistenz
    public LogEntry()
    {

    }


    /**
     * Die Methode erstellt eine Nachricht über den Logeintrag
     */
    public void createMessage()
    {
        int y = dateymd/10000;
        int m = dateymd/100 - y*100;
        int d = dateymd - y*10000 - m*100;
        int h = time/10000;
        int min = time/100 - h*100;
        int s = time - h*10000 - min*100;
        LocalDateTime ldt = LocalDateTime.of(y, m, d, h, min, s);

        String dateS = ldt.format(dayMonthYearAtTime);
        String how = "";
        if(this.move.equals("E"))
        {
            how = " eingelagert von Mitarbeiter ";
        }
        else if(this.move.equals("A"))
        {
            how = " ausgelagert von Mitarbeiter ";
        }
        else
        {
            how = " verkauft an Kunde ";
        }

        String pm = "unbekannt";
        if (product != null){
            pm = this.product.getName();
        }
        String pem = "unbekannt";
        if (person != null){
            pem = this.person.getLastname() + ", " + this.person.getFirstname();
        }
        message = this.quantity + " Stück von Artikel " + pm + " am " + dateS
                + how + pem;

    }


    public String getAction()
    {
        String action = "";
        if(this.move.equals("E"))
        {
            action = " eingelagert von Mitarbeiter ";
        }

        else if(this.move.equals("A"))
        {
            action = " ausgelagert von Mitarbeiter ";
        }

        else
        {
            action = " verkauft an Kunde ";
        }
        return action;
    }

    public String createDateAndTime()
    {
        int y = dateymd/10000;
        int m = dateymd/100 - y*100;
        int d = dateymd - y*10000 - m*100;
        int h = time/10000;
        int min = time/100 - h*100;
        int s = time - h*10000 - min*100;
        LocalDateTime ldt = LocalDateTime.of(y, m, d, h, min, s);

        String dateS = ldt.format(dayMonthYearAtTime);
        return dateS;
    }

    
    public int getDateymd() 
    {
        return this.dateymd;
    }

    public void setDateymd(int dateymd) 
    {
        this.dateymd = dateymd;
    }

    public int getTime() 
    {
        return this.time;
    }

    public void setTime(int time) 
    {
        this.time = time;
    }

    public int getQuantity() 
    {
        return this.quantity;
    }

    public void setQuantity(int quantity) 
    {
        this.quantity = quantity;
    }

    public Product getProduct() 
    {
        return this.product;
    }

    public void setProduct(Product product) 
    {
        this.product = product;
    }

    public Person getPerson() 
    {
        return this.person;
    }

    public void setPerson(Person person) 
    {
        this.person = person;
    }

    public String getMove() 
    {
        return this.move;
    }

    public void setMove(String move) 
    {
        this.move = move;
    }

    public String getPersonType() 
    {
        return this.personType;
    }

    public void setPersonType(String personType) 
    {
        this.personType = personType;
    }

    public String getMessage()
    {
        return this.message; 
    }

    public void setMessage(String newMessage)
    {
        this.message = newMessage;
    }

    public long getPersonNumber() 
    {
        return personNumber;
    }

    public void setPersonNumber(long personNumber) 
    {
        this.personNumber = personNumber;
    }

    public long getProductNumber() 
    {
        return productNumber;
    }

    public void setProductNumber(long productNumber) 
    {
        this.productNumber = productNumber;
    }




    
    @Override
   public String toString()
    {
        return "| Log-message = '" + ((this.getMessage() == null) ? "no message here" : this.getMessage()) + "' |";
    }
}