package domain.exceptions;

public class CantFindLogException extends Exception 
{
    public CantFindLogException(int index)
    {
        //super("Es Konnten keine Ergebnisse zu der Suchanfrage " + string + " gefunden werden" );
        super("Es Konnten keine Logeintäge mit dem Index " + index + " gefunden werden" );
    }
}