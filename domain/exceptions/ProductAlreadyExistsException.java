package domain.exceptions;
import valueObjects.Product;

public class ProductAlreadyExistsException extends Exception {
    
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final Product product;
    /**
    * Constructor
    * 
    * @param username , Username des Nutzer dessen Login nicht erfolgreich war
    */

    public ProductAlreadyExistsException(Product product)
    {
        super("Das Produkt" + product.getName() + " befindet sich bereits im Sortiment! Bitte verwenden Sie den Befehl für die Änderung des Lagerbestands existierender Artikel." );
        this.product = product;
    }
    
    public Product getProduct()
    {
    return product;    
    }
}
