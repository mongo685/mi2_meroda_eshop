package domain.exceptions;

import valueObjects.Product;

public class StockNotEnoughException extends Exception
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor
     * 
     * @param productname , Produktname des nicht vorhandenen Produktes
     */
    public StockNotEnoughException(Product product)
    {
        super("Die gewollte Menge des Produktes:  " + product.getName() + " überschreitet den maximalen Lagerbestand von: "+ product.getStock() +".  Bitte wählen Sie eine neue geeignete Menge.");
    }
  
}

