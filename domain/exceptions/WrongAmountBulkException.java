package domain.exceptions;

public class WrongAmountBulkException extends Exception{

   
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor
     * 
     * @param product  BulkProduct
     * @param quantity Menge von Bulkproduct
     */

    public WrongAmountBulkException(int packsize, int quantity)
    {
        super("Die gewünschte Menge: " + quantity + " ist kein Vielfaches von " + packsize +
         ". Bitte versuchen Sie es erneut mit der richtigen Menge." );
    }
    
}
 
