package domain.exceptions;

import valueObjects.Customer;

public class CustomerAlreadyExists extends Exception
{
    private static final long serialVersionUID = 1L;
    Customer affectedCustomer;
    
    public CustomerAlreadyExists(Customer affectedCustomer)
    {
        super("Der Benutzername " + affectedCustomer.getUsername() + " ist bereits belegt");
    }
    
}