package domain.exceptions;

public class ProductDoesntExistException extends Exception
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor
     * 
     * @param productname , Produktname des nicht vorhandenen Produktes
     */
    public ProductDoesntExistException(String productname)
    {
        super("Das Produkt mit dem Namen " + productname + " existiert nicht.");
    }
  
}









