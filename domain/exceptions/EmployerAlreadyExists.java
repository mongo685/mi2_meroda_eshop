package domain.exceptions;

import valueObjects.Employer;

public class EmployerAlreadyExists extends Exception
{
    private static final long serialVersionUID = 1L;
    Employer affectedEmployer;
    
    public EmployerAlreadyExists(Employer affectedEmployer)
    {
        super("Der Benutzername " + affectedEmployer.getUsername() + " ist bereits belegt");
    }
    
}