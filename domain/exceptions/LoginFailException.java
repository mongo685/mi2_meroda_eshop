package domain.exceptions;

public class LoginFailException extends Exception{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor
     * 
     * @param username , Username des Nutzer dessen Login nicht erfolgreich war
     */

    public LoginFailException(String username)
    {
        super("Der Nutzer mit dem Nutzernamen: " + username + " konnte nicht eingeloggt werden. Bitte überprüfen Sie die Zugangsdaten und versuchen es erneut." );
    }
    
}

