package domain.lists;

import java.util.*;

import domain.exceptions.EmployerAlreadyExists;
import valueObjects.Employer;

//! Dateinamen-Endung: .elt

public class EmployerList
{
    // Erstellt die Employerlist
    private ArrayList<Employer> employerList ;

    public EmployerList() 
    {
        this.employerList = new ArrayList<Employer>();   
        // Ein immer angelegter default employer, wie der Root <emroot, emroot>
        // this.employerList.add(new Employer());
    }



     /**
     * Setze deleted Flage, des gesuchten Employers, auf true
     */
    public boolean removeEmployer(String username)
    {
        boolean success = false;
        for(Employer employer : employerList)
        {
            if(employer.getUsername().equals(username))
            {
                employer.setDeleted(true);
                success = true;
            }
        }
        return success;
    }
    

    public Employer searchEmployer(String firstname, String lastname, long employernumber)
    {
        for(Employer employer : this.employerList)
        {
            if(employer!=null && firstname.equals(employer.getFirstname()) && lastname.equals(employer.getLastname()) && employer.getNumber() == employernumber)
            {
                return employer;
            }
        }
        return null;
    }

    
    /**
	 * Methode zum Hinzufügen von Mitarbeitern in die Liste.
     * Die Methode prueft erst ob es dieses Objekt bereits gibt, Vergleich durch Attribut "username",
     * wenn Username bereits in der Liste belegt, dann erfolgt kein Hinzufuegen
     * 
	 * @param givenEmployer , der hinzugefügt werden soll.
     * @return true bei Erfolg, sonst false.
	 */
    public boolean addEmployer(Employer givenEmployer) throws EmployerAlreadyExists
    {
        // Pruefe ob der gesuchte Employer bereits in der Liste vorkommt.
        boolean success = checkAlreadyExist(givenEmployer);

        if(success)
        {
            throw new EmployerAlreadyExists(givenEmployer);
        }

        if(this.employerList.add(givenEmployer))
        {
            success = true;
        } else {
            success = false;
        }

        return success;
    }

    
    /**
     * Methode, um ein Mitarbeiter in der Mitarbeiterliste nach seinem Username zu suchen.
     * 
     * @param username , des gesuchten Mitarbeiters
     * @return true bei finden des Mitarbeiters in der Liste , sonst false
     */
    public ArrayList<Employer> searchEmployerByUsername(String searchUsername)
	{
		ArrayList<Employer> foundEmployer = new ArrayList<Employer>();
		
		for (Employer employer : this.employerList)
		{
			if(employer!=null && employer.getUsername().equals(searchUsername))
			{
                foundEmployer.add(employer);
			}
        }
        
        if (foundEmployer != null)
        {
            return foundEmployer;
        }else{
            return null;
        } 
	}


    /**
     * Vergleicht die EmployerListe ob der gesuchte Employer bereits eingetragen ist.
     * 
     * @return true wenn bereits vorhanden, sonst false
     */
    public boolean checkAlreadyExist(Employer toCheck)
    {
        for(Employer employer : this.employerList)
        {
            if(employer!=null && employer.getUsername().equals(toCheck.getUsername()))
            {
                return true;
            }
        }
        return false;
    }
    
    /**
	 * Methode zum Abfragen der Mitarbeiterliste.
     * 
	 * 
	 * @return Liste der Mitarbeiter.
	 * 
	 */
    public ArrayList<Employer> getEmployerList() 
    {														
        return new ArrayList<Employer>(employerList);
    }
    
     /**
	 * Methode zum Setzen der Mitarbeiterliste.
     * 
	 * 
	 * @return Neue Liste der Mitarbeiter.
	 * 
	 */
    public void setEmployerList(ArrayList<Employer> employerList) 
    {
        this.employerList = employerList;
    }

    //Mitarbeiter suchen fuer LogEntry
	public Employer searchEmployerByNumber(long number) 
	{
		Iterator<Employer> it = employerList.iterator();
		while (it.hasNext()) 
		{
			Employer employer = (Employer) it.next();
			if (employer.getMUN().getNumber() == number) 
			{
				return employer;
			}
		}
		return null;
	}


    // toString Methode für die Ausgabe der Mitarbeiterliste
    @Override
        public String toString()
        {
            return"{" +
            " Hier ist die Mitarbeiterliste'" +  getEmployerList();
        
        }
}
