package domain.lists;

import java.util.ArrayList;

import domain.exceptions.CantFindLogException;
import domain.logging.LogEntry;
import valueObjects.Product;

public class LogList 
{
    private ArrayList <LogEntry> logList;

    public LogList()
    {
        this.logList = new ArrayList<LogEntry>();
    }


    
    /** Methode um LogEntry hinzuzufuegen
     * 
     * @param logentry, der zu hinzuzufuegende Logentry
     */
    public boolean addLogEntry(LogEntry logentry)
    {
        if(this.logList.add(logentry))
        {
            return true;
        } else {
            return false;
        }
    }


    /** Methode um LogEntry an bestimmter stelle zu entfernen
     * 
     * @param index, bestimmte Stelle der Liste
     */
    public void removeLogEntryByIndex(int index)
    {
        this.logList.remove(index);
    }


    /** Methode um LogList zu leeren */
    public void emptyLogList()
    {
        this.logList.clear();
    }

 
    /** Methode um bestimmten Logentry aus der Liste zu erhalten
     * 
     * @return logentry-Objekt
     */
    public LogEntry getLogEntryByIndex(int index) throws CantFindLogException
    { 
        if(index<0 || index>=logList.size())
        {
            CantFindLogException e = new CantFindLogException(index);
            throw e;

        }
        else
        {
        return this.logList.get(index);
        }
    }


    /** Zeigt kompletten Inhalt der List, in der Konsole */
    public void showTotalLog()
    {
        for( LogEntry logentry : this.logList)
        {
            System.out.println( logentry.toString() );
        }
    }

    /** Zeigt alle Eintraege des bestimmten Logs, in der Konsole */
    public void showProductLog(String name)
    {
        System.out.println("Bewegungsprotkoll für Artikel " + name + ":");
        System.out.println("");
        ArrayList<LogEntry> list = getProductLog(name);
        for(LogEntry logentry : list)
        {
            System.out.println(logentry.getMessage());
        }
    }


    /** 
     * Gibt alle ListEintraege eines bestimmten Namens zurueck
     * 
     * @return ArrayList
     */
    public ArrayList<LogEntry> getAllSpecificEntries(String name) 
    {
        ArrayList<LogEntry> list = getProductLog(name);
        return list;
    }


    /** Methode um LogListe zu erhalten
     * 
     * @return Kopie des LogList-Objekts
     */
    public ArrayList<LogEntry> getLogList()
    {
        return new ArrayList<LogEntry>(this.logList);
    }


    /** Methode um LogList zu setzen
     * 
     * @param newLogList
     */
    public void setLogList(ArrayList<LogEntry> newLogList)
    {
        this.logList = newLogList;
    }




    /**
     * Methode, um Logliste für bestimmtes Produkt sortiert nach Datum zu bekommen
     */
    public ArrayList<LogEntry> getProductLog(String productName) 
    {
        ArrayList<LogEntry> oldList = new ArrayList<LogEntry>(this.logList);
        ArrayList<LogEntry> list = new ArrayList<LogEntry>();
        ArrayList<LogEntry> newList = new ArrayList<LogEntry>();
        for (LogEntry logEntry : oldList)
        {
            String pName = "unbekannt";
            if (logEntry.getProduct() != null)
            {
                pName = logEntry.getProduct().getName();
            }
            if(pName.equals(productName))
            {
                list.add(logEntry);
            }    
        }

        while(list.size() > 0)
		{
            int mind = 99999999;
            int mint = 999999;
			int index = -1;
			LogEntry logEntryX = null;
			
			//komplette alte Liste durchgehen
			for(int i = 0; i < list.size(); i++) 
			{
				LogEntry logEntry = list.get(i);
                if(logEntry.getDateymd() <= mind) 
                    if(logEntry.getTime() <= mint){
                    {
                        mind=logEntry.getDateymd();
                        mint=logEntry.getTime();
                        //Index vom LogEntry mit aelteren Datum und Uhrzeit
                        index = i;
                        //LogEntry mit aelterem Datum und Uhrzeit
                        logEntryX = logEntry;
                    }
                }

			}
			//LogEntry mit aelterem Datum und Uhrzeit in die Liste einfuegen
			if(logEntryX != null)
			{
				newList.add(logEntryX);
			}
			//Produkte, die in der neuen liste hinzugefügt wurden, werden aus der alten entfernt
			if(index >= 0) 
			{
				list.remove(index);
			}
		}

        return newList;
    }
    
    
}