package domain.lists;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import valueObjects.Product;
import domain.exceptions.*;
//! Dateinamen-Endung: .plt

/**
 * Klasse .
 * 
 * @author Majbrit
 * @version 1
 */
public class ProductList
{
	// Verwaltung des Produktbestands in einer ArrayList
	//<Product> bedeutet, dass nur Produkte hinzugefügt werden können
	private ArrayList<Product> productList = new ArrayList<Product>();
	
	

	// Persistenz-Schnittstelle, die für die Details des Dateizugriffs verantwortlich ist
	//kommt später
	
	/**
	 * Methode zum Einlesen von Produktdaten aus einer Datei.
	 * 
	 * @param datei Datei, die einzulesenden Produktbestand enthaelt
	 * @throws IOException
	 */
	//kommt später
	public void readData(String datei)// throws IOException 
	{
		
	}

	/**
	 * Methode zum Schreiben der Produktdaten in eine Datei.
	 * 
	 * @param datei Datei, in die der Produktbestand geschrieben werden soll
	 * @throws IOException
	 */
	//kommt später
	public void writeData(String datei)// throws IOException  
	{
		
	}

	
	/**
	 * Methode, die ein Produkt an das Ende der Produktliste einfuegt.
	 * 
	 * @param product das einzufügende Produkt
	 * 
	 *  TODO: Pruefe auf bereits vorhandene Objekte in der Liste, die aber deactivated sind.
	 * @throws productAlreadyExistsException wenn das Produkt bereits existiert
	 */
	public boolean addProduct(Product product) throws ProductAlreadyExistsException
	{
		boolean success = compareProduct(product);
		
		if (success)
		{
			throw new ProductAlreadyExistsException(product);
		}
		
		if(this.productList.add(product))
        {
            	success = true;
    	} else {
            	success = false;
        }
		return success;
		  
	}

	/**
	 * Methode, um ein Produkt in der Produktliste zu suchen.
	 * 
	 * @param product compareProduct das gesucht wird
	 * 
	 * 
	 * @return true bei finden des Produktes in der Liste , sonst false
	 */
	public boolean compareProduct(Product compareProduct)
	{
		Product foundProduct = null;
		
		for (Product product : this.productList)
		{
			if(product!=null && product.getName().equals(compareProduct.getName()))
			{
				return true;
			}
		}
			return false;
	}
		

	/**
	 * Methode, die anhand eines Produktnamens nach Produkten sucht. Es wird eine Liste von Produkten
	 * zurückgegeben, die alle Produkte mit exakt Übereinstimmendem Produktnamen enthält.
	 * 
	 * @param name Name des gesuchten Produkts
	 * @return Liste der Produkte mit gesuchtem Namen (evtl. leer)
	 */
	public ArrayList<Product> searchProducts(String name) 
	{
		ArrayList<Product> result = new ArrayList<Product>();
		// Produktbestand durchlaufen und nach Name suchen
		Iterator<Product> it = productList.iterator();
		while (it.hasNext()) 
		{
			Product product = (Product) it.next();
			if (product.getName().equals(name)&&(!product.getDeleted())) 
			{
				result.add(product);
			}
		}
		return result;
    }
    
    /**
	 * Methode, die anhand eines Preises nach Produkten sucht. Es wird eine Liste von Produkten
	 * zurückgegeben, die alle Produkte mit exakt Übereinstimmendem Preis enthält.
	 * 
	 * @param price Preis der gesuchten Produkte
	 * @return Liste der Produkte mit gesuchtem Preis (evtl. leer)
	 */
	public ArrayList<Product> searchProductsPrice(double price) 
	{
		ArrayList<Product> result = new ArrayList<Product>();
		// Produktbestand durchlaufen und nach Preis suchen
		Iterator<Product> it = productList.iterator();
		while (it.hasNext()) 
		{
			Product product = (Product) it.next();
			if (product.getPrice() == price &&(!product.getDeleted())) 
			{
				result.add(product);
			}
		}
		return result;
	}

	//Produkt suchen fuer LogEntry
	public Product searchProductByNumber(long number) 
	{
		
		
		Iterator<Product> it = productList.iterator();
		while (it.hasNext()) 
		{
			Product product = (Product) it.next();
			if (product.getMUN().getNumber() == number) 
			{
				return product;
			}
		}
		return null;
	}
	
	/**
	 * Methode, die eine KOPIE des Produktbestands zurückgibt.
	 * (Eine Kopie ist eine gute Idee, wenn ich dem Empfänger 
	 * der Daten nicht ermöglichen möchte, die Original-Daten 
	 * zu manipulieren.)
	 * ACHTUNG: die in der kopierten Produktliste referenzierten
	 * 			sind nicht kopiert worden, d.h. ursprüngliche
	 * 			Produktliste und ihre Kopie verweisen auf dieselben
	 * 			Produkt-Objekte.
	 * 
	 * @return Liste aller Produkte im Produktbestand (Kopie)
	 */
	public ArrayList<Product> getProductList() 
	{
		return new ArrayList<Product>(this.productList);
	}
	
	/**
	 * Methode sortiert Produkte nach Artikelnummer
	 * 
	 * @return vector mit Produktliste, die nach der Artikelnummer(man) sortiert ist
	 */
	public ArrayList<Product> sortMan() 
	{
		//gefüllte Liste 
		ArrayList<Product> listold = new ArrayList<Product>(productList);
		System.out.println(listold.size());

		//leere Liste, in die später die Produkte in sotierter Reihenfolge kommen	
		ArrayList<Product> listnew = new ArrayList<Product>();

		//wird so lange durchgeführt, wie noch Produkte in der alten Liste vorhanden sind
		while(listold.size() > 0) // Use isEmpty() to check whether the collection is empty or not.
		{
			//der größte Wert, der in man passt
			long min = Long.MAX_VALUE;
			int index = -1;
			Product px = null;
			
			for(int i = 0; i < listold.size(); i++) 
			{
				Product p = listold.get(i);
				//ersten get.MAN ist von der Klasse Product zweites get.MAN ist von der Klasse MAN
				if(p.getMUN().getNumber() <= min) 
				{
					min=p.getMUN().getNumber();
					//Index vom Produkt mit der kleineren (kleinsten) MAN
					index = i;
					//Produkt mit der kleineren (kleinsten) MAN
					px = p;
				}

			}
			//Produkt mit kleinster MAN wird in die neue Liste eingefügt
			if(px != null)
			{
				listnew.add(px);
			}
			//Produkte, die in der neuen liste hinzugefügt wurden, werden aus der alten entfernt
			if(index >= 0) 
			{
				listold.remove(index);
			}
		}
		return listnew;
	}

	/**
	 * Methode sortiert Produkte nach Namen
	 * 
	 * @return vector mit Produktliste, deren Namen nach Alphabet sortiert sind
	 */
	public ArrayList<Product> productlistSortName() 
	{
		//gefüllte Liste 
		ArrayList<Product> listold = new ArrayList<Product>(productList);

		//leere Liste, in die später die Produkte in sotierter Reihenfolge kommen	
		ArrayList<Product> listnew = new ArrayList<Product>();
		

		//wird so lange durchgeführt, wie noch Produkte in der alten Liste vorhanden sind
		while(listold.size() > 0)
		{
			//Wort, das alphabetisch an letzer Stelle steht
			String name = "Zzzzzzzzzzzzzzzzzzzzzzzz";
			int index = -1;
			Product px = null;
			
			//komplette alte Liste durchgehen
			for(int i = 0; i < listold.size(); i++) 
			{
				//p ist das Produkt an der Listenstelle i
				Product p = listold.get(i);
				//vergleicht Namen lexikalisch miteinander
				if(p.getName().compareTo(name) <= 0) 
				{
					name=p.getName();
					//Index vom Produkt, das nach Alphabet weiter vorne ist
					index = i;
					//Produkt, das nach Alphabet weiter vorne ist 
					px = p;
				}

			}
			//Produkt mit kleinster MAN wird in die neue Liste eingefügt
			if(px != null)
			{
				listnew.add(px);
			}
			//Produkte, die in der neuen liste hinzugefügt wurden, werden aus der alten entfernt
			if(index >= 0) 
			{
				listold.remove(index);
			}
		}
		return listnew;
	}
	/**
	 * Methode um den Bestand eines Produktes zu ändern
	 * 
	 * @param Artikelname und neuer Bestand
	 */
	public boolean setStock(String name, int newStock)
	{
		for(Product product : this.productList) 
		{
			if(product!=null && name.equals(product.getName())) 
			{
				if (product.setStock(newStock))
				{
					return true;
				}
			}
		}
		return false;
	}	
	

	public Product searchProduct(String searchProduct) throws ProductDoesntExistException
	{
		Product foundProduct = null;
		for (Product product : this.productList)
		{
			if(product!=null && product.getName().equals(searchProduct))
			{
				System.out.println(product.toString());
				foundProduct = product;
			}
		}
		if (foundProduct != null)
		{
			return foundProduct;
		}
		else throw new ProductDoesntExistException(searchProduct);
	}


	/** Macht Objekt fuer alle Nutzer unsichtbar.
	 * 
     * @return boolean true bei Erfolg, sonst false.
	 */
    public boolean deactivateProduct(Product product)
    {
		Product toDelet = product;
		boolean success = false;

		if(toDelet.getDeleted() != true)
		{
			toDelet.setDeleted(true);
			toDelet.setStock(0);
			success = true;
		} 
		return success;
	}
	

	/** Macht Objekt fuer alle Nutzer wieder sichtbar.
	 * 
	 * @return boolean true bei Erfolg, sonst false.
	 */
	public boolean activateProduct(Product product)
	{
		Product toActivate = product;
		boolean success = false;

		if(toActivate.getDeleted() != false)
		{
			toActivate.setDeleted(false);
			success = true;
		} 
		return success;
	}


	//ursprüngliche ArrayList als String ausgeben
	public String toString() 
	{
		String s = "";
		for (Product p : this.productList)
		{
			s += p.getMUN().getNumber() + "\n " + p.getName() + "\n" + p.getPrice() + " € \n" + p.getStock() + "\r\n";
		}
		return s;
	}
	
	//neu erstellte ArrayList, der die sortierten Daten bekommt als String ausgeben
	public String arrayToString(ArrayList<Product> arrayList) 
	{
		String s = "";
		for (Product p : arrayList)
		{
			s += p.getMUN().getNumber() + "\n " + p.getName() + "\n " + p.getPrice() + " \n € " + p.getStock() + "\r\n";
		}
		return s;
	}




}