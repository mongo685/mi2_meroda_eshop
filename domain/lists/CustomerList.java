package domain.lists;

import java.util.ArrayList;
import java.util.Iterator;

import domain.exceptions.CustomerAlreadyExists;
import valueObjects.Customer;

//! Dateinamen-Endung: .clt

/** Klasse in der die Customer-Objekte untergebracht werden und mit der Verwaltet wird.
 *  Die Customer Liste sortiert sich nach jedem neu hinzugefügten Customer & nach jedem 
 *  entfernten Customer neu.
 */
public class CustomerList
{
    private ArrayList <Customer> customerList;

    public CustomerList()
    {
        this.customerList = new ArrayList<Customer>();
        // Ein immer angelegter default customer, wie der Root <curoot, curoot>
        //this.customerList.add(new Customer());
    }



    /**  Liest das .clt Dokument aus.
     * 
     *  @param fileName
     *  @return true bei erfolgreichen auslesen, sonst false
     */
    public boolean readData(String fileName)
    {
        boolean success = false;
        return success;
    }


    /**  Schreibt gesammte CustomerListe in .ctl Dokument,
     *   wenn das Dokument nicht vorhanden ist, so wird es erstellt.
     * 
     *  @param 
     *  @return true bei erfolgreichen 
     */
    public boolean writeData(ArrayList<Customer> customerList)
    {
        boolean success = false;
        return success;
    }



    /**
     * Fuegt neues Customer-Objekt in die Liste hinzu. Die Methode prueft erst ob es
     * dieses Objekt bereits gibt, Verglich durch Attribute Username, wemn Username
     * bereits in der Liste belegt, dann erfolgt kein hinzufuegen
     * 
     * @param customer , der hinzugefuegt werden soll.
     * @return true bei Erfolg, sonst false
     * @throws CustomerAlreadyExists
     */
    public boolean addCustomer(Customer givenCustomer) throws CustomerAlreadyExists
    {

       // Pruefe ob der gesuchte Employer bereits in der Liste vorkommt.
       boolean success = checkAlreadyExist(givenCustomer);

       if(success)
       {
           throw new CustomerAlreadyExists(givenCustomer);
       }

       if(this.customerList.add(givenCustomer))
       {
           success = true;
       } else {
           success = false;
       }

       return success;
    }


    /**
     * Vergleicht die EmployerListe ob der gesuchte Employer bereits eingetragen ist.
     * 
     * @return true wenn bereits vorhanden, sonst false
     */
    public boolean checkAlreadyExist(Customer toCheck)
    {
        for(Customer customer : this.customerList)
        {
            if(customer!=null && customer.getUsername().equals(toCheck.getUsername()))
            {
                return true;
            }
        }
        return false;
    }


    /**
     * Setze deleted Flage, des gesuchten Customers, auf true
     */
    public boolean removeCustomer(String username)
    {
        boolean success = false;
        for(Customer customer : customerList)
        {
            if(customer.getUsername().equals(username))
            {
                customer.setDeleted(true);
                success = true;
            }
        }
        return success;
    }


    /** Sucht einen Customer in der Liste
     * 
     * @param firstname
     * @param lastname
     * @param customernumber
     * @return found customer, sonst false
     */
    public Customer searchCustomer(String firstname, String lastname, long customernumber)
    {
        for(Customer customer : this.customerList)
        {
            if(customer!=null && firstname.equals(customer.getFirstname()) && lastname.equals(customer.getLastname()) && customer.getNumber() == customernumber)
            {
                return customer;
            }
        }
        return null;
    }
   
    public ArrayList<Customer> searchCustomerByUsername(String searchUsername)
	{
		ArrayList<Customer> foundCustomer = new ArrayList<Customer>();
		
		for (Customer customer : this.customerList)
		{
			if(customer!=null && customer.getUsername().equals(searchUsername))
			{
                foundCustomer.add(customer);
			}
        }
        
        if (foundCustomer != null)
        {
            return foundCustomer;
        }else{
            return null;
        } 
	}

    /**
     * Leert die komplette Liste
     */
    public void emptyCustomerList()
    {
        this.customerList.clear();
    }



    public ArrayList<Customer> getCustomerList() 
    {
        return new ArrayList<Customer> (this.customerList);
    }

    
    
    public void setCustomerList(ArrayList<Customer> customerList) 
    {
        this.customerList = customerList;
    }

    //Mitarbeiter suchen fuer LogEntry
	public Customer searchCustomerByNumber(long number) 
	{
		Iterator<Customer> it = customerList.iterator();
		while (it.hasNext()) 
		{
			Customer customer = (Customer) it.next();
			if (customer.getMUN().getNumber() == number) 
			{
				return customer;
			}
		}
		return null;
	}


    @Override
    public String toString() 
    {
        return "{" +
            "customerList='" + getCustomerList() + "'" +
            "}";
    }

}