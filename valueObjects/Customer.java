package valueObjects;

import java.util.ArrayList;

import valueObjects.labeling.Address;
/**
 * Klasse für die Kunden
 * 
 * 
 * @author Robert
 */

public class Customer extends Person 
{
   private Address address;
   Basket customerbasket = new Basket();
   
   
   public Customer (String username, String password, String firstname, String lastname, String streetname, String town, String country, int plz, int streetnumber )
   {
       super(username, password, firstname, lastname);
       this.address = new Address(streetname, town, country, plz, streetnumber);
   } 
   
   //* Weil ich faul bin: Hier ein Dummy Customer
    public Customer()
    {
        super("warum", "???","Kevin", "Meissner");
        this.address = new Address();
    }



    public Address getAddress() {
        return this.address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Customer address(Address address) {
        this.address = address;
        return this;
    }

    public Basket getBasket()
    {
        return this.customerbasket;
    }

    public void setBasket(Basket newbasket)
    {
        this.customerbasket = newbasket;
    }


    @Override
    public String toString() {
        return 
            "firstname = " + getFirstname() + " | " +
            "lastname = " + getLastname() + " | " +
            "mun = " + getNumber() + "\n " +
            "address: " + getAddress().toString() + "\n";
    }


    public ArrayList<String> getListOfValues()
    {
        ArrayList<String> listOfValues = new ArrayList<String>();
        listOfValues.add(this.getUsername());
        listOfValues.add(this.getPassword());
        listOfValues.add(this.getFirstname());
        listOfValues.add(this.getLastname());
        listOfValues.add(this.getAddress().getStreetname());
        listOfValues.add("" + this.getAddress().getStreetnumber());
        listOfValues.add(this.getAddress().getTown());
        listOfValues.add("" + this.getAddress().getPlz());
        listOfValues.add(this.getAddress().getCountry());
        listOfValues.add(""+this.getMUN().getNumber());
        listOfValues.add(""+this.getDeleted());

        return listOfValues; 
    }

}
