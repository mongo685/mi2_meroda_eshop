package valueObjects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import domain.Eshop;
import domain.exceptions.*;

/**
 * Klasse für den Warenkorb.
 * 
 * 
 * @author majbrit
 */
public class Basket 
{
    private HashMap<Product, Integer> map; 

    /**
	 * Konstruktor
	 * 
	 */
    public Basket()
    {
        map = new HashMap<Product, Integer>();
    }
    
    /**
	 * Methode zum Einfuegen von Produkten in den Warenkorb. 
	 * 
	 * @param product Produkt, das eingefuegt werden soll
     * 
	 */
    //ein Artikel mit Menge wird zum Warenkorb hinzugefuegt
    public void put(Product product, int quantity) throws WrongAmountBulkException
    {
        int quBa = 0; //Menge im Warenkorb
        if(map.containsKey(product)) 
        {
            quBa = map.get(product);
        }

        //prueft ob es ein Massengutartikel ist
        if(product instanceof BulkProduct)
        {
            BulkProduct product1 = (BulkProduct) product;
            //wenn die neue Anzahl der Packungroesse oder dem Vielfachen enspricht, wird sie geändert
            if(quantity%product1.getPackSize() == 0)
            {
                //Menge im Warenkorb wird um die neue Menge erhoeht
                quBa += quantity; 
            }
            else
            {
                throw new WrongAmountBulkException(product1.getPackSize(),quantity);
                //!wrongAmountException
            }
        }
        else
        {
            //Menge im Warenkorb wird um die neue Menge erhoeht
            quBa += quantity; 
        }
        map.put(product, quBa);
    }




    /**
	 * Methode, um Menge eines Produktes herauszufinden
     * @return Menge des Produktes
	 */
    //um herauszufinden welche Menge für ein bestimmtes Produkt ausgewaehlt wurde
    public int getProductStock(Product product) 
    {
        int quBa = 0; //Menge im Warenkorb
        if(map.containsKey(product)) 
        {
            quBa = map.get(product);
        }
        return quBa;
    }


    /**
	 * Methode zum Loeschen eines Produktes aus dem Warenkorb. 
	 * 
	 * @param product Produkt, das geloescht werden soll
	 */
    //Produkt mit gesamter Menge aus dem Warenkorb nehmen
    public void remove(Product product) 
    {
        map.remove(product);
    }


    /**
	 * Methode zum Verringern der Anzahl eines Produkts im Warenkorb. 
	 * 
	 * @param product das zu verringernde Produkt
	 */
    //bei einem Produkt die Anzahl verringern
    public boolean reduce(Product product, int quantity) throws WrongAmountBulkException
    {
        int quBa=0;
        if(map.containsKey(product)) 
        {
            quBa = map.get(product);
        }

        //prueft ob es ein Massengutartikel ist
        if(product instanceof BulkProduct)
        {
            BulkProduct product1 = (BulkProduct) product;
            //wenn die neue Anzahl der Packungroesse oder dem Vielfachen enspricht, wird sie geändert
            if(quantity%product1.getPackSize() == 0)
            {
                //Menge im Warenkorb wird um die neue Menge verringert
                if(quBa>=quantity) 
                {
                    quBa -= quantity;
                    if(quBa!=0)
                    {
                        map.put(product, quBa);
                    }
                    // wenn die Menge 0 beträgt, wird das Produkt aus dem Warenkorb entfernt
                    else
                    {
                        map.remove(product);
                    }
                    return true;
                } else {
                    return false;
                }
            }
            else
            {
              throw new WrongAmountBulkException(product1.getPackSize(), quantity);
                //!wrongAmountException
                
            }
        }
        else
        {
            //Menge im Warenkorb wird um die neue Menge verringert
            if(quBa>=quantity) 
            {
                quBa -= quantity;
                if(quBa!=0)
                {
                    map.put(product, quBa);
                }
                // wenn die Menge 0 beträgt, wird das Produkt aus dem Warenkorb entfernt
                else
                {
                    map.remove(product);
                }
                return true;
            } else {
                return false;

            }
        }

    }




    /**
	 * Methode liest alle Produkte aus der Liste aus. 
	 * 
     * @return Liste mit allen Produkten
	 */
    //alle Produkte aus der Liste auslesen
    public ArrayList<ProductQuantity> getProdList()
    {
        ArrayList<ProductQuantity> list = new ArrayList<ProductQuantity>();
        //liest alle Werte aus der Hashmap aus 
        Iterator<Product> iter = map.keySet().iterator();
        while (iter.hasNext())
        {
            Product product = iter.next();
            ProductQuantity pq = new ProductQuantity(product, map.get(product));
            list.add(pq); 
        }
        return list;
    }



    /**
	 * Methode berechnet den Gesamtpreis. 
	 * 
     * @return Gesamtpreis
	 */
    public double getTotalprice()
    {
        double totalprice = 0;


        //liest alle Werte aus der Hashmap aus 
        Iterator<Product> iter = map.keySet().iterator();
        while (iter.hasNext())
        {
            Product product = iter.next();
            //der Preis des Produkts wird mit der Anzahl im Warenkorb multipliziert
            double price; 
            //Alle Preise müssen als Stueckpreis angegeben werden
            price = product.getPrice() * map.get(product);      
            //Preis wird zum Gesamtpreis dazu addiert
            totalprice += price;
        }
        return totalprice;
    }


	public ArrayList<ProductQuantity> sortName() 
	{
        return sortName(getProdList());
    }


    /**
	 * Methode sortiert Produkte nach Namen
	 * 
	 * @return vector mit Produktliste, deren Namen nach Alphabet sortiert sind
	 */
	private ArrayList<ProductQuantity> sortName(ArrayList<ProductQuantity> list) 
	{
		//gefüllte Liste 
			//Wort, das alphabetisch an letzer Stelle steht
		ArrayList<ProductQuantity> listold = new ArrayList<ProductQuantity>(list);

		//leere Liste, in die später die Produkte in sotierter Reihenfolge kommen	
		ArrayList<ProductQuantity> listnew = new ArrayList<ProductQuantity>();
		

		//wird so lange durchgeführt, wie noch Produkte in der alten Liste vorhanden sind
		while(listold.size() > 0)
		{
			//Wort, das alphabetisch an letzer Stelle steht
			String name = "Zzzzzzzzzzzzzzzzzzzzzzzz";
			int index = -1;
			ProductQuantity px = null;
			
			//komplette alte Liste durchgehen
			for(int i = 0; i < listold.size(); i++) 
			{
				//p ist das Produkt an der Listenstelle i
				ProductQuantity p = listold.get(i);
				//vergleicht Namen lexikalisch miteinander
				if(p.getProduct().getName().compareTo(name) <= 0) 
				{
					name=p.getProduct().getName();
					//Index vom Produkt, das nach Alphabet weiter vorne ist
					index = i;
					//Produkt, das nach Alphabet weiter vorne ist 
					px = p;
				}

			}
			//Produkt mit kleinster MAN wird in die neue Liste eingefügt
			if(px != null)
			{
				listnew.add(px);
			}
			//Produkte, die in der neuen liste hinzugefügt wurden, werden aus der alten entfernt
			if(index >= 0) 
			{
				listold.remove(index);
			}
		}
		return listnew;
	}

    
    //alle Produkte aus dem Warenkorb nehmen
    public void clear() 
    {
        map.clear();
    }


    public HashMap<Product, Integer> getHashMap()
    {
        return this.map;
    }


    //Liste in sortierter Reihenfolge als String ausgeben
    public String toString() 
	{
        ArrayList<ProductQuantity> vector = sortName();
		String s = "";
		for (ProductQuantity p : vector)
		{
            //Ausgabe fuer Massengutartikel
            if(p.getProduct() instanceof BulkProduct)
            {
                BulkProduct bp = (BulkProduct) p.getProduct();
                s += "MUN: "+p.getProduct().getMUN().getNumber() +" | " + "Massengutartikel: "+p.getProduct().getName() 
                + " | " +"Preis pro Packung: "+ Eshop.convertPrice(p.getProduct().getPrice()) + " | " +"Stückzahl: "+ p.getQuantity() 
                + " | "+ "Packungsgröße: " +bp.getPackSize() + "\r\n";
            }
            else
            //Ausgabe fuer Einzelartikel
            {
                s += "MUN: "+p.getProduct().getMUN().getNumber() +" | " + "Einzelartikel: "+p.getProduct().getName() 
                + " | " +"Einzelpreis: "+ Eshop.convertPrice(p.getProduct().getPrice()) + " | " +"Stückzahl: "+ p.getQuantity() + "\r\n";
            }
        }
		return s;
	}
    
    

}