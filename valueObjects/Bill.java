package valueObjects;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import domain.Eshop;

/**
 * Eine Rechnung die bei jedem Einkauf (Basket wird gekauft) erstellt wird und
 * alle notwendigen Informationen beinhaltet, welche benötigt werden um einen
 * Einkauf gueltig zu machen.
 * 
 * @author David Melamed
 */ 
public class Bill
{
    private final Customer customer;
    private final LocalDateTime datetime;
    private final DateTimeFormatter dayMonthYearAtTime = DateTimeFormatter.ofPattern("dd-MM-yyyy' AT 'HH:mm:ss");

    
    
    public Bill(final Customer customer)
    {
        this.customer = customer;
        this.datetime = LocalDateTime.now();
    }


    public void printBasketContent()
    {
        Iterator<Product> iter = this.customer.getBasket().getHashMap().keySet().iterator();
        while(iter.hasNext())
        {
            Product productInBasket = iter.next();
            System.out.println(productInBasket.toStringBill(this.customer.getBasket().getHashMap().get(productInBasket)));
        }
    }


    /** Gibt die Bill, mit allen Werten, in der CUI aus.
     * 
     * @author David Melamed
     */
    public void printCuiBill()
    {
        System.out.println("\n");
        System.out.println( "\n" +
                            "Kaufdatum: " + this.datetime.format(dayMonthYearAtTime) + "\n" +
                            "Kunde: " + this.customer.getFirstname() + " " + this.customer.getLastname() + "\n" +
                            // Adresse steht in einer Zeile, Werte durch "|" getrennt:
                            "Adresse: " + this.customer.getAddress().getCountry() + " | " +  
                            this.customer.getAddress().getTown() + " | " +
                            this.customer.getAddress().getPlz() + " | " + 
                            this.customer.getAddress().getStreetname() + " | " +
                            this.customer.getAddress().getStreetnumber() + "\n" +
                            "Warenkorb: \n");
                            printBasketContent();
        System.out.println("\n");   
        System.out.println("Gesammtpreis: " + Eshop.convertPrice(this.customer.getBasket().getTotalprice()));
    }

     
}