package valueObjects;

//hi
/**
 * Klasse zur Repräsentation einzelner Mitarbeiter.
 * 
 * @author majbrit
 */
public class Employer extends Person 
{

    /** 
     * ROOT-Employer 
     */
    public Employer()
    {
        super("emroot","emroot","emroot", "emroot");
    }
    

    public Employer(String username, String password, String firstname, String lastname) 
    {
        super(username, password, firstname, lastname);
    }

}