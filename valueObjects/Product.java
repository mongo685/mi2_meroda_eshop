package valueObjects;

import valueObjects.labeling.MUN;

public class Product
{
    private String name;
    private int stock;
    private double price;
    private MUN mun;
    private int reserved = 0; //reservierte Menge (liegt in einem Warenkorb)
    private boolean deleted;


    //für BulkProduct
    public Product(String name, double price)
    {
        this.name = name;
        this.stock = 0;
        this.price = price;
        this.mun = new MUN();
        this.deleted = false;
    }

    // default constructor
    public Product()
    {
        this.name = "";
        this.stock = 0;
        this.price = 0.0;
        this.mun = new MUN();
        this.deleted = false;
    }


    public String getName()
    {
        return this.name;
    }

    
    public void setName(String newName)
    {
        this.name = newName;
    }


    public int getStock() 
    {
        return this.stock;
    }


    public boolean setStock(int newStock)
    {
        if(newStock>=reserved)
        {
            this.stock = newStock;
            //Produkt ist geloescht, wenn der Bestand auf null ist
            if(this.stock==0)
            {
                deleted=true;
            }
            else
            {
                deleted=false;
            }
            return true;
        }
        return false;
    }


    public void addStock(int st)
    {
        this.stock += st;
        //Produkt ist geloescht, wenn der Bestand auf null ist
        if(this.stock==0)
        {
            deleted=true;
        }
        else
        {
            deleted=false;
        }
    }


    public void reduceStock(int st)
    {
        this.stock -= st;
        //Produkt ist geloescht, wenn der Bestand auf null ist
        if(this.stock==0)
        {
            deleted=true;
        }
        else
        {
            deleted=false;
        }
    }


    public MUN getMUN()
    {
        return this.mun;
    }


    public double getPrice()
    {
        return this.price;
    }

    public void setPrice(double newPrice)
    {
        this.price = newPrice;
    }


    public boolean getDeleted()
    {
        return this.deleted;
    }

    public void setDeleted(boolean newDeleted) 
    {
        this.deleted = newDeleted;
    }
    

    /** Methode um zwei Product-Objekte miteinander vergleichen zu koennen.
     *  Als Vergleichparameter dirnt die einmalige MUN-Nummer
     * 
     * @return true bei Erfolg, sonnst false
     */
    @Override
    public boolean equals(Object obj) 
    {
        boolean success = false;
        if(obj instanceof Product)
        {

            Product tocompare = (Product) obj; 
            if(tocompare instanceof Product) 
            {
                return this.mun.getNumber() == tocompare.mun.getNumber() &&
                    this.name.equals(tocompare.name) 
                        ? success = true : false;
            }
            return success;
        } else {
            return false;
        }
    }


    public int hashCode()
    {
        return mun.hashCode();
    }


    /**
     * @return the reserved
     */
    public int getReserved() 
    {
        return reserved;
    }

    public void setReserved(int res)
    {
        reserved = res;
    }


    // nur wenn die neue reservierte Menge kleiner/gleich der Menge im Lager ist
    // kann die reservierte Menge erhöht werden
    public boolean addReserved(int res) 
    {
        if(reserved + res <= stock)
        {
            this.reserved += res;
            return true;
        }
        return false;
    }



    public void reduceReserved(int res)
    {
        this.reserved -= res;
    }

    public String toStringBill(int basketstock)
    {
        return 
                "Artikelname " + this.getName() + " | " +
                "Vorrat " + basketstock + " | " +
                "Einzelpreis " + this.getPrice() + " | " +
                "Artikelnummer " + this.mun.getNumber();
    }

    
    @Override
    public String toString() 
    {
        return 
                "Artikelname " + this.getName() + " | " +
                "Vorrat " + this.getStock() + " | " +
                "Einzelpreis " + this.getPrice() + " | " +
                "Artikelnummer " + this.mun.getNumber() + "\n";
    }

 


}