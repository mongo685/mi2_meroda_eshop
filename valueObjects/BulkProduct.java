package valueObjects;

import domain.exceptions.WrongAmountBulkException;

/**
 * Klasse für Massengutartikel.
 * 
 * @author Majbrit
 * @version 1
 */
public class BulkProduct extends Product 
{
    private int packSize;

    public BulkProduct(String name, double price, int packSize)
    {
        super(name, price);
        this.packSize = packSize;
        
    }
    
    // default constructor
    public BulkProduct()
    {
        super();
        this.packSize = 0;
    }


    /**
	 * Methode legt die Anzahl des Produkts im Lager fest.
	 * 
	 */
    // throws WrongAmountBulkexc. 
     public boolean setStock(int newStock)
    {
        //wenn die neue Anzahl der Packungroesse oder dem Vielfachen enspricht, wird sie geändert
        if(packSize != 0)
        {
            if(newStock%packSize == 0) 
            {
                super.setStock(newStock); 
                return true;
            } 
        }
        
        return false;
        
        
    }

    public boolean checkQuantity(int q) throws WrongAmountBulkException
    {
        if(packSize != 0)
        {
            if(q%packSize == 0) 
            {
                return true;
            }
         throw new WrongAmountBulkException(packSize,q) ;
        }
        
        return false;
        
    }

    /**
	 * Methode legt die Packungsgroesse fest.
	 * 
	 */
    public void setPackSize(int packSize)
    {
        this.packSize = packSize;
    }

    /**
	 * Methode gibt die Packungsgroesse zurueck.
	 * 
	 */
    public int getPackSize()
    {
        return packSize;
    }

    @Override
    public String toString() {
        return 
                "Massengutartikel " + this.getName() + " | " +
                "Packungsgröße " + this.getPackSize() + " | " +
                "Vorrat " + this.getStock() + " | " +
                "Preis pro Packung " + this.getPrice() + " | " +
                "MUN " + this.getMUN().getNumber();
    }

}