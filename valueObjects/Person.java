package valueObjects;

import java.util.ArrayList;

import valueObjects.labeling.MUN;


public abstract class Person
{
    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private MUN mun;
    private static int counter; // zaehlt die Personen-Objekte im System
    private boolean deleted;



    public Person(String username, String password, String firstname, String lastname)
    {
        ++counter;
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.mun = new MUN();
        this.deleted = false;
    }
    


	public String getFirstname()
    {
        return this.firstname;
    }

    public void setFirstname(String newFirstname)
    {
        this.firstname = newFirstname;
    }


    public String getLastname()
    {
        return this.lastname;
    }

    public void setLastname(String newLastname)
    {
        this.lastname = newLastname;
    }


    public long getNumber()
    {
        return this.mun.getNumber();
    }

    public void setNumber(MUN newNumber)
    {
        this.mun = newNumber;
    }


    public MUN getMUN()
    {
        return this.mun;
    }


    public int getCounter()
    {
        return counter;
    }

    //? Ist es ueberhazpt noetig diese Methode zu haben?
    public void setCounter(int newCounter)
    {
        //! -> Wenn diese Methode unbedacht verwendet wird geht das System des kapput.-
    }


    public String getUsername()
    {
         return username;
    }
    
    public void setUsername(String newUsername)
    {
        this.username = newUsername;
    }


    public String getPassword()
    {
        return password;
    }

    public void setPassword(String newPassword)
    {
        this.password = newPassword;
    }


    public boolean getDeleted()
    {
        return this.deleted;
    }

    public void setDeleted(boolean deleted) 
    {
        this.deleted = deleted;
    }


    public ArrayList<String> getListOfValues()
    {
        ArrayList<String> listOfValues = new ArrayList<String>();
        listOfValues.add(this.getUsername());
        listOfValues.add(this.getPassword());
        listOfValues.add(this.getFirstname());
        listOfValues.add(this.getLastname());
        listOfValues.add(""+this.getMUN().getNumber());
        listOfValues.add(""+this.getDeleted());
        return listOfValues; 
    }

   /** Methode um zwei Person-Objekte miteinander vergleichen zu koennen.
     *  Als Vergleichparameter dirnt die einmalige MUN-Nummer
     * 
     * @return true bei Erfolg, sonnst false
     */
    @Override
    public boolean equals(Object obj) 
    {
        boolean success = false;
        if(obj instanceof Person)
        {

            Person toCompare = (Person) obj; 
            if(toCompare instanceof Person) 
            {
                return this.mun.getNumber() == toCompare.mun.getNumber() &&
                    this.firstname.equals(toCompare.firstname) 
                        ? success = true : false;
            }
            return success;
        } else {
            return false;
        }
    }


    @Override
    public String toString()
    {
        return "firstname = " + getFirstname() + " | " +
        " lastname = " + getLastname() + " | " +
        " mun = " + getNumber() + " | " +
        " username = " + getUsername() + " | " +
        " password = " + getPassword() + "\n";
    }


}