package valueObjects.labeling;

/**
 * Klasse der Adressen der Kunden
 * 
 * 
 * @author Robert
 */
public class Address 
{
    private String streetname;
    private String town;
    private String country;
    private int plz;
    private int streetnumber;


    public Address (String streetname,String town,String country,int plz, int streetnumber)
    {
        this.streetname = streetname;
        this.town = town;
        this.country = country;
        this.plz = plz;
        this.streetnumber = streetnumber;
    }

    /** Eine default Adresse */
    public Address ()
    {
        this.streetname = "Einhardstraße";
        this.town = "Fulda";
        this.country = "Deutschland";
        this.plz = 36039;
        this.streetnumber = 39;
    }


    public String getStreetname()
    {
        return this.streetname;
    }

    public String getTown()
    {
        return this.town;
    }

    public String getCountry() 
    {
        return this.country;
    }    

    public int getPlz()
    {
        return this.plz;
    }

    public int getStreetnumber()
    {
        return this.streetnumber;
    }

    public void setStreetname(String newStreetname)
    {
        this.streetname = newStreetname;
    }

    public void setTown(String newTown)
    {
        this.town = newTown;
    }

    public void setCountry(String newCountry)
    {
        this.country = newCountry;
    }

    public void setPlz(int newPlz)
    {
        this.plz = newPlz;
    }

    public void setStreetnumber(int newStreetnumber)
    {
        this.streetnumber = newStreetnumber;
    }

    
    
    public String toSave() 
    {
        return   getStreetname() + ";" 
               + getTown() + ";" 
               + getCountry() + ";" 
               + getPlz() + ";" 
               + getStreetnumber();
    }

    @Override
    public String toString() 
    {
        return "\n" +
            " streetname = " + getStreetname() + "\n" +
            " town = " + getTown() + "\n" +
            " country = " + getCountry() + "\n" +
            " plz = " + getPlz() + "\n" +
            " streetnumber = " + getStreetnumber();
    }



}


