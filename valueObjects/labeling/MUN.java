package valueObjects.labeling; // Variabler Datenpfad 

/**
 * My Universal Number (MUN) ist eine, an die European Article Number (EAN), angelehnte Nummer
 * welche einen Zahlenwert, der nur einmal im gesammten System vorhanden ist, zugeweist.
 */
public class MUN
{
    private long number = 0L;
    private static double counter = 0;

    /** Konstruktor der einem Objekt im System eine MUN zuweist
     * 
     */
    public MUN()
    {
        ++counter;
        this.number += counter;
        System.out.println(counter);
    }


    public long getNumber()
    {
        return this.number;
    }

    public void setNumber(long newNumber)
    {
        this.number = newNumber;
    }

    //! Soll entfernt werden , Projekt prüfen
    //! Gefaerliche Methode kann das unique System zerstoeren!
    public static void setCounter(double counter)
    {
        MUN.counter = counter;
    }

    public String toString()
    {
        return "MUN " + this.number;
    }
}