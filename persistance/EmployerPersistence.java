package persistance;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import domain.exceptions.EmployerAlreadyExists;
import domain.lists.CustomerList;
import domain.lists.EmployerList;
import domain.lists.LogList;
import domain.lists.ProductList;
import domain.logging.LogEntry;
import domain.management.CustomerManagement;
import domain.management.EmployerManagement;
import domain.management.ProductManagement;
import valueObjects.Customer;
import valueObjects.Employer;
import valueObjects.Product;
import valueObjects.labeling.MUN;

public class EmployerPersistence implements PersistenceManager {
    private FileWriter fw;
    private BufferedWriter bw;
    private PrintWriter pw;
    private FileReader fr;
    private BufferedReader br;
    public static final String FILENAME = "employer.elt";
    private String seperator = ";";

    /**
     * Oeffnet Stream um lesen zu koennen.
     * 
     * @param fileNameSting , Name der zu lesenden Datei als String.
     */
    @Override
    public void openForReading(String fileNameString) throws IOException {
        fr = new FileReader(fileNameString);
        br = new BufferedReader(fr);
    }

    /**
     * Oeffnet Stream um Dateien beschreiben zu koennen.
     * 
     * @param fileNameString , Name der zu beschreibenden Datei als String.
     */
    @Override
    public void openForWriting(String fileNameString) throws IOException {
        openForWriting(fileNameString, true);
    }

    // Zusammen gefasst Methode um sie einfach verwenden zu koennen.
    public void openForWriting(String fileNameString, boolean append) throws IOException {
        fw = new FileWriter(fileNameString, append);
        bw = new BufferedWriter(fw);
        pw = new PrintWriter(bw);
    }

    /**
     * Schliesst den gesammten Stream
     * 
     * @return true bei Erfolg, sonst false
     */
    @Override
    public boolean close() {
        if (fw != null) {
            try {
                fw.close();
            } catch (IOException exception) {
                exception.printStackTrace();
                return false;
            }
        } else if (fr != null) {
            try {
                fr.close();
            } catch (IOException exception) {
                exception.printStackTrace();
                return false;
            }
        }
        return true;
    }

    /**
     * Einlesen der Employer-Objekte aus einer externen Datenquelle.
     * 
     * @return Employer-Objekt, wenn Einleseprozess erfolgreich, sonst null.
     * @throws IOException
     */
    @Override
    public Employer loadEmployer() throws IOException {
        // Wenn Employernummer (größer)> aktueller MUN counter dann MUN counter =
        // Employernummer //! auch in Kunde und Productpers. hinzufügen
        String objectMembers = br.readLine();
        // erzeugt Luecken in den zahlen
        // MUN mun = new MUN();

        if (objectMembers != null && !objectMembers.trim().equals("")) {
            String stringSplit[] = objectMembers.split(seperator);
            Employer loadedEmployer = new Employer();

            // wird nicht gebraucht, da nichts geloescht wird und so der Counter
            // sowieso bei der richtigen Zahl anfaengt
            // bringt zahlen durcheinander, weil nicht auf Counter von Produkt und
            // Kunde geachtet wird
            /*
             * // Checke ob MUN größer ist, als die des ersten eingelesenen Mitarbeiters
             * if(mun.getNumber() > Long.parseLong(stringSplit[0])) { // Wenn ja, dann setze
             * MUN counter auf die es eingelesenen Mitarbeiters
             * MUN.setCounter(Long.parseLong(stringSplit[0])); }
             */

            for (int i = 0; i < stringSplit.length; i++) {
                if (i == 0) {
                    loadedEmployer.getMUN().setNumber(Long.parseLong(stringSplit[i]));
                } else if (i == 1) {
                    loadedEmployer.setUsername(stringSplit[i]);
                } else if (i == 2) {
                    loadedEmployer.setPassword(stringSplit[i]);
                } else if (i == 3) {
                    loadedEmployer.setFirstname(stringSplit[i]);
                } else if (i == 4) {
                    loadedEmployer.setLastname(stringSplit[i]);
                } else if (i == 5) {
                    loadedEmployer.setDeleted(Boolean.parseBoolean(stringSplit[i]));
                }
            }
            return loadedEmployer;
        }
        return null;
    }

    /**
     * Alle Employer aus einer externen Datenquelle auslesen. Das Datenformat ist
     * als .clt (EmployerListText)
     * 
     * @return Employerliste, wenn Einlesen erfolgreich, sonst null
     * @throws IOException
     */
    public EmployerList loadAllEmployer() throws IOException 
    {
        EmployerList cl = new EmployerList();
        File f = new File(FILENAME);
        
        if (!f.exists()) 
        {
            f.createNewFile();
        }

        System.out.println(f.getAbsolutePath());
        openForReading(FILENAME);
        boolean next = true;

        while (next) 
        {
            Employer c = loadEmployer();
            if (c != null) 
            {
                try {
                    cl.addEmployer(c);
                } catch (EmployerAlreadyExists e) {
                    e.getMessage();
                }
            } else {
                next = false;
            }
        }

        fr.close();
        return cl;
    }

    
    /**
     * Speichert jedes Employer-Objekt in eine externe Datenquelle.
     * 
     * @param Employer Employer-Objekt, das gespeichert werden soll
     * @return true, wenn Schreibvorgang erfolgreich, sonst false
     * @throws IOException
     */
    @Override
    public boolean saveEmployer(Employer employer) throws IOException 
    {
        String employerAsString = "" + employer.getNumber();
        employerAsString += seperator + employer.getUsername();
        employerAsString += seperator + employer.getPassword();
        employerAsString += seperator + employer.getFirstname();
        employerAsString += seperator + employer.getLastname();
        employerAsString += seperator + employer.getDeleted();

        pw.println(employerAsString);
        return false;
    }

    /**
     * Methode zum Schreiben aller Employerdaten in eine externe Datenquelle.
     * 
     * @param employerlist Employer Liste, die gespeichert werden soll
     * @return true, wenn Schreibvorgang erfolgreich, false sonst
     * @throws IOException
     */
    public boolean saveAllEmployer(EmployerList pl) throws IOException 
    {
        openForWriting(FILENAME, false);
        for (Employer p : pl.getEmployerList()) 
        {
            saveEmployer(p);
        }
        bw.flush();
        close();
        return false;
    }

    // ! --- Methoden die implementiert sein muessen ---

    @Override
    public Product loadProduct() throws IOException 
    {
        return null;
    }

    @Override
    public boolean saveProduct(Product product) throws IOException 
    {
        return false;
    }

    @Override
    public Customer loadCustomer() throws IOException 
    {
        return null;
    }

    @Override
    public boolean saveCustomer(Customer customer) throws IOException 
    {
        return false;
    }

    @Override
    public LogList loadAllLogEntrys(CustomerManagement cm, EmployerManagement em, ProductManagement pm) throws IOException 
    {
        return null;
    }

    @Override
    public boolean saveLogEntry(LogEntry logEntry) throws IOException 
    {
        return false;
    }

    @Override
    public boolean saveAllProducts(ProductList productlist) throws IOException 
    {
        return false;
    }

    @Override
    public boolean saveAllCustomer(CustomerList customerlist) throws IOException 
    {
        return false;
    }

}