package persistance;

import java.io.IOException;

import domain.lists.CustomerList;
import domain.lists.EmployerList;
import domain.lists.LogList;
import domain.lists.ProductList;
import domain.logging.LogEntry;
import domain.management.CustomerManagement;
import domain.management.EmployerManagement;
import domain.management.ProductManagement;
import valueObjects.Customer;
import valueObjects.Employer;
import valueObjects.Product;


/**
 *         Allgemeine Schnittstelle für den Zugriff auf ein Speichermedium (z.B.
 *         Datei oder Datenbank) zum Ablegen von Daten.
 * 
 *         Das Interface muss von Klassen implementiert werden, die eine
 *         Persistenz-Schnittstelle realisieren wollen.
 */
public interface PersistenceManager {


	/** Oeffnet Stream um Dateien auslesen zu koennen.
	 * 
	 * @param fileName, Name der zu lesenden Datei
	 */
	public void openForReading(String fileName) throws IOException;


	/** Oeffnet Stream um Dateien beschreiben zu koennen.
	 * 
	 * @param fileName, Name der zu beschreibenden Datei
	 */
	public void openForWriting(String datenquelle) throws IOException;


	/** Schliesst den Stream
	 * 
	 * @return true bei Erfolg, sonst false
	 */
	public boolean close();


	/**
	 * Methode zum Einlesen der Produkt-Objekte aus einer externen Datenquelle.
	 * 
	 * @return Produkt-Objekt, wenn Einlesen erfolgreich, sonst null
	 * @throws IOException
	 */
	public Product loadProduct() throws IOException;


	/**
	 * Methode zum Schreiben der Produktdaten in eine externe Datenquelle.
	 * 
	 * @param product ProduKt-Objekt, das gespeichert werden soll
	 * @return true, wenn Schreibvorgang erfolgreich, false sonst
	 * @throws IOException
	 */
	public boolean saveProduct(Product product) throws IOException;


	/**
	 * Speichert alle Objekte in der Produkt Liste
	 * 
	 * @param productlist
	 * @return true bei Erfolg
	 * @throws IOException
	 */
	public boolean saveAllProducts(ProductList productlist) throws IOException;


	/** Einlesen der Kunden-Objekte aus einer externen Datenquelle.
	 * 
	 * @return Kunden-Objekt, wenn Einlesen erfolgreich, sonst null
	 * @throws IOException
	 */
	public Customer loadCustomer() throws IOException;


	/**
	 * Methode zum Schreiben der Kundendaten in eine externe Datenquelle.
	 * 
	 * @param customer Kunden-Objekte, das gespeichert werden soll
	 * @return true, wenn Schreibvorgang erfolgreich, sonst false
	 * @throws IOException
	 */
	public boolean saveCustomer(Customer customer) throws IOException;


	/**
	 * Speichert alle Objekte in der Customer Liste
	 * 
	 * @param customerlist
	 * @return true bei Erfolg
	 * @throws IOException
	 */
	public boolean saveAllCustomer(CustomerList customerlist) throws IOException;


	/** Einlesen der Mitarbeiter-Objekte aus einer externen Datenquelle.
	 * 
	 * @return Mitarbeiter-Objekte, wenn Einlesen erfolgreich, sonst null
	 * @throws IOException
	 */
	public Employer loadEmployer() throws IOException;


	/**
	 * Methode zum Schreiben der Mitarbeiterdaten in eine externe Datenquelle.
	 * 
	 * @param employer Mitarbeiter-Objekte, das gespeichert werden soll
	 * @return true, wenn Schreibvorgang erfolgreich, sonst false
	 * @throws IOException
	 */
	public boolean saveEmployer(Employer employer) throws IOException;


	/**
	 * Speichert alle Objekte in der Employer Liste
	 * 
	 * @param employerlist
	 * @return true bei Erfolg
	 * @throws IOException
	 */
	public boolean saveAllEmployer(EmployerList employerlist) throws IOException;


	/** Einlesen der LogEntry-Objekte aus einer externen Datenquelle.
	 * 
	 * @return LogEntry-Objekte, wenn Einlesen erfolgreich, sonst null
	 * @throws IOException
	 */
	public LogList loadAllLogEntrys(CustomerManagement cm, EmployerManagement em, ProductManagement pm) throws IOException;


	/**
	 * Methode zum Schreiben der LogEntrydaten in eine externe Datenquelle.
	 * 
	 * @param logEntry LogEntry-Objekte, das gespeichert werden soll
	 * @return true, wenn Schreibvorgang erfolgreich, sonst false
	 * @throws IOException
	 */
	public boolean saveLogEntry(LogEntry logEntry) throws IOException;

}