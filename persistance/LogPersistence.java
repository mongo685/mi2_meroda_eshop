package persistance;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import domain.lists.CustomerList;
import domain.lists.EmployerList;
import domain.lists.LogList;
import domain.lists.ProductList;
import domain.logging.LogEntry;
import domain.management.CustomerManagement;
import domain.management.EmployerManagement;
import domain.management.LogManagement;
import domain.management.ProductManagement;
import valueObjects.Customer;
import valueObjects.Employer;
import valueObjects.Product;

public class LogPersistence implements PersistenceManager {

    private FileWriter fw;
    private BufferedWriter bw;
    private PrintWriter pw;
    private FileReader fr;
    private BufferedReader br;
    public final static String FILENAME = "log.llt";
    private String trenner = ";";

    /**
     * Oeffnet Stream um Dateien auslesen zu koennen.
     * 
     * @param fileName, Name der zu lesenden Datei
     */
    @Override
    public void openForReading(String fileName) throws IOException {
        fr = new FileReader(fileName);
        br = new BufferedReader(fr);
    }

    /**
     * Oeffnet Stream um Dateien beschreiben zu koennen.
     * 
     * @param fileName, Name der zu beschreibenden Datei
     */
    @Override
    public void openForWriting(String datenquelle) throws IOException {
        openForWriting(datenquelle, true);
    }

    public void openForWriting(String datenquelle, boolean append) throws IOException {
        fw = new FileWriter(datenquelle, append);
        bw = new BufferedWriter(fw);
        pw = new PrintWriter(bw);
    }

    /**
     * Schliesst den Stream
     * 
     * @return true bei Erfolg, sonst false
     */
    @Override
    public boolean close() 
    {
        if (fw != null) 
        {
            try 
            {
                fw.close();
            } catch (IOException e) 
            {
                e.printStackTrace();
                return false;
            }
        } else if (fr != null) 
        {
            try 
            {
                fr.close();
            } catch (IOException e) 
            {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    /**
     * Die Methode lädt die Daten, die persistent gespeichert wurden.
     * 
     * @return wieder hergestellter Logeintrag
     */
    private LogEntry loadLogEntry() throws IOException 
    {
        String s = br.readLine();

        if (s != null && !s.trim().equals("")) 
        {
            String sx[] = s.split(trenner);
            LogEntry logEntry = new LogEntry();

            for (int i = 0; i < sx.length; i++) 
            {
                if (i == 0) 
                {
                    logEntry.setQuantity(Integer.parseInt(sx[i]));
                } else if (i == 1) 
                {
                    logEntry.setDateymd(Integer.parseInt(sx[i]));
                } else if (i == 2) 
                {
                    logEntry.setTime(Integer.parseInt(sx[i]));
                } else if (i == 3) 
                {
                    logEntry.setMove(sx[i]);
                } else if (i == 4) 
                {
                    logEntry.setProductNumber(Long.parseLong(sx[i]));
                } else if (i == 5) 
                {
                    logEntry.setPersonNumber(Long.parseLong(sx[i]));
                } 

            }
            return logEntry;
        }
        return null;
    }
    
    /**
     * Die Methode sucht nach den gespeicherten Nummern die Person und das
     * Produkt wieder heraus und fügt sie in den LogEntry ein
     * 
     * @param cm Kundenliste
     * @param em Mitarbeiterliste
     * @param pm Produktliste
     * @return wieder hergestellter Logeintrag
     * @throws IOException
     */
    private LogEntry loadLogEntryExtended(CustomerManagement cm, EmployerManagement em, ProductManagement pm)  throws IOException {
        LogEntry entry = loadLogEntry();
        if (entry != null)
        {
            Product p = pm.searchProductByNumber(entry.getProductNumber());
            if (p != null)
            {
                entry.setProduct(p);
            }
            Employer e = em.searchEmployerByNumber(entry.getPersonNumber());
            if (e != null)
            {
                entry.setPerson(e);
            } else 
            {
                Customer c = cm.searchCustomerByNumber(entry.getPersonNumber());
                if (c != null)
                {
                    entry.setPerson(c);
                }
            }
            entry.createMessage();
        }
        return entry;
    }

    
    /**
     * Methode zum Einlesen aller LogEntry-Objekte aus einer externen Datenquelle.
     * 
     * @return Log, wenn Einlesen erfolgreich, sonst null
     * @throws IOException
     */
    @Override
    public LogList loadAllLogEntrys(CustomerManagement cm, EmployerManagement em, ProductManagement pm) throws IOException {
        LogList log = new LogList();
        // openForReading(System.getProperty("user.dir") + "\\" + FILENAME);
        File f = new File(FILENAME);
        if (!f.exists()) 
        {
            f.createNewFile();
        }
        System.out.println(f.getAbsolutePath());
        openForReading(FILENAME);
        boolean next = true;

        while (next) 
        {
            LogEntry logEntry = loadLogEntryExtended(cm, em, pm);
            if (logEntry != null) 
            {
                log.addLogEntry(logEntry);
            } else {
                next = false;
            }
        }
        fr.close();
        return log;

    }

    @Override
    public boolean saveLogEntry(LogEntry logEntry) throws IOException {

        String s = "" 
              + logEntry.getQuantity();
        s = s + trenner;
        s = s + logEntry.getDateymd();
        s = s + trenner;
        s = s + logEntry.getTime();
        s = s + trenner;
        s = s + logEntry.getMove();
        s = s + trenner;
        s = s + logEntry.getProductNumber();
        s = s + trenner;
        s = s + logEntry.getPersonNumber();

        pw.println(s);
        return true;
    }

    public boolean saveAllLogEntrys(LogManagement log) throws IOException 
    {
        openForWriting(FILENAME, false);
        for (LogEntry logEntry : log.getLogList()) 
        {
            saveLogEntry(logEntry);
        }
        bw.flush();
        close();
        return false;
    }

    // ! --- Methoden die implementiert sein muessen ---

    @Override
    public Product loadProduct() throws IOException 
    {
        return null;
    }

    @Override
    public boolean saveProduct(Product product) throws IOException 
    {
        return false;
    }

    @Override
    public Customer loadCustomer() throws IOException 
    {
        return null;
    }

    @Override
    public boolean saveCustomer(Customer customer) throws IOException {
        return false;
    }

    @Override
    public Employer loadEmployer() throws IOException 
    {
        return null;
    }

    @Override
    public boolean saveEmployer(Employer employer) throws IOException 
    {
        return false;
    }

    @Override
    public boolean saveAllProducts(ProductList productlist) throws IOException 
    {
        return false;
    }

    @Override
    public boolean saveAllCustomer(CustomerList customerlist) throws IOException 
    {
        return false;
    }

    @Override
    public boolean saveAllEmployer(EmployerList employerlist) throws IOException 
    {
        return false;
    }

    
}