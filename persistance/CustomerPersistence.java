package persistance;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import domain.exceptions.CustomerAlreadyExists;
import domain.lists.CustomerList;
import domain.lists.EmployerList;
import domain.lists.LogList;
import domain.lists.ProductList;
import domain.logging.LogEntry;
import domain.management.CustomerManagement;
import domain.management.EmployerManagement;
import domain.management.ProductManagement;
import valueObjects.Customer;
import valueObjects.Employer;
import valueObjects.Product;

public class CustomerPersistence implements PersistenceManager {
    private FileWriter fw;
    private BufferedWriter bw;
    private PrintWriter pw;
    private FileReader fr;
    private BufferedReader br;
    public static final String FILENAME = "customer.clt";
    private String seperator = ";";

    /**
     * Oeffnet Stream um lesen zu koennen.
     * 
     * @param fileNameSting , Name der zu lesenden Datei als String.
     */
    @Override
    public void openForReading(String fileNameString) throws IOException {
        fr = new FileReader(fileNameString);
        br = new BufferedReader(fr);
    }

    /**
     * Oeffnet Stream um Dateien beschreiben zu koennen.
     * 
     * @param fileNameString , Name der zu beschreibenden Datei als String.
     */
    @Override
    public void openForWriting(String fileNameString) throws IOException {
        openForWriting(fileNameString, true);
    }

    // Zusammen gefasst Methode um sie einfach verwenden zu koennen.
    public void openForWriting(String fileNameString, boolean append) throws IOException {
        fw = new FileWriter(fileNameString, append);
        bw = new BufferedWriter(fw);
        pw = new PrintWriter(bw);
    }

    /**
     * Schliesst den gesammten Stream
     * 
     * @return true bei Erfolg, sonst false
     */
    @Override
    public boolean close() {
        if (fw != null) {
            try {
                fw.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
                return false;
            }
        } else if (fr != null) {
            try {
                fr.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
                return false;
            }
        }
        return true;
    }

    /**
     * Einlesen der Customer-Objekte aus einer externen Datenquelle.
     * 
     * @return Customer-Objekt, wenn Einleseprozess erfolgreich, sonst null.
     * @throws IOException
     */
    @Override
    public Customer loadCustomer() throws IOException {
        String objectMembers = br.readLine();

        if (objectMembers != null && !objectMembers.trim().equals("")) {
            String stringSplit[] = objectMembers.split(seperator);
            Customer loadedCustomer = new Customer();

            // wird nicht gebraucht, da nichts geloescht wird und so der Counter
            // sowieso bei der richtigen Zahl anfaengt
            // bringt zahlen durcheinander, weil nicht auf Counter von Produkt und
            // Mitarbeiter geachtet wird
            for (int i = 0; i < stringSplit.length; i++) {
                if (i == 0) {
                    loadedCustomer.getMUN().setNumber(Long.parseLong(stringSplit[i]));
                } else if (i == 1) {
                    loadedCustomer.setUsername(stringSplit[i]);
                } else if (i == 2) {
                    loadedCustomer.setPassword(stringSplit[i]);
                } else if (i == 3) {
                    loadedCustomer.setFirstname(stringSplit[i]);
                } else if (i == 4) {
                    loadedCustomer.setLastname(stringSplit[i]);
                } else if (i == 5) { // Address-Objekt im Customer wird definiert
                    loadedCustomer.getAddress().setStreetname(stringSplit[i]);
                } else if (i == 6) {
                    loadedCustomer.getAddress().setTown(stringSplit[i]);
                } else if (i == 7) {
                    loadedCustomer.getAddress().setCountry(stringSplit[i]);
                } else if (i == 8) {
                    loadedCustomer.getAddress().setPlz(Integer.parseInt(stringSplit[i]));
                } else if (i == 9) {
                    loadedCustomer.getAddress().setStreetnumber(Integer.parseInt(stringSplit[i]));
                } else if (i == 10) {
                    loadedCustomer.setDeleted(Boolean.parseBoolean(stringSplit[i]));
                }
            }
            return loadedCustomer;
        }
        return null;
    }

    /**
     * Alle Customer aus einer externen Datenquelle auslesen. Das Datenformat ist
     * als .clt (CustomerListText)
     * 
     * @return Customerliste, wenn Einlesen erfolgreich, sonst null
     * @throws IOException
     * @throws CustomerAlreadyExists
     */
    public CustomerList loadAllCustomer() throws IOException, CustomerAlreadyExists 
    {
        CustomerList cl = new CustomerList();
        File f = new File(FILENAME);
        
        // erstellt neues Dokument, wenn nötig
        if (!f.exists()) 
        {
            f.createNewFile();
        }

        System.out.println(f.getAbsolutePath());
        openForReading(FILENAME);
        boolean next = true;

        while (next) 
        {
            Customer c = loadCustomer();
            if (c != null) 
            {
                cl.addCustomer(c);
            } else {
                next = false;
            }
        }
        fr.close();
        return cl;
    }

    /**
     * Speichert jedes Customer-Objekt in eine externe Datenquelle.
     * 
     * @param customer Customer-Objekt, das gespeichert werden soll
     * @return true, wenn Schreibvorgang erfolgreich, sonst false
     * @throws IOException
     */
    @Override
    public boolean saveCustomer(Customer customer) throws IOException 
    {
        // boolean success = false;

        String customerAsString = "" + customer.getNumber();
        customerAsString += seperator + customer.getUsername();
        customerAsString += seperator + customer.getPassword();
        customerAsString += seperator + customer.getFirstname();
        customerAsString += seperator + customer.getLastname();
        // toSave Methode gibt Adresse im selben Format wie im elt Dokument aus.
        customerAsString += seperator + customer.getAddress().toSave();
        customerAsString += seperator + customer.getDeleted();

        pw.println(customerAsString);
        return false;
    }

    /**
     * Methode zum Schreiben aller Customerdaten in eine externe Datenquelle.
     * 
     * @param customer CustomerListe, die gespeichert werden soll
     * @return true, wenn Schreibvorgang erfolgreich, false sonst
     * @throws IOException
     */
    public boolean saveAllCustomer(CustomerList pl) throws IOException 
    {
        openForWriting(FILENAME, false);
        for (Customer p : pl.getCustomerList()) 
        {
            saveCustomer(p);
        }
        bw.flush();
        close();
        return false;
    }

    // ! --- Methoden die implementiert sein muessen ---

    @Override
    public Product loadProduct() throws IOException 
    {
        return null;
    }

    @Override
    public boolean saveProduct(Product product) throws IOException 
    {
        return false;
    }

    @Override
    public Employer loadEmployer() throws IOException 
    {
        return null;
    }

    @Override
    public boolean saveEmployer(Employer employer) throws IOException 
    {
        return false;
    }

    @Override
    public LogList loadAllLogEntrys(CustomerManagement cm, EmployerManagement em, ProductManagement pm) throws IOException 
    {
        return null;
    }

    @Override
    public boolean saveLogEntry(LogEntry logEntry) throws IOException 
    {
        return false;
    }

    @Override
    public boolean saveAllProducts(ProductList productlist) throws IOException 
    {
        return false;
    }

    @Override
    public boolean saveAllEmployer(EmployerList employerlist) throws IOException 
    {
        return false;
    }
}