package persistance;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import domain.exceptions.ProductAlreadyExistsException;
import domain.lists.CustomerList;
import domain.lists.EmployerList;
import domain.lists.LogList;
import domain.lists.ProductList;
import domain.logging.LogEntry;
import domain.management.CustomerManagement;
import domain.management.EmployerManagement;
import domain.management.ProductManagement;
import valueObjects.BulkProduct;
import valueObjects.Customer;
import valueObjects.Employer;
import valueObjects.Product;
import valueObjects.labeling.MUN;

public class ProductPersistence implements PersistenceManager {
    private FileWriter fw;
    private BufferedWriter bw;
    private PrintWriter pw;
    private FileReader fr;
    private BufferedReader br;
    public final static String FILENAME = "product.plt";
    private String trenner = ";";


    /**
     * Oeffnet Stream um Dateien auslesen zu koennen.
     * 
     * @param fileName, Name der zu lesenden Datei
     */
    @Override
    public void openForReading(String fileName) throws IOException {
        fr = new FileReader(fileName);
        br = new BufferedReader(fr);

    }


    /**
     * Oeffnet Stream um Dateien beschreiben zu koennen.
     * 
     * @param fileName, Name der zu beschreibenden Datei
     */
    @Override
    public void openForWriting(String datenquelle) throws IOException {
        openForWriting(datenquelle, true);
    }

    public void openForWriting(String datenquelle, boolean append) throws IOException {
        fw = new FileWriter(datenquelle, append);
        bw = new BufferedWriter(fw);
        pw = new PrintWriter(bw);
    }


    /**
     * Schliesst den Stream
     * 
     * @return true bei Erfolg, sonst false
     */
    @Override
    public boolean close() {
        if (fw != null) {
            try {
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        } else if (fr != null) {
            try {
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }


    /**
     * Methode zum Einlesen der Produkt-Objekte aus einer externen Datenquelle.
     * 
     * @return Produkt-Objekt, wenn Einlesen erfolgreich, sonst null
     * @throws IOException
     */
    @Override
    public Product loadProduct() throws IOException {
        String s = br.readLine();
        //erzeugt Luecken in den zahlen
        //MUN mun = new MUN();

        if (s != null && !s.trim().equals("")) {
            String sx[] = s.split(trenner);
            Product p = null;

            if (sx.length == 6) {
                p = new Product();
            } else {
                p = new BulkProduct();
            }

            //wird nicht gebraucht, da nichts geloescht wird und so der Counter
            //sowieso bei der richtigen Zahl anfaengt
            //bringt zahlen durcheinander, weil nicht auf Counter von Produkt und 
            //Kunde geachtet wird
            /*
            // Checke ob MUN größer ist, als die des ersten eingelesenen Produktes
            if(mun.getNumber() > Long.parseLong(sx[0]))
            {
                // Wenn ja, dann setze MUN counter auf die des eingelesenen Produktes
                MUN.setCounter(Long.parseLong(sx[0]));
            } else {
                MUN.setCounter(0);
            }*/

            for (int i = 0; i < sx.length; i++) {
                if (i == 0) {
                    p.getMUN().setNumber(Long.parseLong(sx[i]));
                } else if (i == 1) {
                    p.setName(sx[i]);
                } else if (i == 2) {
                    p.setPrice(Double.parseDouble(sx[i]));
                } else if (i == 3) {
                    p.setStock(Integer.parseInt(sx[i]));
                } else if (i == 4) {
                    p.setReserved(Integer.parseInt(sx[i]));
                } else if (i == 5) {
                    p.setDeleted(Boolean.parseBoolean(sx[i]));
                } else if (i == 6) {
                    BulkProduct bp = (BulkProduct) p;
                    bp.setPackSize(Integer.parseInt(sx[i]));
                    p.setStock(Integer.parseInt(sx[3]));
                }
            }
            return p;
        }
        return null;
    }


    /**
     * Methode zum Einlesen aller Produkt-Objekte aus einer externen Datenquelle.
     * 
     * @return Produktliste, wenn Einlesen erfolgreich, sonst null
     * @throws IOException
     * @throws ProductAlreadyExistsException
     */
    public ProductList loadAllProducts() throws IOException, ProductAlreadyExistsException {
        ProductList pl = new ProductList();
        // openForReading(System.getProperty("user.dir") + "\\" + FILENAME);
        File f = new File(FILENAME);
        if (!f.exists()) 
        {
            f.createNewFile();
        }
        System.out.println(f.getAbsolutePath());
        openForReading(FILENAME);
        boolean next = true;
        while (next) {
            Product p = loadProduct();
            if (p != null) {
                pl.addProduct(p);
            } else {
                next = false;
            }
        }
        fr.close();
        return pl;
    }


    /**
     * Methode zum Schreiben der Produktdaten in eine externe Datenquelle.
     * 
     * @param product ProduKt-Objekt, das gespeichert werden soll
     * @return true, wenn Schreibvorgang erfolgreich, false sonst
     * @throws IOException
     */
    public boolean saveProduct(Product product) throws IOException {
        String s = "";
        s = s + product.getMUN().getNumber();
        s = s + trenner;
        s = s + product.getName();
        s = s + trenner;
        s = s + product.getPrice();
        s = s + trenner;
        s = s + product.getStock();
        s = s + trenner;
        s = s + product.getReserved();
        s = s + trenner;
        s = s + product.getDeleted();
        if (product instanceof BulkProduct) {
            BulkProduct productB = (BulkProduct) product;
            s = s + trenner;
            s = s + productB.getPackSize();
        }
        pw.println(s);
        return true;
    }


    /**
     * Methode zum Schreiben aller Produktdaten in eine externe Datenquelle.
     * 
     * @param product ProduKt-Objekt, das gespeichert werden soll
     * @return true, wenn Schreibvorgang erfolgreich, false sonst
     * @throws IOException
     */
    @Override
    public boolean saveAllProducts(ProductList pl) throws IOException {
        openForWriting(FILENAME, false);
        for (Product p : pl.getProductList()) {
            saveProduct(p);
        }
        bw.flush();
        close();
        return false;
    }


    @Override
    public Customer loadCustomer() throws IOException 
    {
        return null;
    }

    @Override
    public boolean saveCustomer(Customer customer) throws IOException 
    {
        return false;
    }

    @Override
    public Employer loadEmployer() throws IOException 
    {
        return null;
    }

    @Override
    public boolean saveEmployer(Employer employer) throws IOException 
    {
        return false;
    }

    @Override
    public LogList loadAllLogEntrys(CustomerManagement cm, EmployerManagement em, ProductManagement pm) throws IOException 
    {
        return null;
    }

    @Override
    public boolean saveLogEntry(LogEntry logEntry) throws IOException 
    {
        return false;
    }

    @Override
    public boolean saveAllCustomer(CustomerList customerlist) throws IOException 
    {
        return false;
    }

    @Override
    public boolean saveAllEmployer(EmployerList employerlist) throws IOException 
    {
        return false;
    }

    
    
}